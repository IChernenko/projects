<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>



<div class="content registration_form woocommerce">

    <div class="container">
        <div class="row" id="page_title">
            <div class="col-md-8 col-md-offset-2 text-center">

                <h1><?php _e( 'Login', 'woocommerce' ); ?></h1>
                <?php wc_print_notices(); ?>

            </div>

        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div id="myTabContent" class="tab-content margin_form">
                    <div class="tab-pane fade active in" id="soiskatel">
                    	<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
                        <form class="form-horizontal register" method="post">
                         	<?php do_action( 'woocommerce_login_form_start' ); ?>

							<p class="form-row form-row-wide">
								<?php wp_nonce_field( 'woocommerce-login' ); ?>								
								<input type="text" class="form-control" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" placeholder="Username or email address" />
							</p>
							<p class="form-row form-row-wide">
								<input class="form-control" type="password" name="password" id="password" placeholder="Password" />
							</p>

							<?php do_action( 'woocommerce_login_form' ); ?>
					      
							
					     <p class="lost_password">
							<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
						</p>
						<div class="row">
				           <div class="col-md-8 col-md-offset-2 text-center">								
								<input type="submit" class="btn btn-default register_button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />							    
					        </div>
                        </div>
						
						<?php do_action( 'woocommerce_login_form_end' ); ?>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>