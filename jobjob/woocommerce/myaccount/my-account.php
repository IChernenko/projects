<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices(); ?>



<div class="content page-left-sidebar" id="lk">
    <div class="container">
        <div class="row">
            <div class="col-md-3">   
            <input type="hidden" name="wp-user-avatar" id="wp-user-avatar-existing" value="2585">
            <div class="my_accaunt_avatar_wrapper">
                    <a href="#" id="wpua-add-existing" class="choose_image_class">

                        <?php $current_user = wp_get_current_user(); ?>
                        <?php echo get_avatar( $current_user->user_email); ?>
                    </a>
                        <?php echo do_shortcode( '[avatar_upload]' ); ?>
                    </div>                     
                <div class="left-sidebar">
                	<?php 
                        if ( has_nav_menu( 'accaunt_menu' ) ) {
    				    	wp_nav_menu(array(
    				    		'theme_location' => 'accaunt_menu'
    				    	));
                        }
				    ?>
                  
                </div>
                
                </div>
            <div class="col-md-9">
              
               
                    <?php wc_print_notices(); ?>

                    <form action="" method="post" class="my_accaunt_page">

                        <?php do_action( 'woocommerce_edit_account_form_start' ); ?>

                        <p class="form-row form-row-first">
                            <label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
                            <input type="text" class="input-text form-control" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
                        </p>
                        <p class="form-row form-row-last">
                            <label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
                            <input type="text" class="input-text form-control" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
                        </p>
                        <div class="clear"></div>

                        <p class="form-row form-row-wide">
                            <label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                            <input type="email" class="input-text form-control" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
                        </p>

                        <fieldset>
                            <legend><?php _e( 'Password Change', 'woocommerce' ); ?></legend>

                            <p class="form-row form-row-wide">
                                <label for="password_current"><?php _e( 'Current Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
                                <input type="password" class="input-text form-control" name="password_current" id="password_current" />
                            </p>
                            <p class="form-row form-row-wide">
                                <label for="password_1"><?php _e( 'New Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
                                <input type="password" class="input-text form-control" name="password_1" id="password_1" />
                            </p>
                            <p class="form-row form-row-wide">
                                <label for="password_2"><?php _e( 'Confirm New Password', 'woocommerce' ); ?></label>
                                <input type="password" class="input-text form-control" name="password_2" id="password_2" />
                            </p>
                        </fieldset>
                        <div class="clear"></div>

                        <?php do_action( 'woocommerce_edit_account_form' ); ?>

                        <p>
                            <?php wp_nonce_field( 'save_account_details' ); ?>
                            <input type="submit" class="my_accaunt_save_button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
                            <input type="hidden" name="action" value="save_account_details" />
                        </p>

                        <?php do_action( 'woocommerce_edit_account_form_end' ); ?>

                    </form>

            </div>
        </div> 
    </div>
</div>


