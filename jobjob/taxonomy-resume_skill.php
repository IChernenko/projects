<?php /**  * The template for displaying archive pages.  *  * @link
https://codex.wordpress.org/Template_Hierarchy  *  * @package jobjob  */

get_header(); ?>




<?php 
$paged = 1;
if ( get_query_var('paged') ) $paged = get_query_var('paged');
if ( get_query_var('page') ) $paged = get_query_var('page');
	$args = array(
        'post_type' => 'resume',
		'posts_per_page' => '-1',
        'paged' => $paged,
        'tax_query' => array(
            array(
                'taxonomy' => 'resume_skill',
                'field' => 'slug',
                'terms' => $_GET['resume_skill']
            ),
        ),
     );
	// $loop = new WP_Query($args);    
    query_posts( $args ); 
?>


<div class="content page-right-sidebar" id="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php single_cat_title( '', true ); ?> - 
                        <?php 
                           printf( _n( '%d resume Available', '%d resumes Available', $wp_query->found_posts ), $wp_query->found_posts ); 
                        ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    
                    <div id="right-sidebar-banner">
                        <?php dynamic_sidebar( 'sidebar_archive' ); ?>
                    </div>
                </div>
                <div class="col-md-9 col-md-pull-3 container-blog container-soiskately">
                <div class="blog_item_wrapper">   
                    <?php 
    					while ( have_posts() ) : the_post(); ?>
    			           <?php get_template_part( 'template-parts/content', 'resume_listing' ); ?>
    			    	<?php endwhile;				    
                    ?>      
                </div>         
                <?php the_posts_pagination( array(
                    'mid_size' => 2,
                    'prev_text' => __( 'Back', 'textdomain' ),
                    'next_text' => __( 'Onward', 'textdomain' ),
                )); ?> 
                    
                </div>
                
            </div>
        </div>
    </div>


<?php get_footer(); ?>
