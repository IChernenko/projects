<?php
/**
 * jobjob functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package jobjob
 */

if ( ! function_exists( 'jobjob_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function jobjob_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on jobjob, use a find and replace
	 * to change 'jobjob' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'jobjob', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	
	add_theme_support( 'job-manager-templates' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'jobjob' ),
		'accaunt_menu' => esc_html__( 'Accaunt Menu', 'jobjob' ),
		'login_menu' => esc_html__( 'login Menu', 'jobjob' ),
		'blog-menu' => esc_html__( 'blog menu', 'jobjob' ),
		'footer-1_menu' => esc_html__( 'footer-1 Menu', 'jobjob' ),
		'footer-2_menu' => esc_html__( 'footer-2 Menu', 'jobjob' ),
		'footer-3_menu' => esc_html__( 'footer-3 Menu', 'jobjob' ),
		'footer-4_menu' => esc_html__( 'footer-icon Menu', 'jobjob' )
	) );



	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'jobjob_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // jobjob_setup
add_action( 'after_setup_theme', 'jobjob_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jobjob_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'jobjob_content_width', 640 );
}
add_action( 'after_setup_theme', 'jobjob_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jobjob_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jobjob' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Main Page Sidebar', 'jobjob' ),
		'id'            => 'sidebar_home',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Page Sidebar', 'jobjob' ),
		'id'            => 'sidebar_blog',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Archive Page Sidebar', 'jobjob' ),
		'id'            => 'sidebar_archive',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Company Page Sidebar', 'jobjob' ),
		'id'            => 'sidebar_company',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Single Resume', 'jobjob' ),
		'id'            => 'sidebar_single_resume',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Single Job', 'jobjob' ),
		'id'            => 'sidebar_single_job',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Single Blog', 'jobjob' ),
		'id'            => 'sidebar_single_blog',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'jobjob_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function getJobmetaMap() {
	$coord = array();
    $args = array(  
        'post_type' => 'job_listing',
        'posts_per_page' => '-1'
     );
    query_posts( $args ); 
    while ( have_posts() ) : the_post();
    $lat = get_post_meta( get_the_ID(), 'geolocation_lat', true );
    $lng = get_post_meta( get_the_ID(), 'geolocation_long', true );
    $coord[$lat] = $lng;
    endwhile;  
    wp_reset_query();

    return json_encode($coord);
}

function jobjob_scripts() {

	wp_enqueue_style( 'jobjob-style', get_stylesheet_uri() );
	wp_enqueue_style( 'jobjob-bootstrap-grid', get_template_directory_uri() . '/libs/bootstrap/css/bootstrap-grid.min.css' );
	wp_enqueue_style( 'jobjob-bootstrap-min', get_template_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'jobjob-animate', get_template_directory_uri() . '/libs/animate/animate.css' );
	wp_enqueue_style( 'jobjob-fonts', get_template_directory_uri() . '/css/fonts.css' );
	wp_enqueue_style( 'jobjob-main', get_template_directory_uri() . '/css/main.css' );
	wp_enqueue_style( 'jobjob-select', get_template_directory_uri() . '/css/bootstrap-select.css' );
	wp_enqueue_style( 'jobjob-bootstrap_skin', get_template_directory_uri() . '/css/bootstrap_skin.css' );
	wp_enqueue_style( 'jobjob-media', get_template_directory_uri() . '/css/media.css"' );
	

	wp_enqueue_script( 'jobjob-main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '20120206214', true );
	wp_enqueue_script( 'jobjob-bootstrap', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.min.js', array('jquery'), '201202061', true );
	wp_enqueue_script( 'jobjob-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'jobjob-modernizr', get_template_directory_uri() . '/libs/modernizr/modernizr.js', array(), '123456987', true );
	wp_enqueue_script( 'jobjob-select-js', get_template_directory_uri() . '/js/bootstrap-select.js', array(), '1234569873', true );
	
	wp_enqueue_script( 'jobjob-waypoints', get_template_directory_uri() . '/libs/waypoints/waypoints.min.js', array(), '201202062', true );
	wp_enqueue_script( 'jobjob-animate', get_template_directory_uri() . '/libs/animate/animate-css.js', array(), '201202063', true );
	wp_enqueue_script( 'jobjob-plugins-scroll', get_template_directory_uri() . '/libs/plugins-scroll/plugins-scroll.js', array(), '201202064', true );

	

	wp_enqueue_script( 'jobjob-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


	 wp_localize_script( "jobjob-main-js", "php_vars", getJobmetaMap() );
}
add_action( 'wp_enqueue_scripts', 'jobjob_scripts' );

// require_once ('functions/modality-customizer.php');	

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load tgm
 */

require_once get_template_directory() . '/wp_bootstrap_navwalker.php';

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';


add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );


function my_theme_register_required_plugins() {

	$plugins = array(		
		array(
			'name'         => 'WP Job Manager', 
			'slug'         => 'wp-job-manager', 
			'required'     => true, 
			'source' => 'https://wordpress.org/plugins/wp-job-manager/', 
		),
		array(
			'name'         => 'Infinite Scroll and Load More Ajax Pagination', 
			'slug'         => 'infinite-scroll-and-load-more-ajax-pagination', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/infinite-scroll-and-load-more-ajax-pagination/', 
		),
		array(
			'name'         => 'WP Job Manager - Company Profiles', 
			'slug'         => 'wp-job-manager-companies', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/wp-job-manager-companies/', 
		),
		array(
			'name'         => 'WP Job Manager - Contact Listing', 
			'slug'         => 'wp-job-manager-contact-listing', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/wp-job-manager-contact-listing/', 
		),
		array(
			'name'         => 'WP Job Manager - Predefined Regions', 
			'slug'         => 'wp-job-manager-locations', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/wp-job-manager-locations/installation/', 
		),
		array(
			'name'                  => 'Resume Manager',
			'slug'                  => 'wp-job-manager-resumes',
			'source'                => get_template_directory() . '/plugins/wp-job-manager-resumes.zip',
			'required'              => false,
			'is_automatic'          => true,
		),
		array(
			'name'                  => 'Bookmarks',
			'slug'                  => 'wp-job-manager-bookmarks',
			'source'                => get_template_directory() . '/plugins/wp-job-manager-bookmarks.zip',
			'required'              => false,
			'is_automatic'          => true,
		),
		array(
			'name'                  => 'Applications',
			'slug'                  => 'wp-job-manager-applications',
			'source'                => get_template_directory() . '/plugins/wp-job-manager-applications.zip',
			'required'              => false,
			'is_automatic'          => true,
		),
		array(
			'name'         => 'Ninja Forms', 
			'slug'         => 'ninja-forms', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/ninja-forms/', 
		),
		array(
			'name'         => 'WordPress ReCaptcha Integration', 
			'slug'         => 'wp-recaptcha-integration', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/wp-recaptcha-integration/', 
		),
		array(
			'name'         => 'WooCommerce - excelling eCommerce', 
			'slug'         => 'woocommerce', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/woocommerce/', 
		),
		array(
			'name'         => 'WooCommerce Menu Extension', 
			'slug'         => 'woocommerce-menu-extension', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/woocommerce-menu-extension/', 
		),
		array(
			'name'         => 'WooCommerce Simple Registration', 
			'slug'         => 'woocommerce-simple-registration', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/woocommerce-simple-registration/', 
		),
		array(
			'name'         => 'WP User Avatar', 
			'slug'         => 'wp-user-avatar', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/wp-user-avatar/', 
		),
		array(
			'name'         => 'Menu Image', 
			'slug'         => 'menu-image', 
			'required'     => false, 
			'source' => 'https://wordpress.org/plugins/menu-image/', 
		),

		
	);


	$config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.


	);

	tgmpa( $plugins, $config );
}







/*
==================================
add requirement field
==================================
*/

add_filter( 'submit_job_form_fields', 'frontend_add_requirement_field' );

function frontend_add_requirement_field( $fields ) {
  $fields['job']['job_requirement'] = array(
    'label'       => __( 'requirement', 'job_manager' ),
    'type'        => 'text',
    'required'    => false,
    'placeholder' => 'some skils',
    'priority'    => 7
  );
  return $fields;
}


add_filter( 'job_manager_job_listing_data_fields', 'admin_add_requirement_field' );

function admin_add_requirement_field( $fields ) {
  $fields['_job_requirement'] = array(
    'label'       => __( 'requirement', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => 'some skils',
    'description' => ''
  );
  return $fields;
}


/*
==================================
add salary field
==================================
*/
add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );

function frontend_add_salary_field( $fields ) {
  $fields['job']['job_salary'] = array(
    'label'       => __( 'Salary', 'job_manager' ),
    'type'        => 'text',
    'required'    => false,
    'placeholder' => 'e.g. 20000',
    'priority'    => 7
  );
  return $fields;
}


add_filter( 'job_manager_job_listing_data_fields', 'admin_add_salary_field' );

function admin_add_salary_field( $fields ) {
  $fields['_job_salary'] = array(
    'label'       => __( 'Salary', 'job_manager' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 20000',
    'description' => ''
  );
  return $fields;
}




/*
==================================
add search by salary
==================================
*/
add_action( 'job_manager_job_filters_search_jobs_start', 'filter_by_salary_field' );
function filter_by_salary_field() {
	?>
	<div id="salary_search_id" class="search_categories salary_search">		
		<input id="salary_checkbox" type="checkbox" name="filter_by_salary" value="check" class="job-manager-filter salary_checkbox">
		<label class="label_checkbox" for="salary_checkbox">Only the vacancies with salary</label>
	</div>
	<?php
}
// add search by salary back end

add_filter( 'job_manager_get_listings', 'filter_by_salary_field_query_args', 10, 2 );
function filter_by_salary_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if (  $form_data['filter_by_salary'] == 'check' ) {
			$query_args['meta_query'][] = array(
				'key'     => '_job_salary',
				'value'   => '0',
				'compare' => '>'
 			); 	
		}
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );		
	}
	return $query_args;
}



// add search by job type back end
add_filter( 'job_manager_get_listings', 'filter_by_date_field_query_args', 10, 2 );
function filter_by_date_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if (  $form_data['sort_additional_date'] == 'date_less' ) {			
			$query_args['orderby']["date"] = 'DESK';
		}
		if (  $form_data['sort_additional_date'] == 'date_more' ) {		
			$query_args['orderby']["date"] = 'ASC';
		}		
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );		
	}
	// var_dump($query_args);
	return $query_args;
}

add_filter( 'job_manager_get_listings', 'order_by_salary_field_query_args', 10, 2 );
function order_by_salary_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if (  $form_data['sort_additional_sallary'] == 'salary_more' ) {
			// $query_args['meta_query'][] = array(				
			
			// 	'key'     => '_job_salary'

			// );
			// $query_args['orderby'] = array(
			
			// 	'meta_key' => '_job_salary',
			// );
			

		}
		// var_dump($query_args);
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );		
	}
	// var_dump($query_args);
	return $query_args;
}	
/*
==================================
add search by job type
==================================
*/

if ( $_SERVER[REQUEST_URI] != '/jm-ajax/get_listings/' ) {	
	setcookie('type_listing_cookies_temp', "", time()-3600, '/');		
}

add_action( 'job_manager_job_filters_search_jobs_end', 'filter_by_type' );
function filter_by_type() {
	?>
	<div class="search_categories">
		<label for="select_type">Type</label>
		<select id="select_type" name="job_listing_type[]" class="job-manager-filter chosen-select" multiple  data-placeholder="Select type">
		<?php 
			$terms = get_terms( 'job_listing_type' );
		?>
		<?php
			$newArray = stripslashes($_COOKIE['type_listing_cookies_temp']);
			$newArray = json_decode($newArray);
	        foreach ( $terms as $term ) {
	        	if ( in_array($term->slug, $newArray)  ) {
	        		echo '<option selected value="' . $term->slug . '">' . $term->name . '</option>';		 
	        	} else {
	        		echo '<option value="' . $term->slug . '">' . $term->name . '</option>';		 
	        	}		       
		    }
    	?>
    	</select>
	</div>
	<?php
}

// add search by job type back end
add_filter( 'job_manager_get_listings', 'filter_by_type_field_query_args', 10, 2 );
function filter_by_type_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if (  $form_data['job_listing_type'] != null ) {
			$query_args['tax_query'][] = array(
				'taxonomy' => 'job_listing_type',
                'terms' =>$form_data['job_listing_type'],
                'field' => 'slug'
			);
		}
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );		

		$cookie_value = json_encode($form_data['job_listing_type']);
		setcookie('type_listing_cookies_temp', "", time()-3600, '/');
		setcookie('type_listing_cookies_temp', $cookie_value, time() + 3600, '/');
	}
	return $query_args;
}


/*
==================================
add search by job region
==================================
*/

if ( $_SERVER[REQUEST_URI] != '/jm-ajax/get_listings/' ) {	
	setcookie('region_listing_cookies_temp', "", time()-3600, '/');		
}

add_action( 'job_manager_job_filters_search_jobs_end', 'filter_by_region' );
function filter_by_region() {
	?>
	<div class="search_categories">
		<label for="select_region">Region</label>
		<select id="select_region" name="job_listing_region[]"  class="job-manager-filter chosen-select" data-placeholder="Select region" multiple >
		<?php 
			$terms = get_terms( 'job_listing_region' );
		?>   
		<?php
			$newArray = stripslashes($_COOKIE['region_listing_cookies_temp']);
			$newArray = json_decode($newArray);
	        foreach ( $terms as $term ) {
	        	if ( in_array($term->slug, $newArray)  ) {
	        		echo '<option selected value="' . $term->slug . '">' . $term->name . '</option>';		 
	        	} else {
	        		echo '<option value="' . $term->slug . '">' . $term->name . '</option>';		 
	        	}		       
		    }
    	?>
    	</select>    	
    </div>

	<?php
}



// add search by job type back end
add_filter( 'job_manager_get_listings', 'filter_by_region_field_query_args', 10, 2 );
function filter_by_region_field_query_args( $query_args, $args ) {	
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if (  $form_data['job_listing_region'] != null ) {
			$query_args['tax_query'][] = array(
				'taxonomy' => 'job_listing_region',
                'terms' => $form_data['job_listing_region'],
                'field' => 'slug'                
			);
		}
		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );

		$cookie_value = json_encode($form_data['job_listing_region']);
		setcookie('region_listing_cookies_temp', "", time()-3600, '/');
		setcookie('region_listing_cookies_temp', $cookie_value, time() + 3600, '/');
	}	
	return $query_args;
}


/*
==================================
add search by job date
==================================
*/
// add_action( 'job_manager_job_filters_search_jobs_start', 'filter_by_date' );/


/*
==================================
woocomerce compability
==================================
*/
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<section id="main">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


	function company_url( $company_name ) {
		global $wp_rewrite;

		$company_name = rawurlencode( $company_name );

		if ( $wp_rewrite->permalink_structure == '' ) {
			$url = home_url( 'index.php?'. 'company' . '=' . $company_name );
		} else {
			$url = home_url( '/' . 'company' . '/' . trailingslashit( $company_name ) );
		}

		return esc_url( $url );
	}


// ===============================================woocommerce registration field

add_action( 'register_form', 'adding_custom_registration_fields' );
function adding_custom_registration_fields( ) {

	//lets make the field required so that i can show you how to validate it later;
	$firstname = empty( $_POST['firstname'] ) ? '' : $_POST['firstname'];
	$lastname  = empty( $_POST['lastname'] ) ? '' : $_POST['lastname'];
	$role  = empty( $_POST['role'] ) ? '' : $_POST['role'];
	?>
	<!-- <div class="form-row form-row-wide">		
		<input type="text" class="form-control" name="firstname" id="reg_firstname" value="<?php echo esc_attr( $firstname ) ?>" placeholder="Name" />
	</div> -->
	<!-- <div class="form-row form-row-wide">		
		<input type="text" class="form-control" name="lastname" id="reg_lastname" value="<?php echo esc_attr( $lastname ) ?>" placeholder="Last name" />		
	</div> -->
	<input type="hidden" name="role" value="employer" />
	<?php
}


add_filter( 'woocommerce_registration_errors', 'registration_errors_validation' );

/**
 * @param WP_Error $reg_errors
 *
 * @return WP_Error
 */
function registration_errors_validation( $reg_errors ) {
	if ( empty( $_POST['firstname'] ) || empty( $_POST['lastname'] ) ) {
		$reg_errors->add( 'empty required fields', __( 'Please fill in the required fields.', 'woocommerce' ) );
	}
	return $reg_errors;
}

add_action('woocommerce_created_customer','adding_extra_reg_fields');

function adding_extra_reg_fields($user_id) {
	extract($_POST);
	update_user_meta($user_id, 'first_name', $firstname);
	update_user_meta($user_id, 'last_name', $lastname);
	wp_update_user( array( 'ID' => $user_id, 'role' => $_POST['role'] ) );
}


// ==============================================ser role to registration field
function ts_wp_dropdown_roles( $selected = '' ) {
    $p = '';
    $r = '';

    $editable_roles = array_reverse( ts_get_editable_roles() );

    foreach ( $editable_roles as $role => $details ) {
        $name = translate_user_role($details['name'] );
        if ( $selected == $role ) // preselect specified role
            $p = "\n\t<option selected='selected' value='" . esc_attr($role) . "'>$name</option>";
        else
            $r .= "\n\t<option value='" . esc_attr($role) . "'>$name</option>";
    }
    echo $p . $r;
}

// Custom editable_roles function to be used by "ts_wp_dropdown_roles" function
function ts_get_editable_roles() {
    global $wp_roles;

    $all_roles = $wp_roles->roles;

    /**
     * Filter the list of editable roles.
     *
     * @since 2.8.0
     *
     * @param array $all_roles List of roles.
     */
    $editable_roles = apply_filters( 'ts_editable_roles', $all_roles );

    return $editable_roles;
}

add_filter("ts_editable_roles" , "ts_filter_roles", 10 ,1);

function ts_filter_roles($all_roles){
    foreach($all_roles as $key=>$value){
        if($key !== "employer" && $key !== "candidate"){
            unset($all_roles[$key]);
        }
    }
    return $all_roles;

}



/** 
 * Load function to change excerpt length
 * 
 */
function new_excerpt_length($length) {
    return 40;
}
add_filter('excerpt_length', 'new_excerpt_length');



// ==========================================register resume taxonomy
// register_taxonomy("job_resume_schedule", array("resume"), array(
// 	"hierarchical" => true, 
// 	"show_admin_column" => true,
// 	'show_ui' => true, 
// 	'public' => true, 
// 	"label" => "Schedule", 
// 	"singular_label" => "schedule", 
// 	"rewrite" => true
// ));


// register_taxonomy("job_resume_experience", array("resume"), array(
// 	"hierarchical" => true, 
// 	"show_admin_column" => true,
// 	'show_ui' => true, 
// 	'public' => true, 
// 	"label" => "Experience", 
// 	"singular_label" => "experience", 
// 	"rewrite" => true
// ));


// ======================================================resume search


// add_filter( 'job_manager_get_listings', 'filter_by_region_field_query_args', 10, 2 );
// function filter_by_region_field_query_args( $query_args, $args ) {	
// 	if ( isset( $_POST['form_data'] ) ) {
// 		parse_str( $_POST['form_data'], $form_data );
// 		if (  $form_data['job_listing_region'] != null ) {
// 			$query_args['tax_query'][] = array(
// 				'taxonomy' => 'job_listing_region',
//                 'terms' => $form_data['job_listing_region'],
//                 'field' => 'slug'                
// 			);
// 		}
// 		add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );

// 		$cookie_value = json_encode($form_data['job_listing_region']);
// 		setcookie('region_listing_cookies_temp', "", time()-3600, '/');
// 		setcookie('region_listing_cookies_temp', $cookie_value, time() + 3600, '/');
// 	}	
// 	return $query_args;
// }


// add_filter( 'avatar_defaults', 'bourncreative_custom_gravatar' );  
// function bourncreative_custom_gravatar ($avatar_defaults) {
//      $myavatar = get_stylesheet_directory_uri() . '/img/custom-gravatar.jpg';
//      $avatar_defaults[$myavatar] = "Custom Gravatar";
//      return $avatar_defaults;
// }

// =============================================get resume


function getResumes($post_per_page = '-1') {	
    $args = array(  
        'post_type' => 'resume',
        'posts_per_page' => $post_per_page
    );
  
 	$resume = array($args); 
    $posts = get_posts( $args );

    foreach( $posts as $post ){ 
    	setup_postdata($post);
		array_push($resume, $post);
    }
	wp_reset_postdata();

   return $resume;
}

// =============================================get jobs


function getJobs($post_per_page = '-1') {	
    $args = array(  
        'post_type' => 'job_listing',
        'posts_per_page' => $post_per_page
    );
  
 	$jobs = array($args); 
    $posts = get_posts( $args );

    foreach( $posts as $post ){ 
    	setup_postdata($post);
		array_push($jobs, $post);
    }
	wp_reset_postdata();

   return $jobs;
}

// =============================================get companies
function getCompanies() {	
    
      
    $args = array(  
        'post_type' => 'job_listing',
        'posts_per_page' => '-1'
    );

	query_posts( $args );
    $companies = array(); 

    while ( have_posts() ) : the_post();        
        $company_name = the_company_name('', '', false);                           
        $company_logo = get_the_company_logo(); 
   
        $company_name = the_company_name('', '', false);
        $companies[$company_name] = $company_logo;
   endwhile;  

   return $companies;
}


// =====================================================post type for footer
// add_action('init', 'footer_register');

// function footer_register() {
 
// 	$labels = array(
// 		'name' => ('Footer'),
// 		'add_new' =>('Add'),
// 		'add_new_item' =>('Add'),
// 		'edit_item' =>('Edit'),
// 		'new_item' =>('New'),
// 		'view_item' =>('View'),
// 		'search_items' =>('Search'),
// 		'not_found' =>('not found'),
// 		'not_found_in_trash' =>('not found'),
// 		'parent_item_colon'=> ''
// 	);
 
// 	$args = array(
// 		'labels' => $labels,
// 		'public' => true,
// 		'supports' => array('title', 'editor')
// 	  ); 
 
// 	register_post_type( 'footer' , $args );	
// }

// $config = array(
//     'id' => 'footer_id',              // meta box id, unique per meta box 
//     'title' => 'Fields',                   // meta box title
//     'pages' => array('footer'),            // post types, accept custom post types as well, default is array('post'); optional
//     'context' => 'normal',               // where the meta box appear: normal (default), advanced, side; optional
//     'priority' => 'high',                // order of meta box: high (default), low; optional
//     'fields' => array(),                 // list of meta fields (can be added by field arrays) or using the class's functions
//     'local_images' => false,             // Use local or hosted images (meta box images for add/remove)
//     'use_with_theme' => get_stylesheet_directory_uri() . '/meta-box-class'           //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
// );

// $my_meta_footer = new AT_Meta_Box($config);

// // $my_meta_footer->addTextarea($prefix.'text_field_footer',array('name'=> 'Text'));
// $my_meta_footer->addImage('image_field_footer',array('name'=> 'Image-1'));
// $my_meta_footer->addImage('image_field_footer-2',array('name'=> 'Image-2'));

// $my_meta_footer->Finish();


// =========================================================walker
// class BlogMenu extends Walker_Nav_Menu {
// 	public function start_lvl( &$output, $depth = 0, $args = array() ) {
// 		$indent = str_repeat( "\t", $depth );		
			
// 			$output .= '<div class="panel-group" id="accordion"' . $depth . ' role="tablist" aria-multiselectable="true">';
		
// 	}

// 	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
// 		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
	
// 	}	

//     function end_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        
     
//     }

//  	function end_lvl(&$output, $depth = 0, $args = array()) {
//         $output .= "</div>";
//         if ($depth == 0) {
            
//             $output .= "";
//         }
//     }
  

// }




function custom_submit_job_steps( $steps ) {
	unset( $steps['preview'] );
	return $steps;
}
add_filter( 'submit_job_steps', 'custom_submit_job_steps' );
/**
 * Change button text (won't work until v1.16.2)
 */
function change_preview_text() {
	return __( 'Submit Job' );
}
add_filter( 'submit_job_form_submit_button_text', 'change_preview_text' );
/**
 * Since we removed the preview step and it's handler, we need to manually publish jobs
 * @param  int $job_id
 */
function done_publish_job( $job_id ) {
	$job = get_post( $job_id );
	if ( in_array( $job->post_status, array( 'preview', 'expired' ) ) ) {
		// Reset expirey
		delete_post_meta( $job->ID, '_job_expires' );
		// Update job listing
		$update_job                  = array();
		$update_job['ID']            = $job->ID;
		$update_job['post_status']   = get_option( 'job_manager_submission_requires_approval' ) ? 'pending' : 'publish';
		$update_job['post_date']     = current_time( 'mysql' );
		$update_job['post_date_gmt'] = current_time( 'mysql', 1 );
		wp_update_post( $update_job );
	}
}
add_action( 'job_manager_job_submitted', 'done_publish_job' );












/**
 * Remove the preview step. Code goes in theme functions.php or custom plugin.
 * @param  array $steps
 * @return array
 */
function custom_submit_resume_steps( $steps ) {
	unset( $steps['preview'] );
	return $steps;
}
add_filter( 'submit_resume_steps', 'custom_submit_resume_steps' );

/**
 * Change button text
 */
function change_resume_form_preview_text() {
	return __( 'Submit Resume' );
}
add_filter( 'submit_resume_form_submit_button_text', 'change_resume_form_preview_text' );

/**
 * Since we removed the preview step and it's handler, we need to manually publish resumes
 * @param  int $resume_id
 */
function done_publish_resume( $resume_id ) {
	$resume = get_post( $resume_id );

	if ( in_array( $resume->post_status, array( 'preview', 'expired' ) ) ) {
		// Reset expirey
		delete_post_meta( $resume->ID, '_resume_expires' );

		// Update listing
		$update                  = array();
		$update['ID']            = $resume->ID;
		$update['post_status']   = get_option( 'resume_manager_submission_requires_approval' ) ? 'pending' : 'publish';
		$update['post_date']     = current_time( 'mysql' );
		$update['post_date_gmt'] = current_time( 'mysql', 1 );
		wp_update_post( $update );
	}
}
add_action( 'resume_manager_resume_submitted', 'done_publish_resume' );




// ==============================================customizer
add_action( 'customize_register', 'themedemo_customize' );
function themedemo_customize($wp_customize) {

    $wp_customize->add_section( 'jobjob_header', array(
        'title'          => 'Header logo customize',
        'priority'       => 35
    ) );

    $wp_customize->add_setting( 'header_text', array(
        'default'        => 'Work'
    ) );

    $wp_customize->add_control( 'header_text', array(
        'label'   => 'Logo text',
        'section' => 'jobjob_header',
        'type'    => 'text'
    ) );

    
    $wp_customize->add_setting( 'header_width', array(
        'default'        => '50',
    ) );

    $wp_customize->add_control( 'header_width' , array(
        'label'   => 'Width',
        'section' => 'jobjob_header',
        'type'    => 'numeric'
    ) );

	// ---------------------------------------------
	$wp_customize->add_section( 'jobjob_footer', array(
        'title'          => 'Footer text',
        'priority'       => 35
    ) );

    $wp_customize->add_setting( 'footer_left_text', array(
        'default'        => '© 2015 - OOO “Job search” 
							All rights reserved'
    ) );

    $wp_customize->add_control( 'footer_left_text', array(
        'label'   => 'Footer left text',
        'section' => 'jobjob_footer',
        'type'    => 'textarea'
    ) );

    // ---------------------------------------------
	$wp_customize->add_section( 'jobjob_main_page', array(
        'title'          => 'Main page',
        'priority'       => 35
    ) );

    $wp_customize->add_setting( 'title_1', array(
        'default'        => 'Search Jobs by category',
    ) );

    $wp_customize->add_control( 'title_1', array(
        'label'   => 'Title 1',
        'section' => 'jobjob_main_page',
        'type'    => 'text'
    ) );


    $wp_customize->add_setting( 'title_2', array(
        'default'        => 'Top employers',

    ) );

    $wp_customize->add_control( 'title_2', array(
        'label'   => 'Title 2',
        'section' => 'jobjob_main_page',
        'type'    => 'text'
    ) );

     $wp_customize->add_setting( 'title_3', array(
        'default'        => 'Jobs of the day'
    ) );

    $wp_customize->add_control( 'title_3', array(
        'label'   => 'Title 3',
        'section' => 'jobjob_main_page',
        'type'    => 'text'
    ) );
 	

 	// ---------------------------------------------
	$wp_customize->add_section( 'jobjob_condition_text', array(
        'title'          => 'Condition text',
        'priority'       => 35
    ) );

    $wp_customize->add_setting( 'condition_text_1', array(
        'default'        => ''
    ) );

    $wp_customize->add_control( 'condition_text_1', array(
        'label'   => 'Text',
        'section' => 'jobjob_condition_text',
        'type'    => 'textarea'
    ) );

    // ---------------------------------------------
	$wp_customize->add_section( 'jobjob_salary_sign', array(
        'title'          => 'Salary sign',
        'priority'       => 35
    ) );

    $wp_customize->add_setting( 'salary_sign_before', array(
        'default'        => ''
    ) );

    $wp_customize->add_control( 'salary_sign_before', array(
        'label'   => 'Before',
        'section' => 'jobjob_salary_sign',
        'type'    => 'text'
    ) );


    $wp_customize->add_setting( 'salary_sign_after', array(
        'default'        => ''
    ) );

    $wp_customize->add_control( 'salary_sign_after', array(
        'label'   => 'After',
        'section' => 'jobjob_salary_sign',
        'type'    => 'text'
    ) );



}





// =====================================================add resume salary field
// Add field to admin
add_filter( 'resume_manager_resume_fields', 'wpjms_admin_resume_form_fields' );
function wpjms_admin_resume_form_fields( $fields ) {
	
	$fields['_candidate_salary'] = array(
	    'label' 		=> __( 'Salary', 'job_manager' ),
	    'type' 			=> 'text',
	    'placeholder' 	=> __( 'Salary', 'job_manager' ),
	    'description'	=> '',
	);

	return $fields;
	
}

// Add field to frontend
add_filter( 'submit_resume_form_fields', 'wpjms_frontend_resume_form_fields' );
function wpjms_frontend_resume_form_fields( $fields ) {
	
	$fields['resume_fields']['candidate_salary'] = array(
	    'label' => __( 'Salary', 'job_manager' ),
	    'type' => 'text',
	    'required' => true,
	    'placeholder' => '',
	    'priority'    => 4
	);

	return $fields;
	
}

// Add a line to the notifcation email with custom field
add_filter( 'apply_with_resume_email_message', 'wpjms_salary_field_email_message', 10, 2 );
function wpjms_salary_field_email_message( $message, $resume_id ) {
  $message[] = "\n" . "Salary: " . get_post_meta( $resume_id, '_candidate_salary', true );  
  return $message;
}

