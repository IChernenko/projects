<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jobjob
 */

get_header(); ?>

<div class="content page-right-sidebar" id="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php single_cat_title( '', true ); ?> - 
						<?php 
                           printf( _n( '%d Job Available', '%d Jobs Available', $wp_query->found_posts ), $wp_query->found_posts ); 
                        ?>

                    </h1>

                </div>
            </div>
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    
                    <div id="right-sidebar-banner"> <?php dynamic_sidebar( 'sidebar_archive' ); ?></div>
                </div>
                <div class="col-md-9 col-md-pull-3 container-blog">
                    <div class="blog_item_wrapper">   
                        <?php if ( have_posts() ) : ?>                     
                         	<?php while ( have_posts() ) : the_post(); ?>
                         		<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
        	                           
                           <?php endwhile;  ?> 
                        <?php endif; ?> 
                    </div>
                <?php the_posts_pagination( array(
                    'mid_size' => 2,
                    'prev_text' => __( 'Back', 'textdomain' ),
                    'next_text' => __( 'Onward', 'textdomain' ),
                )); ?>    
              <!--   <div class="row">
                    <div class="col-md-9 more-btn">
                        <input type="button" class="btn btn-default" id="blog-more" value="Показать еще новости">
                    </div>
                </div> -->
                    
                </div>
                
            </div>
        </div>
    </div>
    <?php global $wp; ?>


    

<?php get_footer(); ?>
