<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jobjob
 */

?>

	<footer id="footer">
            <div class="container">
                <div class="row footer-row">
                    <div class="col-md-3 bottom1">


                        <?php echo nl2br(get_theme_mod( 'footer_left_text' )); ?>

                    </div>
                    <div class="col-md-2 bottom2 footer_menu_container">
                        <?php 
                            wp_nav_menu(array(
                                'theme_location' => 'footer-1_menu'
                            ));
                        ?>
                    </div>
                    <div class="col-md-2 bottom3 footer_menu_container">
                        <?php 
                            wp_nav_menu(array(
                                'theme_location' => 'footer-2_menu'
                            ));
                        ?>
                    </div>
                    <div class="col-md-2 bottom4 footer_menu_container">
                        <?php 
                            wp_nav_menu(array(
                                'theme_location' => 'footer-3_menu'
                            ));
                        ?>
                    </div>
                    <div class="col-md-3 text-right footer-col store">
                    <?php
                        wp_nav_menu(array(
                            'theme_location' => 'footer-4_menu'
                        ));
                    ?>
                       <!--  <a href="#"><img src="img/google%20play.png" alt=""></a>
                        <a href="#"><img src="img/app%20store.png" alt=""></a> -->
                    </div>
                </div>
            </div>
        </footer>

<?php wp_footer(); ?>

</body>
</html>
