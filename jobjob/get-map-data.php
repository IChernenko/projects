<?php    
    $coord = array();
    $args = array(  
        'post_type' => 'job_listing',
        'posts_per_page' => '-1'
     );
    query_posts( $args ); 
    while ( have_posts() ) : the_post();
    $lat = get_post_meta( get_the_ID(), 'geolocation_lat', true );
    $lng = get_post_meta( get_the_ID(), 'geolocation_long', true );
    $coord[$lat] = $lng;
    endwhile;  
    wp_reset_query();

    echo json_encode($coord);
// echo 'some text';
?>