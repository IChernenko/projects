<div class="row">
    <div class="col-md-12">
        <h1><?php
				$count_posts = wp_count_posts('job_listing');
				// echo $count_posts->publish;
				printf( _n( '%d Job Available', '%d Jobs Available', $count_posts->publish ), $count_posts->publish ); 
			?> 
		</h1>
		<form class="job_filters">
	        <div class="sort-control">
	            <ul class="nav nav-pills search_tab ">
	                <li class="active" ><a href="date" data-toggle="tab">By date</a></li>
	                <li class=""><a href="salary" data-toggle="tab">By salary</a></li>
	            </ul>
	            <div class="btn-group">
	              	<select name="sort_additional_date" class="selectpicker job-manager-filter search_select search_select_date search-additional-active">
			    		<option value="date_less">date_less</option>
			    		<option value="date_more">date_more</option>
			    	</select>
			    	<select name="sort_additional_sallary" class="selectpicker job-manager-filter search_select search_select_salary">
			    		<option value="salary_less">none</option>
			    		<option value="salary_less">salary_less</option>
			    		<option value="salary_more">salary_more</option>
			    	</select>	             
	            </div>
	        </div>
	        <input type="hidden" id="activeTab" name="activeThing" value="date"/>
		</form>
    </div>
</div>
<div class="col-md-3 right-sidebar col-md-push-9 job_custom_filter_search">

<?php wp_enqueue_script( 'wp-job-manager-ajax-filters' ); ?>

<?php do_action( 'job_manager_job_filters_before', $atts ); ?>

<form class="job_filters">
	<?php do_action( 'job_manager_job_filters_start', $atts ); ?>

	<div class="search_jobs">
		<?php do_action( 'job_manager_job_filters_search_jobs_start', $atts ); ?>
		<div class="search_keywords">
			<label>Keywords</label>
			<input class="keywords_input" type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Keywords', 'wp-job-manager' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
		</div>
 		
		<?php if ( $categories ) : ?>
			<?php foreach ( $categories as $category ) : ?>
				<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
			<?php endforeach; ?>
		<?php elseif ( $show_categories && ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) : ?>
			<div class="search_categories">
				<label for="search_categories">Category</label>
				<?php if ( $show_category_multiselect ) : ?>
					<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
				<?php else : ?>
					<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => __( 'Any category', 'wp-job-manager' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>


		<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>
	</div>

	<!-- <?php do_action( 'job_manager_job_filters_end', $atts ); ?> -->
</form>

<?php do_action( 'job_manager_job_filters_after', $atts ); ?>

<noscript><?php _e( 'Your browser does not support JavaScript, or it is disabled. JavaScript must be enabled in order to view listings.', 'wp-job-manager' ); ?></noscript>
</div>
