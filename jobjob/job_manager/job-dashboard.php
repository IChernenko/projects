<div class="content page-left-sidebar" id="lk">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="left-sidebar">
                    <?php 
                    	if ( has_nav_menu( 'accaunt_menu' ) ) {
					    	wp_nav_menu(array(
					    		'theme_location' => 'accaunt_menu',
					    		'container' => false
					    	));
				    	}
				    ?>
                </div>
            </div>
            <div class="col-md-9" id="main">
                       
                <?php if ( $jobs ) : ?>
			<div class="blog_item_wrapper">   
                <?php foreach ( $jobs as $job ) : ?>
                <article class="post">
	                <div class="page-items">
	                    <div class="item-left-part">
	                        <h4 class="item-title">
	                        	<?php if ( $job->post_status == 'publish' ) : ?>
									<a href="<?php echo get_permalink( $job->ID ); ?>"><?php echo $job->post_title; ?></a>
								<?php else : ?>
									<?php echo $job->post_title; ?> <small>(<?php the_job_status( $job ); ?>)</small>
								<?php endif; ?>
							</h4>	
							<p class="company"><span>Company:</span><a href="<?php echo company_url(the_company_name('', '', false, $job)); ?>"><?php echo get_the_company_name($job); ?></a></p>						
	                       <p class="item-description"><?php echo get_the_company_tagline($job); ?></p>
        					<p class="item-description"><?php echo $job->post_excerpt; ?></p>
        				
	                        
	                        
	                    </div>
	                    <div class="item-right-part">
	                         <h4 class="price"><?php echo get_post_meta( $job-ID, '_job_salary', true ); ?></h4>
	                        <p class="time">
								<?php echo date_i18n( get_option( 'date_format' ), strtotime( $job->post_date ) ); ?>
	                        </p>
	                      <!--   <p class="time">
								Expires: <?php echo $job->_job_expires ? date_i18n( get_option( 'date_format' ), strtotime( $job->_job_expires ) ) : '&ndash;'; ?>
	                        </p> -->
	                        <!-- <a href="#" class="edit-link">Редактировать</a><a href="#" class="delete-link">Удалить</a> -->
	                        <?php
								$actions = array();

								switch ( $job->post_status ) {
									case 'publish' :
										$actions['edit'] = array( 'label' => __( 'Edit', 'wp-job-manager' ), 'nonce' => false );

										if ( is_position_filled( $job ) ) {
											$actions['mark_not_filled'] = array( 'label' => __( 'Mark not filled', 'wp-job-manager' ), 'nonce' => true );
										} else {
											$actions['mark_filled'] = array( 'label' => __( 'Mark filled', 'wp-job-manager' ), 'nonce' => true );
										}
										break;
									case 'expired' :
										if ( job_manager_get_permalink( 'submit_job_form' ) ) {
											$actions['relist'] = array( 'label' => __( 'Relist', 'wp-job-manager' ), 'nonce' => true );
										}
										break;
									case 'pending_payment' :
									case 'pending' :
										if ( job_manager_user_can_edit_pending_submissions() ) {
											$actions['edit'] = array( 'label' => __( 'Edit', 'wp-job-manager' ), 'nonce' => false );
										}
									break;
								}

								$actions['delete'] = array( 'label' => __( 'Delete', 'wp-job-manager' ), 'nonce' => true );
								$actions           = apply_filters( 'job_manager_my_job_actions', $actions, $job );

								foreach ( $actions as $action => $value ) {
									$action_url = add_query_arg( array( 'action' => $action, 'job_id' => $job->ID ) );
									if ( $value['nonce'] ) {
										$action_url = wp_nonce_url( $action_url, 'job_manager_my_job_actions' );
									}
									echo '<a href="' . esc_url( $action_url ) . '" class="job-dashboard-action-' . esc_attr( $action ) . '">' . esc_html( $value['label'] ) . '</a> &nbsp';
								}
							?>

	                    </div>
	                </div>
	            </article>             
                <?php endforeach; ?>
            </div>
				<?php endif; ?>  	
				<nav class="navigation">			
					<?php get_job_manager_template( 'pagination.php', array( 'max_num_pages' => $max_num_pages ) ); ?>       
				</nav>
            </div>

        </div> 
    </div>
</div>


