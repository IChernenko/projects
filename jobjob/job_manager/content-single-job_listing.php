<div class="content page-right-sidebar" id="page-vacancy-full-description">        
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    <?php dynamic_sidebar('sidebar_single_job'); ?>
                </div>
                <div class="col-md-9 col-md-pull-3">
                    <div class="page-items full-description">
                        <div class="item-left-part">
                            <h4 class="item-title"><?php the_title(); ?></h4>
                             
                            <!-- <img src="/img/Rectangle%2014.png" alt=""> -->
                            <div class="job_logo_img"><?php the_company_logo(); ?></div>   
                            
                            <p class="company"><span>Company:</span> <a href="<?php echo company_url(the_company_name('', '', false)); ?>"> <?php the_company_name(); ?></a></p>
                            <p class="company">

                             <?php 
                                // $terms = get_terms( 'job_listing_region' );
                               
                                    $term_list = wp_get_post_terms($post->ID, 'job_listing_region', array("fields" => "all"));
                                    if ( ! empty( $term_list ) && ! is_wp_error( $term_list ) ){
                                        $separator = ', ';
                                        $output = '';
                                        
                                        foreach ($term_list as $key) {
                                            $term_link = get_term_link( $key );
                                            $output .= '<a href="' . esc_url( $term_link ) . '">' . $key->name . '</a> ' . $separator;
                                         
                                        }
                                        echo trim( $output, $separator );
                               
                                    }


                                    
                                ?>
                            </p>

                            <p class="company job_type_single"> <b>Type: </b>
                                <?php 
                                   $term_list = wp_get_post_terms($post->ID, 'job_listing_type', array("fields" => "all"));
                                    $separator = ', ';
                                    $output = '';
                                    foreach ($term_list as $key) {
                                        $term_link = get_term_link( $key );
                                        $output .= '<a href="' . esc_url( $term_link ) . '">' . $key->name . '</a> ' . $separator;
                                        
                                    }
                                    echo trim( $output, $separator );
                                ?>
                            </p>
                            <p class="company"><b>Requirement: </b><?php echo get_post_meta( get_the_ID(), '_job_requirement', true ); ?></p>
                        </div>
                        <div class="item-right-part">
                            <h4 class="price"><?php echo  get_theme_mod( 'salary_sign_before' ); ?> <?php echo get_post_meta( get_the_ID(), '_job_salary', true ); ?> <?php echo  get_theme_mod( 'salary_sign_after' ); ?></h4>
                           <p class="time"><date><?php printf( human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ); ?></date></p>
                            <!-- <a href="#" class="edit-link">Редактировать</a><a href="#" class="delete-link">Удалить</a> -->
                            
                            
                            <?php do_action( 'single_job_listing_meta_after' ); ?>
                        </div>
                        <div class="item-content">
                            <h6 class="title-description">Description</h6>                      
                            <?php the_content(); ?>
                            <!-- <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#add-otklik">Откликнуться</a> -->
                             <?php if ( candidates_can_apply() ) : ?>
                                <?php get_job_manager_template( 'job-application.php' ); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        

    </div>

