<?php /**  * The template for displaying archive pages.  *  * @link
https://codex.wordpress.org/Template_Hierarchy  *  * @package jobjob  */

get_header(); ?>

   
<?php while ( have_posts() ) : the_post(); ?>
<div class="page-items ">
    <div class="item-left-part">
        <h4 class="item-title"><a href="<?php the_job_permalink(); ?>"><?php the_title(); ?></a></h4>
        <p class="company"><span>Company:</span><a href="<?php echo company_url(the_company_name('', '', false)); ?>"><?php the_company_name(); ?></a></p>
        
        
        <!-- <p class="item-description"><?php the_company_tagline(); ?></p> -->
        <p class="item-description"><?php the_excerpt(); ?></p>
        
        <p class="city">
         <?php 
               $term_list = wp_get_post_terms($post->ID, 'job_listing_region', array("fields" => "all"));
                if ( ! empty( $term_list ) && ! is_wp_error( $term_list ) ){
                    $separator = ', ';
                    $output = '';
                    foreach ($term_list as $key) {
                        $term_link = get_term_link( $key );
                        $output .= '<a href="' . esc_url( $term_link ) . '">' . $key->name . '</a> ' . $separator;
                        
                    }
                    echo trim( $output, $separator );
                }
            ?>
        </p>
    </div>
   
    <div class="item-right-part">
    <?php do_action( 'job_listing_meta_start' ); ?>
       <?php 
        if ( get_post_meta( get_the_ID(), '_job_salary', true ) != "" ) { ?>
            <h4 class="price"><?php echo  get_theme_mod( 'salary_sign_before' ); ?> <?php echo get_post_meta( get_the_ID(), '_job_salary', true ); ?> <?php echo  get_theme_mod( 'salary_sign_after' ); ?></h4>
    <?php }
    ?>
        
        <p class="time"><?php printf( __( '%s ago', 'wp-job-manager' ), human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ); ?></p>
        <?php do_action( 'job_listing_meta_end' ); ?>        
         
       
    </div>
</div>
        
<?php endwhile;  ?>
    

<?php get_footer(); ?>
