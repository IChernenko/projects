<?php /**  * The template for displaying archive pages.  *  * @link
https://codex.wordpress.org/Template_Hierarchy  *  * @package jobjob  */

get_header(); ?>




<?php 
	$args = array('post_type' => 'resume',
		'posts_per_page' => '-1',
        'tax_query' => array(
            array(
                'taxonomy' => 'resume_category',
                'field' => 'slug',
                'terms' => $_GET['resume_category']
            ),
        ),

     );
	$loop = new WP_Query($args);    
?>


<div class="content page-right-sidebar" id="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php single_cat_title( '', true ); ?> - 
                        <?php 
                           printf( _n( '%d resume Available', '%d resumes Available', $loop->found_posts ), $loop->found_posts ); 
                        ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    
                    <div id="right-sidebar-banner">
                        <?php dynamic_sidebar( 'sidebar_archive' ); ?>
                    </div>
                </div>
                <div class="col-md-9 col-md-pull-3 container-blog container-soiskately">
                <div class="blog_item_wrapper">   
                    <?php 
    					if( $loop->have_posts() ) {
    						while($loop->have_posts()) : $loop->the_post(); ?>
    				           <?php get_template_part( 'template-parts/content', 'resume_listing' ); ?> 
    				    	<?php endwhile;
    				    }
                    ?>
                </div>
				
               
                <?php the_posts_pagination( array(
                    'mid_size' => 2,
                    'prev_text' => __( 'Back', 'textdomain' ),
                    'next_text' => __( 'Onward', 'textdomain' ),
                )); ?>    
             <div class="row">
                    <div class="col-md-9 more-btn">
                        <input type="button" class="btn btn-default" id="blog-more" value="Показать еще новости">
                    </div>
                </div>
                    
                </div>
                
            </div>
        </div>
    </div>


<?php get_footer(); ?>
