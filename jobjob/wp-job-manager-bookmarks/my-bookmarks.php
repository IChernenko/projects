

<div class="content page-left-sidebar" id="lk">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="left-sidebar">
                        	<?php 
						    	wp_nav_menu(array(
						    		'theme_location' => 'accaunt_menu',
						    		'container' => false
						    	));
						    ?>
                           <!--  <ul>
                                <li class="active"><a href="#">Вакансии</a></li>
                                <li><a href="#">Уведомления</a></li>
                                <li><a href="#">Закладки</a></li>
                                <li><a href="#">Личные данные</a></li>
                            </ul> -->
                        </div>
                    </div>
                    <div class="col-md-9 bookmark_font_size">
                               <div class="blog_item_wrapper">   
                        <?php 
							foreach ( $bookmarks as $bookmark ) : 
								if ( get_post_status( $bookmark->post_id ) !== 'publish' ) {
									continue;
								}
							$has_bookmark = true;

						?>
                        
                        
                        
                        <div class="page-items">
                            <div class="item-left-part">
                                <h4 class="item-title"><?php echo '<a href="' . get_permalink( $bookmark->post_id ) . '">' . get_the_title( $bookmark->post_id ) . '</a>'; ?></h4>
                                <!-- <p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p> -->
                                <p class="item-description"><?php echo wpautop( wp_kses_post( $bookmark->bookmark_note ) ); ?></p>
                                <!-- <p class="city"><a href="#">Москва</a></p> -->
                            </div>
                            <div class="item-right-part">
                                <!-- <h4 class="price">30 000–55 000 Р</h4> -->
                                <!-- <p class="time">3 минуты назад</p> -->
                                <!-- <a href="#" class="edit-link">Редактировать</a><a href="#" class="delete-link">Удалить</a> -->
                                <?php
								$actions = apply_filters( 'job_manager_bookmark_actions', array(
									'delete' => array(
										'label' => __( 'Delete', 'wp-job-manager-bookmarks' ),
										'url'   =>  wp_nonce_url( add_query_arg( 'remove_bookmark', $bookmark->post_id ), 'remove_bookmark' )
									)
								), $bookmark );

								foreach ( $actions as $action => $value ) {
									echo '<a href="' . esc_url( $value['url'] ) . '" class="job-manager-bookmark-action-' . $action . '">' . $value['label'] . ' bookmark</a> ';
								}
							?>
                            </div>
                        </div>  
						<?php endforeach; ?> 
                    </div>
                    </div>
                </div> 
            </div>
        </div>