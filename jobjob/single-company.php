<?php
/**
 * Single Post
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package jobjob
 * @since jobjob 1.0
 */

get_header(); ?>

	<?php the_post(); ?>
	<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title"><?php the_company_name(); ?> - <?php printf( _n( '%d Job Available', '%d Jobs Available', $wp_query->found_posts ), $wp_query->found_posts ); ?> </h1>
						
		<!-- <h2 class="page-subtitle"></h2> -->
                </div>
            </div>
        </div>
	
	<?php rewind_posts(); ?>
	



	<div id="primary" class="content-area">
		<div id="content" class="container" role="main">
			<div class="company-profile row">
			<div class="col-md-3 right-sidebar col-md-push-9">                    
                <div id="right-sidebar-banner"> <?php dynamic_sidebar( 'sidebar_company' ); ?></div>
            </div>
				<div class="col-md-9 col-md-pull-3 container-blog">
				<div class="page-items first-other-items">
                        <div class="col-sm-5 item-other-left-part single_company_logo">
                            <?php the_company_logo(); ?>
                        </div>
                        <div class="col-sm-7 item-other-right-part">
                            <h6><?php echo get_the_company_name() ?></h6>
                            <?php if ( get_the_company_website() ) : ?>
								<a href="<?php echo get_the_company_website(); ?>" itemprop="url">
									<i class="icon-link"></i>
									<?php _e( 'Website' ); ?>
								</a><br>
						<?php endif; ?>
				
						<?php if ( get_the_company_twitter() ) : ?>
							<a href="http://twitter.com/<?php echo get_the_company_twitter(); ?>">
								<i class="icon-twitter"></i>
								<?php _e( 'Twitter' ); ?>
							</a>
						<?php endif; ?> <br> <br>
                            <?php the_company_tagline(); ?>
                        </div>
                    </div>
					
					<?php if ( have_posts() ) : ?>
					<div class="job_listings">
						<ul class="job_listings">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
							<?php endwhile; ?>
						</ul>
					</div>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>

				

			</div>
		</div><!-- #content -->

		
	</div><!-- #primary -->

<?php get_footer(); ?>