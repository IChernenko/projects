<div class="row">
    <div class="col-md-12">
        <h1><?php
				$count_posts = wp_count_posts('resume');
				// echo $count_posts->publish;
				printf( _n( '%d Resume Available', '%d Resumes Available', $count_posts->publish ), $count_posts->publish ); 
			?> 
		</h1>
		<!-- <form class="job_filters">
	        <div class="sort-control">
	            <ul class="nav nav-pills search_tab ">
	                <li class="active" ><a href="date" data-toggle="tab">По дате</a></li>
	                <li class=""><a href="salary" data-toggle="tab">По зарплате</a></li>
	            </ul>
	            <div class="btn-group">
	              	<select name="sort_additional_date" class="selectpicker job-manager-filter search_select search_select_date search-additional-active">
			    		<option value="date_less">date_less</option>
			    		<option value="date_more">date_more</option>
			    	</select>
			    	<select name="sort_additional_sallary" class="selectpicker job-manager-filter search_select search_select_salary">
			    		<option value="salary_less">salary_less</option>
			    		<option value="salary_more">salary_more</option>
			    	</select>	             
	            </div>
	        </div>
	        <input type="hidden" id="activeTab" name="activeThing" value="date"/>
		</form> -->
    </div>
</div>
<div class="col-md-3 right-sidebar col-md-push-9 job_custom_filter_search">

	<?php wp_enqueue_script( 'wp-resume-manager-ajax-filters' ); ?>

	<?php do_action( 'resume_manager_resume_filters_before', $atts ); ?>

	<form class="resume_filters">

		<div class="search_resumes">
			<?php do_action( 'resume_manager_resume_filters_search_resumes_start', $atts ); ?>

			<div class="search_keywords resume-filter">
				<label for="search_keywords"><?php _e( 'Keywords', 'wp-job-manager-resumes' ); ?></label>
				<input class="form-control keywords_input" type="text" name="search_keywords" id="search_keywords" placeholder="<?php _e( 'All Resumes', 'wp-job-manager-resumes' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
			</div>

			<div class="search_location resume-filter" >
				<label for="search_location"><?php _e( 'Location', 'wp-job-manager-resumes' ); ?></label>
				<input class="form-control keywords_input" type="text" name="search_location" id="search_location" placeholder="<?php _e( 'Any Location', 'wp-job-manager-resumes' ); ?>" value="<?php echo esc_attr( $location ); ?>" />
			</div>

			<?php if ( $categories ) : ?>
				<?php foreach ( $categories as $category ) : ?>
					<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
				<?php endforeach; ?>
			<?php elseif ( $show_categories && get_option( 'resume_manager_enable_categories' ) && ! is_tax( 'resume_category' ) && get_terms( 'resume_category' ) ) : ?>
				<div class="search_categories resume-filter">
					<label for="search_categories"><?php _e( 'Category', 'wp-job-manager-resumes' ); ?></label>
					<?php if ( $show_category_multiselect ) : ?>
						<?php job_manager_dropdown_categories( array( 'taxonomy' => 'resume_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
					<?php else : ?>
						<?php wp_dropdown_categories( array( 'taxonomy' => 'resume_category', 'hierarchical' => 1, 'show_option_all' => __( 'Any category', 'wp-job-manager-resumes' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category ) ); ?>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php do_action( 'resume_manager_resume_filters_search_resumes_end', $atts ); ?>
		</div>
		<!-- <div class="showing_resumes"></div> -->
	</form>

	<?php do_action( 'resume_manager_resume_filters_after', $atts ); ?>














    <!-- <label class="right-sidebar-checkbox">
        <input type="checkbox"> Только вакансии с указанной з/п
    </label>
    <label class="control-label">График работы</label>
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Любой<span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#">Полная занятость</a></li>
            <li><a href="#">Не полная занятость</a></li>
            <li><a href="#">Подработка</a></li>
            <li><a href="#">Удаленная работа</a></li>
        </ul>
    </div>
    <div class="right-sidebar-checkbox type-work">
        <label>
            <input type="checkbox"> Временная работа
        </label>
    </div>
    <label class="control-label">Опыт работы</label>
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Неважно<span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#">Туризм</a></li>
            <li><a href="#">Туризм</a></li>
            <li><a href="#">Туризм</a></li>
        </ul>
    </div>
    <label class="control-label">Отрасль</label>
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Туризм<span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#">Туризм</a></li>
            <li><a href="#">Туризм</a></li>
            <li><a href="#">Туризм</a></li>
        </ul>
    </div>
    <label class="control-label">Источник вакансий</label>
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Все<span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#">Все</a></li>
            <li><a href="#">Все</a></li>
            <li><a href="#">Все</a></li>
        </ul>
    </div> -->
   
                
</div>




