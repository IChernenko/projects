<?php
global $resume_preview;

if ( $resume_preview ) {
	return;
}

if ( resume_manager_user_can_view_contact_details( $post->ID ) ) :
	
	?>
	<div class="resume_contact">
		<input data-toggle="modal" data-target="#add-otklik" class="resume_contact_button btn btn-success btn-block apply_button" type="button" value="<?php _e( 'Contact', 'wp-job-manager-resumes' ); ?>" />

		<!-- <div class="resume_contact_details">
			<?php do_action( 'resume_manager_contact_details' ); ?>
		</div> -->
	</div>
<?php else : ?>
	
	<?php get_job_manager_template_part( 'access-denied', 'contact-details', 'wp-job-manager-resumes', RESUME_MANAGER_PLUGIN_DIR . '/templates/' ); ?>

<?php endif; ?>


<div class="modal" id="add-otklik">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="application_details">
        	<?php do_action( 'resume_manager_contact_details' ); ?>
        </div>
        </div>
    </div>
</div>
