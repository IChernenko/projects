<?php if ( resume_manager_user_can_view_resume( $post->ID ) ) : ?>

<div class="content page-right-sidebar" id="page-vacancy-full-description">

       
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    <?php dynamic_sidebar( 'sidebar_single_resume' ); ?>
                </div>
                <div class="col-md-9 col-md-pull-3">
                    <div class="page-items full-description">
                        <div class="item-left-part">
                        <div class="resume_foto">
                            <?php the_candidate_photo(); ?>
                           
                        </div>
                       

                        <div class="item-title"><a href="<?php the_resume_permalink(); ?>"><?php the_title(); ?></a></div>
                      
                        
                        <p class="resume_skill"><span>Skills:</span>
                            <?php 
                               $term_list = wp_get_object_terms($post->ID, 'resume_skill', array("fields" => "all"));
                               // var_dump($term_list);
                               $separator = ', ';
                               $output = '';
                                foreach ($term_list as $key) {
                                    $term_link = get_term_link( $key );
                                    $output .= '<a href="' . esc_url( $term_link ) . '">' . $key->name . '</a> ' . $separator;
                                    
                                }
                                echo trim( $output, $separator );
                            ?>
                        </p> 
                        <p class="city"><?php the_candidate_location( false ); ?></p>
                        
                        
                    </div>
                        
						
		
                    <div class="item-right-part">
                        <?php 
                            if ( get_post_meta( get_the_ID(), '_candidate_salary', true ) != "" ) { ?>
                              <h4 class="price"><?php echo  get_theme_mod( 'salary_sign_before' ); ?> <?php echo get_post_meta( get_the_ID(), '_candidate_salary', true ); ?> <?php echo  get_theme_mod( 'salary_sign_after' ); ?></h4>
                          <?php   }
                          ?>
                        <p class="time single_resume_time"><date><?php printf( __( 'Updated %s ago', 'wp-job-manager-resumes' ), human_time_diff( get_the_modified_time( 'U' ), current_time( 'timestamp' ) ) ); ?></date></p>
                        <?php do_action( 'single_resume_start' ); ?>
                    </div>
                      
                        
												 
                        <div class="candidate_title candidate_single_title"><?php the_candidate_title(  ); ?></div>
						 <!-- =================education start=============== -->

						<?php if ( $items = get_post_meta( $post->ID, '_candidate_education', true ) ) : ?>
							<h3><?php _e( 'Education', 'wp-job-manager-resumes' ); ?></h3>
							<dl class="resume-manager-education">
							<?php
								foreach( $items as $item ) : ?>									
									<div class="date">Start: <?php echo esc_html( $item['date'] ); ?></div>									
									<div><?php echo $item['qualification'] ?> at <?php  echo esc_html( $item['location']);  ?></div>
									<?php echo wptexturize( $item['notes'] ); ?>
                                    <br>
                                    <br>
								<?php endforeach;
							?>
							</dl>
						<?php endif; ?>
						<!-- =================experience start=============== -->
                        <?php if ( $items = get_post_meta( $post->ID, '_candidate_experience', true ) ) : ?>
							<h3><?php _e( 'Experience', 'wp-job-manager-resumes' ); ?></h3>
							<dl class="resume-manager-experience">
							<?php
								foreach( $items as $item ) : ?>									
									<div class="date">Start: <?php echo esc_html( $item['date'] ); ?></div>									
									<div><?php echo esc_html( $item['job_title'] ); ?> -  <?php echo  esc_html( $item['employer'] ); ?></div>
									<?php echo wptexturize( $item['notes'] ); ?>
                                    <br>
                                    <br>
								<?php endforeach;
							?>
							</dl>
						<?php endif; ?>
						<!-- =================description start=============== -->
                        <div class="item-content">
                            <h6 class="title-description">Description</h6>
                            <?php echo apply_filters( 'the_resume_description', get_the_content() ); ?>
                            <!-- <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#add-otklik">Откликнуться</a> -->
                            <?php get_job_manager_template( 'contact-details.php', array( 'post' => $post ), 'wp-job-manager-resumes', RESUME_MANAGER_PLUGIN_DIR . '/templates/' ); ?>
                        </div>
                    </div>
                </div>
            </div>
     
    </div>


		

	
    <?php else : ?>

	<?php get_job_manager_template_part( 'access-denied', 'single-resume', 'wp-job-manager-resumes', RESUME_MANAGER_PLUGIN_DIR . '/templates/' ); ?>

<?php endif; ?>