<?php
$submission_limit           = get_option( 'resume_manager_submission_limit' );
$submit_resume_form_page_id = get_option( 'resume_manager_submit_resume_form_page_id' );
?>



<div class="content page-left-sidebar" id="lk">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="left-sidebar">
                    <?php 
				    	wp_nav_menu(array(
				    		'theme_location' => 'accaunt_menu',
				    		'container' => false
				    	));
				    ?>
                </div>
            </div>
            <div class="col-md-9 container-soiskately"  id="main">
	                   
            <?php if ( $resumes ) : ?>
			<div class="blog_item_wrapper">   
                <?php foreach ( $resumes as $resume ) : ?>
                	<article class="post">
	                <div class="page-items">
	                    <div class="item-left-part">
	                     <div class="resume_foto">
                            <?php the_candidate_photo('', '', $resume);	?>
                            <p class="city"><?php the_candidate_location( false, $resume ); ?></p>
                        </div>
	                    
	                        <div class="item-title">
	                        	<?php if ( $resume->post_status == 'publish' ) : ?>
								<a href="<?php echo get_permalink( $resume->ID ); ?>"><?php echo esc_html( $resume->post_title ); ?></a>
								<?php else : ?>
									<?php echo esc_html( $resume->post_title ); ?> <small>(<?php the_resume_status( $resume ); ?>)</small>
								<?php endif; ?>
							</div>	
							 <div class="candidate_title"><?php echo get_the_candidate_title( $resume ); ?></div>
							
					            <?php 
					               $term_list = wp_get_object_terms($resume->ID, 'resume_skill', array("fields" => "all"));
					               if ( ! empty( $term_list ) && ! is_wp_error( $term_list ) ){ ?>
					               <p class="resume_skill"><span>Skills:</span>
						            <?php   
						               $separator = ', ';
						               $output = '';
						                foreach ($term_list as $key) {
						                    $term_link = get_term_link( $key );
						                    $output .= '<a href="' . esc_url( $term_link ) . '">' . $key->name . '</a> ' . $separator;
						                    
						                }
						                echo trim( $output, $separator );
						            }
					            ?>
					        </p>	
												
	                        <!-- <p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p> -->
	                        <!-- <p class="company"><span></span><a href="#"><?php the_resume_category( $resume ); ?></a></p> -->
	                        <!-- <?php the_candidate_photo(); ?> -->
	                        
	                        <!-- <p class="city"><a href="#"><?php the_candidate_location( false, $resume ); ?></a></p> -->
	                    </div>
	                    <div class="item-right-part">
	                        <h4 class="price"><?php echo get_post_meta( $resume->ID, '_candidate_salary', true ); ?></h4>
	                        <p class="time">
								<?php 
									if ( ! empty( $resume->_resume_expires ) && strtotime( $resume->_resume_expires ) > current_time( 'timestamp' ) ) {
										printf( __( 'Expires %s', 'wp-job-manager-resumes' ), date_i18n( get_option( 'date_format' ), strtotime( $resume->_resume_expires ) ) );
									} else {
										echo date_i18n( get_option( 'date_format' ), strtotime( $resume->post_date ) );
									}
									
								?>
	                        </p>
	                        <!-- <a href="#" class="edit-link">Редактировать</a><a href="#" class="delete-link">Удалить</a> -->
	                        <?php
								$actions = array();

								switch ( $resume->post_status ) {
									case 'publish' :
										$actions['edit'] = array( 'label' => __( 'Edit', 'wp-job-manager-resumes' ), 'nonce' => false );
										$actions['hide'] = array( 'label' => __( 'Hide', 'wp-job-manager-resumes' ), 'nonce' => true );
									break;
									case 'hidden' :
										$actions['edit'] = array( 'label' => __( 'Edit', 'wp-job-manager-resumes' ), 'nonce' => false );
										$actions['publish'] = array( 'label' => __( 'Publish', 'wp-job-manager-resumes' ), 'nonce' => true );
									break;
									case 'expired' :
										if ( get_option( 'resume_manager_submit_resume_form_page_id' ) ) {
											$actions['relist'] = array( 'label' => __( 'Relist', 'wp-job-manager-resumes' ), 'nonce' => true );
										}
									break;
								}

								$actions['delete'] = array( 'label' => __( 'Delete', 'wp-job-manager-resumes' ), 'nonce' => true );

								$actions = apply_filters( 'resume_manager_my_resume_actions', $actions, $resume );

								foreach ( $actions as $action => $value ) {
									$action_url = add_query_arg( array( 'action' => $action, 'resume_id' => $resume->ID ) );
									if ( $value['nonce'] )
										$action_url = wp_nonce_url( $action_url, 'resume_manager_my_resume_actions' );
									echo ' <a href="' . $action_url . '" class="candidate-dashboard-action-' . $action . '">' . $value['label'] . '</a> &nbsp';
								}
							?>

	                    </div>
	                </div>
	            </article>
                <?php endforeach; ?>
            </div>
            	<nav class="navigation">
                	<?php get_job_manager_template( 'pagination.php', array( 'max_num_pages' => $max_num_pages ) ); ?>
                </nav>
				<?php endif; ?>         
            </div>

        </div> 
    </div>
</div>
