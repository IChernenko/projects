<article class="post">
<div class="page-items">
    <div class="item-left-part">
        <div class="resume_foto">
              <?php the_candidate_photo(); ?>
             
          </div>

        <div class="item-title"><a href="<?php the_resume_permalink(); ?>"><?php the_title(); ?></a></div>
        <div class="candidate_title"><?php the_candidate_title(  ); ?></div>
        <p class="resume_skill resume_skill_listing"><span>Skills:</span>
            <?php 
               $term_list = wp_get_object_terms($post->ID, 'resume_skill', array("fields" => "all"));
               // var_dump($term_list);
               $separator = ', ';
               $output = '';
                foreach ($term_list as $key) {
                    $term_link = get_term_link( $key );
                    $output .= '<a href="' . esc_url( $term_link ) . '">' . $key->name . '</a> ' . $separator;
                    
                }
                echo trim( $output, $separator );
            ?>
        </p>
         <p class="city listing_resume_city"><?php the_candidate_location( false ); ?></p>
        
    </div>

    <div class="item-right-part single_resume_item_right">
      <?php 
        if ( get_post_meta( get_the_ID(), '_candidate_salary', true ) != "" ) { ?>
          <h4 class="price"><?php echo  get_theme_mod( 'salary_sign_before' ); ?> <?php echo get_post_meta( get_the_ID(), '_candidate_salary', true ); ?> <?php echo  get_theme_mod( 'salary_sign_after' ); ?></h4>
      <?php   }
      ?>
        
        <p class="time resume_listing_time"><date><?php printf( __( '%s ago', 'wp-job-manager-resumes' ), human_time_diff( get_post_time( 'U' ), current_time( 'timestamp' ) ) ); ?></date></p>
        <!-- <a href="#" class="add-favorite" data-toggle="modal" data-target="#add-bookmark"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a> -->
    </div>
 

</div>

</article>