<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jobjob
 */

?>



<div class="content page-right-sidebar" id="page-vacancy-full-description">       
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    <?php dynamic_sidebar( 'sidebar_single_blog' ); ?>
                </div>
                <div class="col-md-9 col-md-pull-3">
                    <div class="page-items full-description">    
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php echo get_the_post_thumbnail(  ); ?>
							<header class="entry-header">
								<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

							
							</header>
							    <p class="blog-item-category"><span>Category: </span>
									<?php 
										$categories = get_the_category();
							    		$separator = ', ';
							    		$output = '';
							    		if ( ! empty( $categories ) ) {
							    		    foreach( $categories as $category ) {
							                $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
										        }
										        echo trim( $output, $separator );
										}
									?>
							    </p>
    						<p class="blog-item-time"><span>Date: </span><?php the_time( get_option( 'date_format' ) ); ?></p>
							
							<?php the_content(); ?>
						</article>
                    </div>
                </div>
            </div>
     
    </div>