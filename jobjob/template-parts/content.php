<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jobjob
 */

?>
<article class="page-items post">
    <h4 class="blog-item-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
    <p class="blog-item-category"><span>Category: </span>
		<?php 
			$categories = get_the_category();
    		$separator = ', ';
    		$output = '';
    		if ( ! empty( $categories ) ) {
    		    foreach( $categories as $category ) {
                $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
        }
        echo trim( $output, $separator );
}
		?>
    </p>
    <p class="blog-item-time"><span>Date: </span><?php the_time( get_option( 'date_format' ) ); ?></p>
    <div class="blog-item-description">
        <!-- <img src="/img/rabota.png" alt=""> -->
   
     <?php  if ( has_post_thumbnail( $_post->ID ) ) {
        echo '<a href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
        echo get_the_post_thumbnail( $_post->ID );
        echo '</a>';
    } ?>
     <?php the_excerpt(); ?> 

    </div>

</article>

	
