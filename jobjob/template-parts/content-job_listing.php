<?php
/**
 * job listning single
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package jobjob
 */

 ?>


	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content">		
			<?php the_content(); ?>		
		</div>
	</article>
