<?php
/**
 * 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jobjob
 */

?>

<section id="count">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center">
                <?php $jobs = getJobs('-1'); ?>
                <?php $job_count = count($jobs); ?>
                <?php if ( $job_count > 0 ) { ?>
                     <h2><?php echo $job_count - 1; ?></h2>    
                <?php } else { ?>
                     <h2>0</h2> 
               <?php } ?>                                       
                <h4>Jobs Posted </h4>
            </div>
            <div class="col-sm-3 text-center">
            <?php $resumes = getResumes('-1'); ?>
             <?php $resume_count = count($resumes); ?>
            <?php if ( $resume_count > 0 ) { ?>
                 <h2><?php echo $resume_count - 1; ?></h2>    
            <?php } else { ?>
                 <h2>0</h2> 
           <?php } ?>
                
           
                <h4>Resumes Posted</h4>
            </div>
             <div class="col-sm-3 text-center">
                <?php $companies = getCompanies('-1'); ?>
                 <?php $company_count = count($companies); ?>
                <?php if ( $company_count > 0 ) { ?>
                     <h2><?php echo $company_count - 1; ?></h2>    
                <?php } else { ?>
                     <h2>0</h2> 
               <?php } ?>               
                <h4>Companies</h4>
            </div>
            <div class="col-sm-3 text-center">
            	<?php $blogusers = get_users(); ?>
                <h2><?php echo count($blogusers); ?></h2>
                <h4>Members</h4>                
            </div>
           
        </div>
    </div>
</section>

