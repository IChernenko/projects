<?php
/**
 * Template Name: contacts
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>


<div class="content" id="contact">
        <section id="sec-map">
            <!-- <div id="map-hide"></div> -->
            <div id="map"></div>
        </section>
        <?php get_template_part( 'template-parts/content', 'statistic' ); ?>

        <div class="container">
            <div class="row" id="page_title">
                <div class="col-md-12 text-center">
                    <h1>Feedback</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- <form>
                        <fieldset>
                            <div class="form-group">
                                <label for="inputName" class="control-label">Имя*</label>
                                <input type="text" class="form-control" id="inputName" placeholder="Имя">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="control-label">Електронная почта*</label>
                                <input type="text" class="form-control" id="inputEmail" placeholder="Почта">
                            </div>
                            <div class="form-group">
                                <label for="textArea" class="control-label">Сообщение*</label>
                                <textarea class="form-control" rows="3" id="textArea"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Отправить</button>
                            </div>
                        </fieldset>
                    </form> -->
                    <?php echo do_shortcode( '[ninja_forms id=12]' ); ?>
                    <!-- <?php echo do_shortcode( '[contact-form-7 id="2278" title="main contact form"]' ); ?> -->
                </div>
            </div>
        </div>

    </div>
<?php get_footer(); ?>