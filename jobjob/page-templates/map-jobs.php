<?php
/**
 * Template Name: Map + Jobs
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>
<div class="content" id="home">
            <section id="sec-map">
                <!-- <div id="map-hide"></div> -->
                <div id="map">
                    
                </div>
            </section>
            <?php get_template_part( 'template-parts/content', 'statistic' ); ?>

                <section id="category">
            <div class="container">
                <div class="row row-title">
                    <?php 
                        if ( get_theme_mod( 'title_1' ) == '' ) { ?>
                            <div class="col-md-12 text-center"><h2>Search Jobs by category</h2></div>                      
                    <?php }  else { ?>
                            <div class="col-md-12 text-center"><h2><?php echo  get_theme_mod( 'title_1' ); ?></h2></div>
                    <?php }         ?>
                    
                </div>
                <div class="row">   
						<!-- 
							=================================================
							list categories in columns
							=================================================						
						 -->
                        <?php 
                        	$terms = get_terms( 'job_listing_category' );
                        	$length = count($terms); //get all categories
                        	$column_number = 3;  // set number of column                      	
                        	$lenght_in_one_column = round($length / $column_number); //dynamic count categories in one column
                        	$offset_length_in_one_column = 0; //offset categories for column

                        	for ( $i = 0; $i < $column_number; $i++ ) {
                        		echo ' <div class="col-sm-4 main_category_list">';

                        		$args = array(
	                        		'offset' => $offset_length_in_one_column,
	                        		'number' => $lenght_in_one_column
	                        	);
	                        	// get categories with offset for next column
                        		$terms = get_terms( 'job_listing_category', $args );
								if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){			     
								     foreach ( $terms as $term ) {
                                        // var_dump($term);  
								        echo '<p><a href="' . esc_url(get_term_link( $term )) . '" title="' . sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) . '">' . $term->name . '</a></p>';			        
								     }			    
								}

                        		echo '</div>';
                        		$offset_length_in_one_column += $lenght_in_one_column;
                        	}							
						?>
						<!-- 
							=================================================
							end list categories in columns
							=================================================						
						-->
                    </div>                        
                </div>                
            </section>
            <section id="employer">
                <div class="container">
                    <div class="row row-title">
                        <?php 
                            if ( get_theme_mod( 'title_1' ) == '' ) { ?>
                                <div class="col-md-12 text-center"><h2>Top employers</h2></div>                      
                        <?php }  else { ?>
                                <div class="col-md-12 text-center"><h2><?php echo get_theme_mod( 'title_2' ); ?></h2></div>
                        <?php }         ?>
                        
                    </div>
                    <div class="row text-center">
                        <?php $companies = getCompanies('-1'); ?>                        
                        <?php $companyCount = 0; ?>
                        <?php foreach ($companies as $key => $value) { ?>
                            <?php if ( $companyCount < 12 ) { ?>
                                <div class="col-md-2 col-sm-4"><a href="<?php echo company_url($key); ?>"><img src="<?php echo $value; ?>" alt="img"> </a></div>                                
                                <?php $companyCount++; ?>
                            <?php } ?>                            
                          <?php  } ?>
                        <?php wp_reset_query(); ?>       
                                                        
                    </div>
                </div>
            </section>
            <section id="vacancy">
                <div class="container">
                    <div class="row row-title">
                        <?php 
                            if ( get_theme_mod( 'title_1' ) == '' ) { ?>
                                <div class="col-md-12 text-center"><h2>Jobs of the day</h2></div>                      
                        <?php }  else { ?>
                                <div class="col-md-12 text-center"><h2><?php echo get_theme_mod( 'title_3' ); ?></h2></div>
                        <?php }         ?>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-8">

                        <!-- <?php var_dump(getJobs(12)); ?> -->
                        <?php 
                            $args = array(  
                                'post_type' => 'job_listing',
                                'order' => 'ASC',
                                'posts_per_page' => '3' 
                             );
                        ?>
                        <?php query_posts( $args ); ?>  
    
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php get_job_manager_template_part( 'content', 'job_listing' ); ?>                           
                        <?php endwhile;  ?>
                        <?php wp_reset_query(); ?>

                        </div>
                        <div class="col-md-4">
                            <div class="home-banner">
                                <?php dynamic_sidebar( 'sidebar_home' ); ?>

                            </div>
                        </div>
                    </div> 
                </div>
            </section>
        </div>
<?php get_footer(); ?>