<?php
/**
 * Template Name: content
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>
<div class="content" id="home">
    <div class="container">
        <div class="container_height">
            <?php the_content(); ?>
        </div>
    </div>            
</div>
<?php get_footer(); ?>