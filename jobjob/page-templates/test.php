<?php
/**
 * Template Name: Map + Jobs
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>


<div class="content" id="home">
            <section id="sec-map">
                <div id="map-hide"></div>
                <div id="map"></div>
            </section>
            <section id="count">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 text-center">
                            <h2>33 300</h2>
                            <h4>vacancies</h4>
                        </div>
                        <div class="col-sm-3 text-center">
                            <h2>66600</h2>
                            <h4>resume</h4>
                        </div>
                        <div class="col-sm-3 text-center">
                            <h2>7888</h2>
                            <h4>specialties</h4>
                        </div>
                        <div class="col-sm-3 text-center">
                            <h2>5000</h2>
                            <h4>employers</h4>
                        </div>
                    </div>
                </div>
            </section>

                <section id="category">
            <div class="container">
                <div class="row row-title">
                    <div class="col-md-12 text-center"><h2>Search Jobs by category</h2></div>
                </div>
                <div class="row">     
						<!-- 
							=================================================
							list categories in columns
							=================================================						
						 -->
                        <?php 
                        	$terms = get_terms( 'job_listing_category' );
                        	$length = count($terms); //get all categories
                        	$column_number = 3;  // set number of column                      	
                        	$lenght_in_one_column = round($length / $column_number); //dynamic count categories in one column
                        	$offset_length_in_one_column = 0; //offset categories for column

                        	for ( $i = 0; $i < $column_number; $i++ ) {
                        		echo ' <div class="col-sm-4 main_category_list">';

                        		$args = array(
	                        		'offset' => $offset_length_in_one_column,
	                        		'number' => $lenght_in_one_column
	                        	);
	                        	// get categories with offset for next column
                        		$terms = get_terms( 'job_listing_category', $args );
								if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){			     
								     foreach ( $terms as $term ) {
								       echo '<p><a href="' . get_term_link( $term ) . '" title="' . sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) . '">' . $term->name . '</a></p>';			        
								     }			    
								}

                        		echo '</div>';
                        		$offset_length_in_one_column += $lenght_in_one_column;
                        	}							
						?>
						<!-- 
							=================================================
							end list categories in columns
							=================================================						
						-->
                    </div>                        
                </div>                
            </section>
            <section id="employer">
                <div class="container">
                    <div class="row row-title">
                        <div class="col-md-12 text-center"><h2>Top employers</h2></div>
                    </div>
                    <div class="row text-center">
                        <?php 
                            $args = array(  
                                'post_type' => 'job_listing',
                                'order' => 'ASC',
                                'posts_per_page' => '12' 
                            );
                        ?>
                        <?php query_posts( $args ); ?>  

                        <?php $companies = array(); ?>

                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php 
                                $company_name = the_company_name('', '', false);                           
                                $company_logo = get_the_company_logo(); 
                           
                                $company_name = the_company_name('', '', false);
                                $companies[$company_name] = $company_logo; 
                            ?>
                           
                            
                        <?php endwhile;  ?>
                        <?php foreach ($companies as $key => $value) { ?>
                            <div class="col-md-2 col-sm-4"><a href="<?php echo company_url($key); ?>"><img src="<?php echo $value; ?>" alt="img"> </a></div>
                          <?php  } ?>
                        <?php wp_reset_query(); ?>       
                                                        
                    </div>
                </div>
            </section>
            <section id="vacancy">
                <div class="container">
                    <div class="row row-title">
                        <div class="col-md-12 text-center"><h2>Jobs of the day</h2></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                        <?php 
                            $args = array(  
                                'post_type' => 'job_listing',
                                'order' => 'ASC',
                                'posts_per_page' => '3' 
                             );
                        ?>
                        <?php query_posts( $args ); ?>  
    
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
                            <!-- <div class="page-items">
                                <div class="item-left-part">
                                    <h4 class="item-title"><a href="#">Менеджер по туризму</a></h4>
                                    <p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p>
                                    <p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p>
                                    <p class="city"><a href="#">Москва</a></p>
                                </div>
                                <div class="item-right-part">
                                    <h4 class="price">30 000–55 000 Р</h4>
                                    <p class="time">3 минуты назад</p>
                                    <a href="#" class="edit-link"><span class="glyphicon glyphicon-bookmark"></span>В избранное</a>
                                </div>
                            </div> -->
                        <?php endwhile;  ?>
                        <?php wp_reset_query(); ?>

                        </div>
                        <div class="col-md-4">
                            <div class="home-banner"></div>
                        </div>
                    </div> 
                </div>
            </section>
        </div>
<?php get_footer(); ?>