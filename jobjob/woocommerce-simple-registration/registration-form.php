<?php
/**
 * Registration form.
 *
 * @author 	Jeroen Sormani
 * @package 	WooCommerce-Simple-Registration
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;


?>

<div class="content registration_form woocommerce">

    <div class="container">
        <div class="row" id="page_title">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1><?php _e( 'Register', 'woocommerce' ); ?></h1>
                <?php wc_print_notices(); ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div id="myTabContent" class="tab-content margin_form">
                    <div class="tab-pane fade active in" id="soiskatel">
                        <form class="form-horizontal register" method="post">
                        <?php do_action( 'woocommerce_register_form_start' ); ?>
                            <div class="form-group">
                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

								<p class="form-row form-row-wide">									
									<input type="text" class="form-control" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" placeholder="Login" />
								</p>

							<?php endif; ?>

								<div class="form-row form-row-wide">		
									<input type="text" class="form-control" name="firstname" id="reg_firstname" value="<?php echo esc_attr( $firstname ) ?>" placeholder="First name" />
								</div>
								<div class="form-row form-row-wide">		
									<input type="text" class="form-control" name="lastname" id="reg_lastname" value="<?php echo esc_attr( $lastname ) ?>" placeholder="Last name" />		
								</div>
                               	<div class="form-row form-row-wide">									
									<input type="email" class="form-control input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" placeholder="Email" />
								</div>
								
                                <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
								<div class="form-row form-row-wide">									
									<input type="password" class="form-control" name="password" id="reg_password" placeholder="password"/>
								</div>

							<?php endif; ?>		
								<div class="form-row form-row-wide role_select_wrapper">									
									<select name="role" id="role" class="selectpicker" data-width="100%">
										<option value="candidate">I'm a candidate looking for a job</option>
										<option value="employer">I'm an employer looking to hire</option>
									</select>
								</div>					
                                <div class="checkbox_conditional">
                                	<input type="checkbox" id="condition" /> 
                                	<label class="conditions" for="condition">С <a href="#" data-toggle="modal" data-target="#conditional_modal">условиями</a> работы сервиса и правилами ознакомлен и согласен</label>
                                </div>
                                
                            </div>
                           	
							<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

							
							<div class="row">
					            <div class="col-md-8 col-md-offset-2 text-center">
					                <div class="g-recaptcha" data-sitekey="6Lfw-RETAAAAAI0JOAtfd-b52PHeU48WGFCNLiGF"></div>
					            </div>
					        </div>
					        <?php do_action( 'woocommerce_register_form' ); ?>
							<?php do_action( 'register_form' ); ?>
							<div class="row">
					           <div class="col-md-8 col-md-offset-2 text-center register_form_submit_button">
									<?php wp_nonce_field( 'woocommerce-register' ); ?>
									<input type="submit" class="btn btn-default register_button" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
								    <?php do_action( 'woocommerce_register_form_end' ); ?>
    					        </div>
                            </div>
					        

                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>



<div class="modal" id="conditional_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="application_details">  
                <div class="condition_content">
                    <?php echo get_theme_mod( 'condition_text_1' ); ?>
                </div>           	
            </div>
        </div>
    </div>
</div>