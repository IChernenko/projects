<?php
/**
 * The template for displaying all pages.
 * @package jobjob
 * @since jobjob 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content page-right-sidebar" id="page-vacancy">
			 	<div class="container">				 		
			 		<div class="row">
						<?php while ( have_posts() ) : the_post(); ?>
							
							<?php get_template_part( 'template-parts/content', 'page' ); ?>												
						
						<?php endwhile; ?>
					
					</div>
				</div>
			</div>

		</main>
	</div>

<?php get_footer(); ?>
