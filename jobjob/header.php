<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jobjob
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
 <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,400italic,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
</head>

<header id="header">
    <div class="container">
        <div class="row row_search">
            <!-- <div class="col-sm-2 col-xs-8 logo hide"><img src="img/Logo.PNG" alt=""></div> -->
        <div class="col-sm-2 col-xs-8 logo-text logo">
        <?php if ( get_header_image() != NULL ) {  ?>
            <a href="/"><img src="<?php header_image(); ?>" width="<?php echo get_theme_mod( 'header_width' ) ?>px" alt="img" /></a>
        <?php } else { ?>
            <a href="/" class="logo_link"> <?php echo get_theme_mod( 'header_text' ); ?> </a> 
        <?php } ?> 
        
       
        </div>
            <div class="col-sm-8 search">
                <form role="search" method="GET" action="<?php echo get_permalink('21'); ?>" class="main_search_form">
                    <div class="input-group">
                        <input type="text" id="search_keywords" name="search_keywords" class="form-control input-search" placeholder="  Search...">
                       <!--  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul> -->

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle search_action_button" data-toggle="dropdown">JOB <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right main_search_action">
                                <li><a href="#" data-link="http://simpletestsite.ru/jobs/" class="first_action_link">JOB</a></li>
                                <li><a href="#" data-link="http://simpletestsite.ru/resumes/">CANDIDATES</a></li>
                            </ul>
                            <button class="btn btn-default" type="submit" id="btn-search" value="Search">Search</button>
                        
                        </div>
                    </div>
                </form>
            </div>
            <div class="avatar_inside">
                <?php $current_user = wp_get_current_user(); ?>
                <?php echo get_avatar( $current_user->user_email); ?>
            </div>
            <div class="col-sm-3 col-xs-6 logout">
                <div class="login_menu">

                    <?php 
                        if ( has_nav_menu( 'login_menu' ) ) {
                            wp_nav_menu(array(
                                'theme_location' => 'login_menu',
                                'container' => false
                            ));
                        }
                    ?>
                    
                </div>
            </div>
            
            <!-- <div class="col-sm-2 col-xs-4 login hide">
                <a href="#"><span class="glyphicon glyphicon-log-in"></span>Вход</a>
                <a href="#"><span class="glyphicon glyphicon-user"></span>Регистрация</a>
            </div>
            <div class="col-sm-2 col-xs-4 logout">
                <img src="/img/user.png" alt=""><span class="badge">5</span>
                <a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="glyphicon glyphicon-log-out"></span>Выйти</a>
             
            </div> -->
        </div>
    </div>
    <nav class="navbar navbar-default">    
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="navbar-collapse-1">                           
                    <?php 
		            	wp_nav_menu(array(
		            		'theme_location' => 'primary',
		            		'container' => false,
                            'depth' => 3,
		            		'menu_class' => 'nav navbar-nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		            		'walker' => new wp_bootstrap_navwalker()
		            	));
		            ?>
                </div>
            </div>
        </div>
    </nav>
</header>
