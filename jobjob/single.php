<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package jobjob
 */

get_header(); ?>

<div class="container">
<?php while ( have_posts() ) : the_post(); ?>		
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content">
			
			<?php 
				if ( get_post_type() == 'post' ) {
					get_template_part( 'template-parts/content', 'single' );
				}

				if ( get_post_type() == 'job_listing' ) {
					get_template_part( 'template-parts/content', 'job_listing' );					
				}

				if ( get_post_type() == 'resume' ) {
					get_template_part( 'template-parts/content', 'resume' );
				}

				
			?>
		
		</div>
	</article>


<?php endwhile;  ?>


</div>




<?php get_footer(); ?>
