<?php /**  * The template for displaying archive pages.  *  * @link
https://codex.wordpress.org/Template_Hierarchy  *  * @package jobjob  */

get_header(); ?>

   
<div class="content page-right-sidebar" id="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php single_cat_title( '', true ); ?> - 
                        <?php 
                           printf( _n( '%d post Available', '%d posts Available', $wp_query->found_posts ), $wp_query->found_posts ); 
                        ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 right-sidebar col-md-push-9">
                    
                    <div id="right-sidebar-banner">
                        <?php dynamic_sidebar( 'sidebar_archive' ); ?>
                    </div>
                </div>
                <div class="col-md-9 col-md-pull-3 container-blog" id="main">
                    <div class="blog_item_wrapper">        
                        <?php if ( have_posts() ) : ?>                     
                            <?php while ( have_posts() ) : the_post(); ?>                      
                                <?php get_template_part( 'template-parts/content', get_post_type() ); ?>                               
                           <?php endwhile;  ?> 
                        <?php endif; ?> 
                    </div>
                 <nav class="navigation">
                    <?php the_posts_pagination( array(
                        'mid_size' => 2,
                        'prev_text' => __( 'Back', 'textdomain' ),
                        'next_text' => __( 'Onward', 'textdomain' ),
                    )); ?>
                </nav>
                    
                </div>
                
            </div>
        </div>
    </div>

    

<?php get_footer(); ?>
