<?php
/**
 * main blog page
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jobjob
 */

get_header(); ?>

<div class="content page-right-sidebar" id="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Blog</h1>
                </div>
            </div>
            <div class="row">
            <!-- ==========================blog menu ======================================= -->
            <?php
              $menu_name = 'blog-menu';
              $locations = get_nav_menu_locations();
              $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
              $menuitems = wp_get_nav_menu_items( $menu->term_id );

            ?>




                <div class="col-md-3 right-sidebar col-md-push-9">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                     <div class="panel panel-default">
                            <?php
                            $count = 0;
                            $submenu = false;
                            if ( $menuitems != '' ) {

                            
                            foreach( $menuitems as $item ):
                                $link = $item->url;
                                $title = $item->title;
                                // item does not have a parent so menu_item_parent equals 0 (false)
                                if ( !$item->menu_item_parent ):
                                // save this id for later comparison with sub-menu items
                                $parent_id = $item->ID;
                            ?>                            
                             
                                <div class="panel-heading" role="tab" id="heading<?php echo $parent_id ?>">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $parent_id ?>" aria-expanded="true" aria-controls="collapse<?php echo $parent_id ?>" class=""><?php echo $title; ?></a>
                                </div>
                                <?php endif; ?>

                                <?php if ( $parent_id == $item->menu_item_parent ): ?>

                                    <?php if ( !$submenu ): $submenu = true; ?>
                                       <div id="collapse<?php echo $parent_id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $parent_id ?>" aria-expanded="false">
                                       <div class="panel-body">
                                    <?php endif; ?>

                                  <!--       <li class="item">
                                            <a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a>
                                        </li> -->
                                        <a href="<?php echo $link; ?>" class="btn-block"><?php echo $title; ?></a>
                                        

                                    <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                    </div>
                                </div>
                                    <?php $submenu = false; endif; ?>

                                <?php endif; ?>

                            <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                                                     
                            <?php $submenu = false; endif; ?>

                        <?php $count++; endforeach; ?>
                        <!-- end if -->
                        <?php } ?>
                        </div>
                     
     
                    </div>
                    <div id="right-sidebar-banner">
                        <?php dynamic_sidebar( 'sidebar_blog' ); ?>
                    </div>
                </div>
                <div class="col-md-9 col-md-pull-3 container-blog" id="main">
                    <div class="blog_item_wrapper">   
                        <?php if ( have_posts() ) : ?>                     
                            <?php while ( have_posts() ) : the_post(); ?>                      
                                <?php get_template_part( 'template-parts/content', get_post_type() ); ?>                               
                           <?php endwhile;  ?> 
                        <?php endif; ?>   
                    </div>            
                    <?php the_posts_pagination( array(
                        'mid_size' => 2,
                        'prev_text' => __( 'Back', 'textdomain' ),
                        'next_text' => __( 'Onward', 'textdomain' ),
                    )); ?>
            
                    
                </div>
                
            </div>
        </div>
    </div>


<?php get_footer(); ?>




