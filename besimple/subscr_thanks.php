<?php
/*
Template Name: Subscr_thanks
*/
get_header();?>
<style type="text/css">
    .Home-button{background-color: #d7171d;  cursor: pointer;}
    .Home-button:hover{background-color: #fb3c42; }
    .Home-button{
        position: relative;
        display: inline-block;
        vertical-align: top;
        min-width: 163px;
        height: 45px;
        text-decoration: none;
        text-transform: uppercase;
        text-align: center;
        font-size: 14px;
        color: #fff;
        padding: 14px 5px 0px 5px;
    }
</style>
<div class="content">

    <div class="content__container">
        <div class="content-cont">
            <div class="content-centre">
                <!-- .boxLess -->
                <div class="boxLess box">
                    <div style="display: none;">
                    <?php if(have_posts()){

                        the_content();
                        wp_reset_postdata();
                    }?>
                    </div>
</br>
<span style="font-size: 30px; text-align: center; margin-top: 10px;" class="subscr_thanks"><?php __( 'Thanks for subscribing', 'besimple');?></span>
</br>
</br>
<a class="Home-button"  href="<?php echo get_site_url(); ?>" style="background-color: #d7171d; cursor: pointer; color:#ffffff; text-decoration: none;min-width: 163px;  height: 45px;"><?php echo __('Home','besimple'); ?></a>
                    </div>
            </div>
        </div>
    </div>
</div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
