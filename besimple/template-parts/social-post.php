<div class="socialNet-info">
<?php if( get_theme_mod('post_social_link_setting_1') != '' && get_theme_mod('post_social_img_setting_1') != '' && get_theme_mod('post_social_hov_img_setting_1') != '' ){ ?>
  <?php for($i=1;$i<=5;$i++){ ?>
      <?php if( get_theme_mod('post_social_link_setting_'.$i) != '' && get_theme_mod('post_social_img_setting_'.$i) != '' && get_theme_mod('post_social_hov_img_setting_'.$i) != '' ){ ?>
        <?php 
          if ( $i == 1 ) {
            $link = 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_post_permalink());                                    
          }
           if ( $i == 2 ) {
            $link = 'https://twitter.com/home?status=' . urlencode(get_post_permalink());                                    
          }
           if ( $i == 3 ) {
            $link = 'https://plus.google.com/share?url=' . urlencode(get_post_permalink());                                    
          }
           if ( $i == 4 ) {
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
              $url = $thumb['0'];
              $link = 'https://pinterest.com/pin/create/button/?url=' . urlencode(get_post_permalink()) . '&media=' . urlencode($url) . '&description=' . get_the_excerpt();                                    
          }
        ?>    
          <a class="socialNetworks__fb js-social-share" href="<?php echo $link; ?> " target="_blank">
              <img src="<?php echo get_theme_mod('post_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('post_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('post_social_img_setting_'.$i).'\''; ?>"/>
          </a>
      <?php } ?>
  <?php } ?>
<?php } else { ?>
  <?php $link = 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_post_permalink()); ?>
  <a class="socialNetworks__fb js-social-share" href="<?php echo $link; ?> " target="_blank">
      <img src="<?php echo get_template_directory_uri() ?>/images/7.png" onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/12.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/7.png'" />
  </a>
  <?php $link = 'https://twitter.com/home?status=' . urlencode(get_post_permalink()); ?>
  <a class="socialNetworks__fb js-social-share" href="<?php echo $link; ?> " target="_blank">
      <img src="<?php echo get_template_directory_uri() ?>/images/8.png" onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/13.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/8.png'" />
  </a>
  <?php $link = 'https://plus.google.com/share?url=' . urlencode(get_post_permalink()); ?>
  <a class="socialNetworks__fb js-social-share" href="<?php echo $link; ?> " target="_blank">
      <img src="<?php echo get_template_directory_uri() ?>/images/11.png" onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/14.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/11.png'" />
  </a>
  <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
        $url = $thumb['0'];
        $link = 'https://pinterest.com/pin/create/button/?url=' . urlencode(get_post_permalink()) . '&media=' . urlencode($url) . '&description=' . get_the_excerpt(); 
    ?>
  <a class="socialNetworks__fb js-social-share" href="<?php echo $link; ?> " target="_blank">
      <img src="<?php echo get_template_directory_uri() ?>/images/9.png" onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/22.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/9.png'" />
  </a>
<?php } ?>
</div>