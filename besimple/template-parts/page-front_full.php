<div class="content-centre">
                    <!-- .boxLess -->
<?php 
$pageId = get_the_ID();
$saved_data = get_post_meta($pageId,'posts_field_id_1',true); 
if ( $saved_data == '-1' ) {                          
  $latest_cpt = get_posts("post_type=post&numberposts=1");  
  $saved_data = $latest_cpt[0]->ID;
}  
$post = get_post( $saved_data );                           
setup_postdata( $post );
$post_meta = get_post_meta(get_the_ID(),'image_field_id',true);
?>


<?php 
    if ( get_theme_mod( 'front_page_add_block' ) != '' ) {
        get_template_part( 'template-parts/content', 'box' );
    }
?>
<?php wp_reset_postdata(); ?>

    <div class="boxLess box">

        <?php 
            wp_reset_query();
           if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } 
            elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } 
            else { $paged = 1; } 
            $args = array(
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
            'paged' => $paged,

        );

//                 
        $wp_query = new WP_Query( $args );

        if ( $wp_query->have_posts() ) {

        while ( $wp_query->have_posts() ) {
            $wp_query->the_post();
            $post_categories = get_the_category();?>

                <div class="boxLess__cont box_item">

                    <div class="boxLess-block">
                        <div class="boxLess-img"><?php the_post_thumbnail();?></div>
                        <div class="boxLess-cont">
                            <span class="boxLess-category">
                                <?php
                                if(!empty($post_categories)){
                                    echo $category_line = '';
                                    foreach($post_categories as $category) {
                                        if($category->term_id != 1)
                                            $category_line .= '<a href="'.get_term_link($category->term_id).'">'.$category->cat_name.'</a>, ';
                                    }
                                }
                                    echo rtrim($category_line,', ');
                                ?>
                            </span>
                            <span class="boxLess-title"><a href="<?php echo get_post_permalink ();?>"><?php echo get_the_title();?></a></span>
                            <span class="boxLess-date"><?php echo date('F j, Y',strtotime(get_the_date()));?></span>
                            <div class="boxLess-text">
                                <p><?php $excerpt = get_the_excerpt(); echo custom_limit_excerpt($excerpt, 80);?></p>
                            </div>

                        </div>

                        <a class="boxLess-button" href="<?php echo get_post_permalink ();?>"><?php echo __('CONTINUE READING','besimple'); ?></a>

                        <div class="infoMenu">
                            <ul>
                                <li><a href="<?php echo get_post_permalink ().'&scroll=comments';?>"><?php echo ( get_comments_number() != 1 ) ? get_comments_number().__( ' Comments','besimple') : get_comments_number().__(' Comment','besimple'); ?></a></li>
                                <li>
                                    <?php get_template_part('template-parts/social', 'post') ?>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
        <?php } ?>
        <?php wp_reset_postdata(); ?>

            
        <?php } ?>


    </div>        
    

    <nav class="navigation">
        <?php the_posts_pagination( array(
            'mid_size' => 2,
            'prev_text' => __( 'Back', 'besimple' ),
            'next_text' => __( 'Onward', 'besimple' ),
        )); ?>
    </nav>

                    
  <?php 
  if ( get_option('posts_per_page') < $wp_query->found_posts ) {
?>

<div class="morePosts-cont">
    <a class="morePosts-button" ><?php echo __('LOAD MORE POSTS','besimple'); ?></a>
</div> 

<?php 
    }
?>

</div>