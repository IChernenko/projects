    <!-- .titleBlock -->
    
    <div class="titleBlock">

        <?php
        $args = array('post_type' => 'slides',
            'orderby' => 'menu_order',
            'posts_per_page' => -1);

        $slides = new WP_Query($args);

        if ( $slides->have_posts() ) : ?>
            <div class="titleBlock__img">
                <div class="slideBox">
                    <?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
                        <div class="slideBox__cont"> <?php the_post_thumbnail('slides');?> </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>

                </div>
            </div>
        <?php endif;    ?>


        <div class="titleBlock__container">
            <div class="titleBlock-cont">
                <div class="titleBlock-box">
                <?php
                    $args = array('post_type' => 'news',
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'post_status' => 'publish',
                        'posts_per_page' => 1);

                        $news = new WP_Query($args);

                        if ( $news->have_posts() ) : $news->the_post();?>

                            <span class="titleBox-name"><?php echo __('NEWS','besimple'); ?></span>
                            <h2><?php the_title(); ?></h2>
                            <span class="titleBox-text"><?php $excerpt = get_the_excerpt(); echo custom_limit_excerpt($excerpt, 20);?></span>

                            <a class="readMore-button" href="<?php echo get_post_permalink ();?>"><?php echo __('READ MORE','besimple'); ?></a>
                           
                        <?php endif;    ?>
                        <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>

