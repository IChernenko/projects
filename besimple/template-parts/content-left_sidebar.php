<div class="content">
        <div class="content__container">
            <div class="content-cont">
            <?php get_sidebar();?> 
                <div class="content-right">
                    <div class="contentBox">
                            <?php the_content();?>

                        <div class="contentMenu">
                            <ul>
                                <?php $post_tags = get_the_tags();
                                if(!empty($post_tags)) {
                                    foreach ($post_tags as $tag) { ?>
                                        <li><a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name; ?></a></li>
                                    <?php }
                                }?>
                            </ul>
                        </div>


                        <div class="infoMenu">
                            <ul>
                                <li><?php the_author_posts_link(); ?></li>
<!--                                <li><a href="javascript:void(0)">by --><?php //the_author();?><!--</a></li>-->
                                <li><a class="show-hide-comments" href="javascript:void(0)"><?php echo ( get_comments_number() != 1 ) ? get_comments_number().__( ' Comments','besimple') : get_comments_number().__(' Comment','besimple'); ?></a></li>
                                <li>
                                    <?php get_template_part('template-parts/social', 'post') ?>
                                </li>
                            </ul>
                        </div>
                            <?php comments_template(); ?>

                      

                    </div>


                        <?php // If comments are closed and there are comments, let's leave a little note, shall we?
                        if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

                            <p class="no-comments"><?php esc_html_e( 'Comments are closed', 'besimple' ); ?></p>
                        <?php endif;?>

                        <?php
                        $args = array(
                            'comment_field' => '<div class="leaveReply-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="'._x( 'Comment', 'noun' , 'besimple').'"></textarea></div>',
                            'class_form' => 'leaveReply__cont',
                            'fields' => apply_filters(
                                'comment_form_default_fields', array(
                                    'author' =>'<div class="leaveReply-input"><div class="leaveReply-inputBox">' . '<input id="author" placeholder="'. _x( 'Name *','required field' , 'besimple' ).'" name="author" type="text" value="' .
                                        esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />'.'</div>',

                                    'email'  => '<div class="leaveReply-inputBox">' . '<input id="email" placeholder="'. _x( 'Email *','required field' , 'besimple' ).'" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                                        '" size="30"' . $aria_req . ' />'  .  '</div>',

                                    'url'    => '<div class="leaveReply-inputBox">' . '<input id="url" name="url" placeholder="'. _x( 'Website','required field' , 'besimple' ).'" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /> ' . '</div></div>'

                                )
                            ),
                        );
                        comment_form($args); ?>

                </div>

            </div>

        </div>

    </div>