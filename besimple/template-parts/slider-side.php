    <!-- .titleBlock -->
    
    <div class="titleBlock side-slider">
            
            
            <div class="slideBox">
                 <?php
                    $args = array('post_type' => 'slides',
                        'orderby' => 'menu_order',
                        'posts_per_page' => -1);

                    $slides = new WP_Query($args);

                  if ( $slides->have_posts() ) : ?>
                   <?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
                    <?php 
                      $pageId = get_the_ID();
                      $saved_data = get_post_meta($pageId,'posts_field_id_slides_1',true);
                      $postInSlide = get_post( $saved_data ); 
                      $post_categories = get_the_category($postInSlide);
                      // var_dump($postInSlide);                             
                      
                      // $post_meta = get_post_meta(get_the_ID(),'image_field_id',true);
                    ?>
                 <div class="slideBox__cont">
                      <div class="titleBlock__img"><?php the_post_thumbnail('slides');?></div>
                      <div class="titleBlock__container">                           
                           <div class="titleBlock-fon"></div>
                           <div class="titleBlock-cont">
                                <div class="titleBlock-box">
                                     <span class="titleBox-name">
                                       <?php
                          if(!empty($post_categories)) {
                            $category_line = '';
                              foreach ($post_categories as $category) {
                                  if ($category->term_id != 1)
                                      $category_line .= '<a href="' . get_term_link($category->term_id) . '">' . $category->cat_name . '</a>, ';
                              }
                          }
                                            echo rtrim($category_line,', ');
                                            // unset($category_line);
                                        ?>
                                      </span>
                                     <h2><?php echo $postInSlide->post_title; ?></h2>
                                     <span class="titleBox-text"><p><?php echo custom_limit_excerpt($postInSlide->post_excerpt, 20);?></p></span>
                                     <a class="readMore-button" href="<?php echo $postInSlide->guid; ?>"><?php echo __('READ MORE','besimple'); ?></a>
                                </div>
                           </div>
                      </div>
                 </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
                <?php endif;    ?>

            </div>
       </div>

