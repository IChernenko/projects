
<div class="contentBox">
<?php $post_meta = get_post_meta($post->ID, 'image_field_id', true); ?>
    
    <img src="<?php echo $post_meta['url'] == '' ? get_template_directory_uri().'/images/content/big/img-2.jpg' : $post_meta['url']; ?>" alt="img"/>
    
    <?php  $post_categories = get_the_category(); 

    ?>
    <span class="contentBox-name">
      <?php
          if(!empty($post_categories)) {
            $category_line = '';
              foreach ($post_categories as $category) {
                  if ($category->term_id != 1)
                      $category_line .= '<a href="' . get_term_link($category->term_id) . '">' . $category->cat_name . '</a>, ';
              }
          }
          echo rtrim($category_line,', ');
          
      ?>
    </span>
    <h1 class="text-centre-2"><?php the_title(); ?></h1>
    <span class="contentBox-date"><?php echo date('F j, Y',strtotime(get_the_date()));?></span>

    
    
    <p><?php $excerpt = get_the_excerpt(); echo custom_limit_excerpt($excerpt, 100);?></p>
    
    
    <a class="contentBox-button" href="<?php echo get_post_permalink() ?>"><?php echo __('CONTINUE READING','besimple'); ?></a>
  
    
    <div class="infoMenu infoMenu-indent">
         <ul>
             <li><?php the_author_posts_link(); ?></li>
              <li><a href="<?php echo get_post_permalink ().'&scroll=comments';?>"><?php echo ( get_comments_number() != 1 ) ? get_comments_number().__( ' Comments','besimple') : get_comments_number().__(' Comment','besimple'); ?></a></li>
              <li>                                     
                   <?php get_template_part('template-parts/social', 'post') ?>
              </li>
         </ul>
    </div>
    

</div>
<?php  wp_reset_query(); ?>