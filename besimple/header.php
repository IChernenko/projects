<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section opens <div class="total-container"> and contain todayNews, hat and menu divs
 *
 * @package besimple
 * @since besimple 1.0
 */
?>

<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="UTF-8">
<?php wp_head(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=0, maximum-scale=1" />
    <?php if(get_theme_mod( 'google_font_setting' ) != '') { ?>
        <link href='https://fonts.googleapis.com/css?family=<?php echo get_theme_mod( 'google_font_setting' ); ?>:400,400italic,700,700italic&subset=latin,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
        <style type="text/css">
            *,.menu-block .submenu a,.search__text,.titleBlock-box h1, .titleBlock-box h2,.newsletter__name,.blockInfo__title,.instagram-name,.represent__title,.contentBox h1,.contentBox h2,.contentBox h3,.contentBox h4,.contentBox h1 span,.contentBox h2 span,.contentBox h3 span,.contentBox h4 span,.contentBox blockquote,.contentBox blockquote span,.relatedPost-link,.eventsInfo-name a,.eventsBlock-name a,.boxBig-title,.boxLess-title,.boxLess-title a,.popularPosts-text,.todayNews-link a,.menu-block .submenu a:hover,.comment-reply-title,.pingback .edit-link ,.comment-list .reply a ,.comment-form label ,.form-allowed-tags ,.no-comments ,.form-allowed-tags code ,.comment-reply-title ,.comment-edit-link,#reply-title{font-family: '<?php echo get_theme_mod( 'google_font_setting' ); ?>', sans-serif; }
        </style>
    <?php } ?>
    <?php if(get_theme_mod( 'color_scheme_setting' ) != '' ) { $color=get_theme_mod( 'color_scheme_setting' );?>
        <style type="text/css">
            .Home-button,.titleBlock-fon,.wpcf7-submit,.form-submit input,.eventsBlock,.represent.represent--noFon,.newsletter, .represent__butt, .contentMenu ul a,.inputFooter-submit,.contentBox-button,.boxBig-button,.boxLess-button,.morePosts-button,.leaveReply__button input,.side-slider .titleBlock__container{background-color: <?php echo $color; ?>; }
            .represent.represent--noFon .represent__butt,.inputNewsletter-submit,.todayNews-title, .todayNews-category { color: <?php echo $color; ?>;}
            .todayNews-box:first-child { border-right: 1px solid <?php echo $color; ?>;}
        </style>
    <?php } ?>
    <?php if(get_theme_mod( 'hover_color_scheme_setting' ) != '') { $hover_color=get_theme_mod( 'hover_color_scheme_setting' ); ?>
        <style type="text/css">
            .Home-button:hover,.wpcf7-submit,.represent__butt:hover, .contentMenu ul a:hover,.inputFooter-submit:hover,.contentBox-button:hover,.boxBig-button:hover,.boxLess-button:hover,.morePosts-button:hover,.leaveReply__button input:hover{background-color: <?php echo $hover_color; ?>; }
        </style>
    <?php } ?>
    <?php if(get_theme_mod( 'today_news_display_setting' ) == '') { ?>
        <style type="text/css">
            .todayNews {
                display:none !important;
            }
        </style>
    <?php } ?>
</head>
<body <?php body_class(); ?>>



<div class="total-container">


    <!-- .todayNews -->
    <div class="todayNews">

        <div class="todayNews__container">

            <div class="todayNews-box">
                <span class="todayNews-title"><?php echo __('TODAY NEWS','besimple'); ?></span>
            </div>

            <?php             
            $args = array(
                'posts_per_page' => 4,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'news',
                'post_status' => 'publish');

            $the_query = new WP_Query( $args );

            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $post_categories = get_the_terms(get_the_ID(),'news_category');?>
            <div class="todayNews-box">
                <?php
                if(!empty($post_categories)){
                    $category_line = '';
                    foreach($post_categories as $category) {
                        $category_line .= $category->name.', ';
                    }
                }?>
                <span class="todayNews-category"><?php echo rtrim($category_line,', '); ?></span>
                <div class="todayNews-link"><a href="<?php echo get_post_permalink ();?>"><?php echo get_the_title();?></a></div>
            </div>
            <?php } ?>
            <?php wp_reset_postdata(); ?>

        </div>

    </div>
    <!-- END .todayNews -->
    <!-- .hat -->
    <div class="hat">

        <div class="hat__container">

            <div class="hat-block">


                <div class="logo">
                    <a href="<?php echo get_site_url(); ?>" class="header_logo_link">
                      <?php if ( get_theme_mod('header_logo_setting') != '' ) { ?>

                        <img src="<?php echo get_theme_mod('header_logo_setting') ?>" alt="img" />

                        <?php } elseif  ( get_theme_mod('header_text_logo') != '' ) {

                            echo get_theme_mod('header_text_logo');
                            echo "<div class='header_logo_text_bottom'>" .  get_theme_mod('header_text_logo_bottom') . "</div>";

                        } else { ?>
                            <img src="<?php echo get_template_directory_uri().'/images/logo.png' ?>" alt="img" />
                       <?php } ?>
                    </a>
                    
                    </a>
                </div>
                
                <div class="socialNetworks">
                    <?php if( get_theme_mod('header_social_link_setting_1') != '' && get_theme_mod('header_social_img_setting_2') != '' && get_theme_mod('header_social_hov_img_setting_3') != '' ){ ?>
                        <?php for($i=1;$i<=5;$i++){ ?>
                            <?php if( get_theme_mod('header_social_link_setting_'.$i) != '' && get_theme_mod('header_social_img_setting_'.$i) != '' && get_theme_mod('header_social_hov_img_setting_'.$i) != '' ){ ?>
                                <a class="socialNetworks__fb" href="<?php echo get_theme_mod('header_social_link_setting_'.$i);?>">
                                    <img src="<?php echo get_theme_mod('header_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('header_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('header_social_img_setting_'.$i).'\''; ?>"/>
                                </a>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <a class="socialNetworks__fb" href="https://www.facebook.com/">
                            <img src='<?php echo get_template_directory_uri(); ?>/images/1.png' onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/4.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/1.png'" />
                        </a>
                        <a class="socialNetworks__fb" href="https://twitter.com">
                            <img src='<?php echo get_template_directory_uri(); ?>/images/2.png' onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/5.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/2.png'" />
                        </a>
                        <a class="socialNetworks__fb" href="https://www.pinterest.com/">
                            <img src='<?php echo get_template_directory_uri(); ?>/images/3.png' onmouseover="this.src='<?php echo get_template_directory_uri() ?>/images/6.png'" onmouseout="this.src='<?php echo get_template_directory_uri() ?>/images/3.png'" />
                        </a>
                    <?php } ?>
                </div>

            </div>

        </div>

    </div>
    <!-- END .hat -->





    <!-- .menu -->
    <div class="menu">

        <div class="menu__container">


            <div class="menu-block">

                <div class="mobile-nav">
                    <span class="mobile-nav__title"><?php echo __('Menu','besimple'); ?></span>
                    <div class="mobile-nav__butt">
                        <span class="mobileNav-line"></span>
                        <span class="mobileNav-line"></span>
                        <span class="mobileNav-line"></span>
                    </div>
                </div>

                <ul class="menuCont">
                    <?php
                    $menu = wp_get_nav_menu_object('header_menu');
                    $menu_items = wp_get_nav_menu_items($menu->term_id);
                    // var_dump($menu_items);
                    for ($i = 0; $i < count($menu_items); $i++) {
                        if( (boolean)$menu_items[$i]->menu_item_parent != (boolean)$menu_items[$i+1]->menu_item_parent ) {
                            if( (boolean)$menu_items[$i]->menu_item_parent == false ) {
                    ?>
                        <li>
                            <a class="linkArrow" href="javascript:void(0)"><?php echo $menu_items[$i]->title ?></a>
                            <ul class="submenu">
                            <?php  }else{ ?>
                                <li> <a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a> </li>
                            </ul>
                        </li>
                            <?php  } ?>
                        <?php  }else{ ?>
                            <li> <a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a> </li>
                        <?php  } ?>
                    <?php  } ?>

                <div class="search_header">
                    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo get_site_url(); ?>">
                        <input value="" name="s" id="s" class="search__text" type="text" placeholder="<?php echo __('Search','besimple'); ?>" />
                        <input id="searchsubmit" class="search__button" type="submit" />
                    </form>
                </div>


            </div>


        </div>

    </div>



    <!-- END .menu -->