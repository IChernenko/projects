#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: besimple\n"
"POT-Creation-Date: 2016-02-09 00:45+0200\n"
"PO-Revision-Date: 2016-02-09 00:45+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:21
msgid "404"
msgstr ""

#: aboutme.php:61
msgid "Related post"
msgstr ""

#: comments.php:40 comments.php:58
msgid "Comment navigation"
msgstr ""

#: comments.php:43 comments.php:61
msgid "Older Comments"
msgstr ""

#: comments.php:44 comments.php:62
msgid "Newer Comments"
msgstr ""

#: footer.php:27
msgid "All rights reserved"
msgstr ""

#: footer.php:61
msgid "FOLLOW US"
msgstr ""

#: footer.php:96 footer.php:100
msgid "SUBSCRIBE"
msgstr ""

#: footer.php:99
msgid "EMAIL"
msgstr ""

#: functions.php:49
msgid "Primary"
msgstr ""

#: functions.php:105 functions.php:681
msgid "Sidebar"
msgstr ""

#: functions.php:114
msgid "Sidebar-subscribe"
msgstr ""

#: functions.php:124
msgid "Sidebar-google-ads"
msgstr ""

#: functions.php:133 functions.php:541 functions.php:615
msgid "Footer"
msgstr ""

#: functions.php:192
msgid "No more posts to load"
msgstr ""

#: functions.php:308
msgid "Header Menu"
msgstr ""

#: functions.php:309
msgid "Left Footer Menu"
msgstr ""

#: functions.php:310
msgid "Middle Footer Menu"
msgstr ""

#: functions.php:311
msgid "Social Footer Menu"
msgstr ""

#: functions.php:312
msgid "Bottom Footer Menu"
msgstr ""

#: functions.php:313
msgid "Social Menu"
msgstr ""

#: functions.php:327
msgid "News Category"
msgstr ""

#: functions.php:341 functions.php:342
msgid "Slides"
msgstr ""

#: functions.php:352 functions.php:353
msgid "News"
msgstr ""

#: functions.php:391
msgid "Header Meta Box"
msgstr ""

#: functions.php:407
msgid "Post Header Image "
msgstr ""

#: functions.php:422 functions.php:494
msgid "Related Post Meta Box"
msgstr ""

#: functions.php:467
msgid "Shortcode Meta Box"
msgstr ""

#: functions.php:482
msgid "Shortcode"
msgstr ""

#: functions.php:513
msgid "Header"
msgstr ""

#: functions.php:520
msgid "Display News Header"
msgstr ""

#: functions.php:524
msgid "Show and hide Today News section"
msgstr ""

#: functions.php:533
msgid "Header Logo"
msgstr ""

#: functions.php:536
msgid "Upload a logo to replace the default in the header"
msgstr ""

#: functions.php:548 functions.php:622
msgid "Code field"
msgstr ""

#: functions.php:561
msgid "Tagline field"
msgstr ""

#: functions.php:573
msgid "Label field"
msgstr ""

#: functions.php:586
msgid "Logo"
msgstr ""

#: functions.php:589
msgid "Upload a logo to replace the default in the footer"
msgstr ""

#: functions.php:598
msgid "Color scheme"
msgstr ""

#: functions.php:601
msgid "Change color scheme of the site"
msgstr ""

#: functions.php:610
msgid "Change hover color"
msgstr ""

#: functions.php:631
msgid "Position customize"
msgstr ""

#: functions.php:638
msgid "Front page"
msgstr ""

#: functions.php:655
msgid "Show page block"
msgstr ""

#: functions.php:668
msgid "Box slider"
msgstr ""

#: functions.php:720
msgid "Header social menu"
msgstr ""

#: functions.php:729 functions.php:1020
msgid "First social network"
msgstr ""

#: functions.php:762 functions.php:1053
msgid "Second social network"
msgstr ""

#: functions.php:795 functions.php:1086
msgid "Third social network"
msgstr ""

#: functions.php:828 functions.php:1119
msgid "Fourth social network"
msgstr ""

#: functions.php:861 functions.php:991 functions.php:1152
msgid "Fifth social network"
msgstr ""

#: functions.php:891
msgid "Post social menu"
msgstr ""

#: functions.php:900
msgid "Facebook social network"
msgstr ""

#: functions.php:923
msgid "Twitter social network"
msgstr ""

#: functions.php:946
msgid "Google+ social network"
msgstr ""

#: functions.php:969
msgid "Pinterest social network"
msgstr ""

#: functions.php:1011
msgid "widget social menu"
msgstr ""

#: functions.php:1236
#, php-format
msgid "<b class=\"fn\">%s</b> <span class=\"says\">says:</span>"
msgstr ""

#: functions.php:1239
msgid "Your comment is awaiting moderation."
msgstr ""

#: functions.php:1247
msgid "(Edit)"
msgstr ""

#: header.php:65
msgid "TODAY NEWS"
msgstr ""

#: header.php:140
msgid "Menu"
msgstr ""

#: header.php:171
msgid "Search"
msgstr ""

#: inc/class-tgm-plugin-activation.php:333
msgid "Install Required Plugins"
msgstr ""

#: inc/class-tgm-plugin-activation.php:334
msgid "Install Plugins"
msgstr ""

#: inc/class-tgm-plugin-activation.php:335
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:336
msgid "Something went wrong with the plugin API."
msgstr ""

#: inc/class-tgm-plugin-activation.php:338
#, php-format
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:343
#, php-format
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:348
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to install the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to install the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:353
#, php-format
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:358
#, php-format
msgid "There is an update available for: %1$s."
msgid_plural "There are updates available for the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:363
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to update the %1$s plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to update the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:368
#, php-format
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:373
#, php-format
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:378
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to activate the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to activate the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:383
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:388
msgid "Begin updating plugin"
msgid_plural "Begin updating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:393
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:397
msgid "Return to Required Plugins Installer"
msgstr ""

#: inc/class-tgm-plugin-activation.php:398
msgid "Return to the dashboard"
msgstr ""

#: inc/class-tgm-plugin-activation.php:399
#: inc/class-tgm-plugin-activation.php:3029
msgid "Plugin activated successfully."
msgstr ""

#: inc/class-tgm-plugin-activation.php:400
#: inc/class-tgm-plugin-activation.php:2832
msgid "The following plugin was activated successfully:"
msgid_plural "The following plugins were activated successfully:"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:401
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: inc/class-tgm-plugin-activation.php:402
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: inc/class-tgm-plugin-activation.php:403
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:404
msgid "Dismiss this notice"
msgstr ""

#: inc/class-tgm-plugin-activation.php:405
msgid "Please contact the administrator of this site for help."
msgstr ""

#: inc/class-tgm-plugin-activation.php:520
msgid "This plugin needs to be updated to be compatible with your theme."
msgstr ""

#: inc/class-tgm-plugin-activation.php:521
msgid "Update Required"
msgstr ""

#: inc/class-tgm-plugin-activation.php:635
msgid "Set the parent_slug config variable instead."
msgstr ""

#: inc/class-tgm-plugin-activation.php:828
#: inc/class-tgm-plugin-activation.php:3438
msgid "Return to the Dashboard"
msgstr ""

#: inc/class-tgm-plugin-activation.php:935
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: inc/class-tgm-plugin-activation.php:935
#: inc/class-tgm-plugin-activation.php:938
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: inc/class-tgm-plugin-activation.php:938
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: inc/class-tgm-plugin-activation.php:1116
#: inc/class-tgm-plugin-activation.php:2828
msgctxt "plugin A *and* plugin B"
msgid "and"
msgstr ""

#: inc/class-tgm-plugin-activation.php:1881
#, php-format
msgctxt "%s = version number"
msgid "TGMPA v%s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2167
msgid "Required"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2170
msgid "Recommended"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2186
msgid "WordPress Repository"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2189
msgid "External Source"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2192
msgid "Pre-Packaged"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2209
msgid "Not Installed"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2213
msgid "Installed But Not Activated"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2215
msgid "Active"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2221
msgid "Required Update not Available"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2224
msgid "Requires Update"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2227
msgid "Update recommended"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2235
#, php-format
msgctxt "%1$s = install status, %2$s = update status"
msgid "%1$s, %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2280
#, php-format
msgctxt "plugins"
msgid "All <span class=\"count\">(%s)</span>"
msgid_plural "All <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2283
#, php-format
msgid "To Install <span class=\"count\">(%s)</span>"
msgid_plural "To Install <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2286
#, php-format
msgid "Update Available <span class=\"count\">(%s)</span>"
msgid_plural "Update Available <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2289
#, php-format
msgid "To Activate <span class=\"count\">(%s)</span>"
msgid_plural "To Activate <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2371
msgctxt "as in: \"version nr unknown\""
msgid "unknown"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2379
msgid "Installed version:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2387
msgid "Minimum required version:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2399
msgid "Available version:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2422
#, php-format
msgid ""
"No plugins to install, update or activate. <a href=\"%1$s\">Return to the "
"Dashboard</a>"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2436
msgid "Plugin"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2437
msgid "Source"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2438
msgid "Type"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2442
msgid "Version"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2443
msgid "Status"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2491
#, php-format
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Install %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2495
#, php-format
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Update %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2500
#, php-format
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Activate %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2570
msgid "Upgrade message from the plugin author:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2603
msgid "Install"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2609
msgid "Update"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2612
msgid "Activate"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2643
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2645
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2681
msgid "No plugins are available to be installed at this time."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2683
msgid "No plugins are available to be updated at this time."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2788
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2814
msgid "No plugins are available to be activated at this time."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3028
msgid "Plugin activation failed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3362
#, php-format
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3364
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3365
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3369
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3370
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3370
#: inc/class-tgm-plugin-activation.php:3376
msgid "Show Details"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3370
#: inc/class-tgm-plugin-activation.php:3376
msgid "Hide Details"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3371
msgid "All installations and activations have been completed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3372
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3375
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3376
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3377
msgid "All installations have been completed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3378
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/template-tags.php:28
#, php-format
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:33
#, php-format
msgctxt "post author"
msgid "by %s"
msgstr ""

#: inc/template-tags.php:50 inc/template-tags.php:56
msgid ", "
msgstr ""

#: inc/template-tags.php:52
#, php-format
msgid "Posted in %1$s"
msgstr ""

#: inc/template-tags.php:58
#, php-format
msgid "Tagged %1$s"
msgstr ""

#: inc/template-tags.php:64
msgid "Leave a comment"
msgstr ""

#: inc/template-tags.php:64
msgid "1 Comment"
msgstr ""

#: inc/template-tags.php:64
msgid "% Comments"
msgstr ""

#: inc/template-tags.php:71 template-parts/content-page.php:33
#, php-format
msgid "Edit %s"
msgstr ""

#: inc/wordpress-theme-customizer-custom-controls/select/taxonomy-dropdown-custom-control.php:35
msgid "None"
msgstr ""

#: meta-box-class/my-meta-box-class.php:420
#: meta-box-class/my-meta-box-class.php:459
msgid "Remove"
msgstr ""

#: meta-box-class/my-meta-box-class.php:432
msgid "Add"
msgstr ""

#: meta-box-class/my-meta-box-class.php:789
msgid "Select a color"
msgstr ""

#: meta-box-class/my-meta-box-class.php:876
msgid "Select ..."
msgstr ""

#: meta-box-class/my-meta-box-class.php:1825
#: meta-box-class/my-meta-box-class.php:1833
msgid "Sorry, you cannot upload this file type for this field."
msgstr ""

#: search.php:55 slides.php:44 template-parts/slider-full.php:39
msgid "NEWS"
msgstr ""

#: search.php:59 slides.php:48 template-parts/slider-full.php:43
#: template-parts/slider-side.php:45
msgid "READ MORE"
msgstr ""

#: search.php:129 slides-full.php:49 slides-full.php:133 slides-left.php:55
#: slides-left.php:124 slides-right.php:55 slides-right.php:124
#: slides-side-full.php:51 slides-side-full.php:126 slides-side-left.php:54
#: slides-side-left.php:123 slides-side-right.php:55 slides-side-right.php:124
#: slides.php:155 template-parts/content-box.php:29
#: template-parts/page-archive_full.php:52
#: template-parts/page-archive_left.php:47
#: template-parts/page-archive_right.php:49
#: template-parts/page-front_full.php:69 template-parts/page-front_left.php:59
#: template-parts/page-front_right.php:63
msgid "CONTINUE READING"
msgstr ""

#: search.php:158
#, php-format
msgid "Nothing found for: %s"
msgstr ""

#: search.php:165 slides-full.php:179 slides-left.php:163 slides-right.php:163
#: slides-side-full.php:172 slides-side-left.php:162 slides-side-right.php:163
#: slides.php:194 template-parts/page-archive_full.php:86
#: template-parts/page-archive_left.php:80
#: template-parts/page-archive_right.php:82
#: template-parts/page-front_full.php:103 template-parts/page-front_left.php:92
#: template-parts/page-front_right.php:97
msgid "LOAD MORE POSTS"
msgstr ""

#: slides-full.php:169 slides-left.php:157 slides-right.php:157
#: slides-side-full.php:162 slides-side-left.php:156 slides-side-right.php:157
#: slides.php:188 template-parts/page-archive_full.php:79
#: template-parts/page-archive_left.php:73
#: template-parts/page-archive_right.php:75
#: template-parts/page-front_full.php:96 template-parts/page-front_left.php:85
#: template-parts/page-front_right.php:90
msgid "Back"
msgstr ""

#: slides-full.php:170 slides-left.php:158 slides-right.php:158
#: slides-side-full.php:163 slides-side-left.php:157 slides-side-right.php:158
#: slides.php:189 template-parts/page-archive_full.php:80
#: template-parts/page-archive_left.php:74
#: template-parts/page-archive_right.php:76
#: template-parts/page-front_full.php:97 template-parts/page-front_left.php:86
#: template-parts/page-front_right.php:91
msgid "Onward"
msgstr ""

#: subscr_thanks.php:41
msgid "Home"
msgstr ""

#: template-parts/content-left-sidebar.php:49
#: template-parts/content-left_sidebar.php:41
#: template-parts/content-no_sidebar.php:40
#: template-parts/content-right_sidebar.php:40
msgid "Comments are closed"
msgstr ""

#: template-parts/content-left-sidebar.php:54
#: template-parts/content-left_sidebar.php:46
#: template-parts/content-no_sidebar.php:45
#: template-parts/content-right_sidebar.php:45 typography.php:94
msgctxt "noun"
msgid "Comment"
msgstr ""

#: template-parts/content-left-sidebar.php:58
#: template-parts/content-left_sidebar.php:50
#: template-parts/content-no_sidebar.php:49
#: template-parts/content-right_sidebar.php:49
msgctxt "required field"
msgid "Name *"
msgstr ""

#: template-parts/content-left-sidebar.php:61
#: template-parts/content-left_sidebar.php:53
#: template-parts/content-no_sidebar.php:52
#: template-parts/content-right_sidebar.php:52
msgctxt "required field"
msgid "Email *"
msgstr ""

#: template-parts/content-left-sidebar.php:64
#: template-parts/content-left_sidebar.php:56
#: template-parts/content-no_sidebar.php:55
#: template-parts/content-right_sidebar.php:55
msgctxt "required field"
msgid "Website"
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:21
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:25
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:31
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/content-page.php:22 template-parts/content.php:38
msgid "Pages:"
msgstr ""

#: template-parts/content.php:33
#, php-format
msgid "Continue reading %s <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: typography.php:89
msgid "Comments are closed."
msgstr ""

#: widgets/besimple_categories.php:10 widgets/besimple_categories.php:13
msgid "Besimple categories"
msgstr ""

#: widgets/besimple_categories.php:77 widgets/popular_post_widget.php:123
#: widgets/recent_comments_besimple_widget.php:76
#: widgets/recent_post_widget.php:68 widgets/subscribe_and_follow_widget.php:57
msgid "New title"
msgstr ""

#: widgets/besimple_categories.php:82 widgets/popular_post_widget.php:128
#: widgets/recent_comments_besimple_widget.php:81
#: widgets/recent_post_widget.php:73 widgets/subscribe_and_follow_widget.php:62
msgid "Title:"
msgstr ""

#: widgets/popular_post_widget.php:10
msgid "Popular width Widget"
msgstr ""

#: widgets/popular_post_widget.php:13
msgid "Popular width widget"
msgstr ""

#: widgets/recent_comments_besimple_widget.php:10
msgid "Recent comments Besimple"
msgstr ""

#: widgets/recent_comments_besimple_widget.php:13
msgid "Recent comments widget"
msgstr ""

#: widgets/recent_post_widget.php:10
msgid "Recent posts Besimple"
msgstr ""

#: widgets/recent_post_widget.php:13
msgid "Recent posts widget"
msgstr ""

#: widgets/subscribe_and_follow_widget.php:10
msgid "Subscribe and Follow Besimple"
msgstr ""

#: widgets/subscribe_and_follow_widget.php:13
msgid "Subscribe and Follow widget"
msgstr ""

#. Theme Name of the plugin/theme
msgid "besimple"
msgstr ""

#. Theme URI of the plugin/theme
#. Description of the plugin/theme
#. Author URI of the plugin/theme
msgid "no"
msgstr ""

#. Author of the plugin/theme
msgid "TaHUoP"
msgstr ""
