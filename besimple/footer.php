<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #total-container div element.
 *
 * @package besimple
 * @since besimple 1.0
 */
?>
    <div class="hFooter"></div>

</div>
<!--end of total-container div (start in header)-->

  <!-- .footer -->
  <div class="footer">

       <div class="footer-top">

            <div class="footer__container">


                 <div class="footerBox">

                      <div class="logoFooter">
                        <a href="<?php echo get_site_url(); ?>" class="footer_logo_link">
                          <?php if ( get_theme_mod('footer_logo_setting') != '' ) { ?>

                            <img src="<?php echo get_theme_mod('footer_logo_setting') ?>" alt="img" />

                            <?php } elseif  ( get_theme_mod('footer_logo_text') != '' ) {

                                echo get_theme_mod('footer_logo_text');

                            } else { ?>
                                <img src="<?php echo get_template_directory_uri().'/images/logo-footer.png' ?>" alt="img" />
                           <?php } ?>
                        </a>
                      </div>

                      <div class="infoFooter"><span><?php echo get_theme_mod('footer_label_change_setting')!='' ? get_theme_mod('footer_label_change_setting') : '© 2015. Be.Simple<br>'.__('All rights reserved','besimple'); ?></span></div>

                 </div>

                 <div class="footerBox">

                      <ul class="menuFooter">
                           <?php
                           $menu = wp_get_nav_menu_object('left-footer-menu');
                           $menu_items = wp_get_nav_menu_items($menu->term_id);
                           if(!empty($menu_items)){
                           foreach ($menu_items as $item) {?>
                               <li> <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a> </li>
                           <?php  } }?>
                      </ul>

                 </div>

                 <div class="footerBox">

                      <ul class="menuFooter">
                           <?php
                           $menu = wp_get_nav_menu_object('middle-footer-menu');
                           $menu_items = wp_get_nav_menu_items($menu->term_id);
                           if(!empty($menu_items)){
                           foreach ($menu_items as $item) {?>                                
                                <li> <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a> </li>
                           <?php  } }?>
                      </ul>

                 </div>

                 <div class="footerBox">

                      <span class="titleFooter"><?php echo __('FOLLOW US','besimple'); ?></span>
                      <ul class="socialNet-footer">
                           <?php
                           $menu = wp_get_nav_menu_object('social-footer-menu');
                           $menu_items = wp_get_nav_menu_items($menu->term_id);
                           if(!empty($menu_items)){
                           foreach ($menu_items as $item) {?>
                           <?php 
                          
                                if ( strtolower($item->title) == 'facebook' ) {
                                  $image = get_template_directory_uri() . '/images/f.png';                               
                                }
                                if ( strtolower($item->title) == 'twitter' ) {
                                  $image = get_template_directory_uri() . '/images/t.png';                               
                                }
                                if ( strtolower($item->title) == 'instagram' ) {
                                  $image = get_template_directory_uri() . '/images/i.png';                               
                                }
                              ?>
                                <li> <img src="<?php echo $image; ?>" alt="img"> <a  href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a> </li>
                           <?php  }} ?>
                      </ul>

               </div>

                 <div class="footerBox">
                <?php  if ( !is_user_logged_in() ) { 
                         if ( is_active_sidebar( 'footer-1' )) { 
                              dynamic_sidebar( 'footer-1' ); 
                          }                       
                                
                      } else { ?>

              

                      <span class="titleFooter"><?php echo __('SUBSCRIBE','besimple'); ?></span>
                      <div class="inputFooter">
                           <form>
                                <input class="inputFooter-text" type="text" placeholder="<?php echo __('EMAIL','besimple'); ?>" />
                               <input class="inputFooter-submit" type="submit" value="<?php echo __('SUBSCRIBE','besimple'); ?>" />
                          </form>
                     </div>
                <?php } ?>

                 </div>

            </div>

       </div>

       <div class="footer-bottom">

            <div class="footer__container">

                 <ul class="menuFooter-bottom">
                      <?php
                      $menu = wp_get_nav_menu_object('bottom-footer-menu');
                      $menu_items = wp_get_nav_menu_items($menu->term_id);
                      if(!empty($menu_items)){
                      foreach ($menu_items as $item) {?>
                           <li> <a href="<?php echo $item->url ?>"><?php echo $item->title ?></a> </li>
                      <?php  }} ?>
                 </ul>

                 <span class="infoFooter-bottom"><?php echo get_theme_mod('footer_tagline_change_setting')!='' ? get_theme_mod('footer_tagline_change_setting') : 'Be. Simple - '.__( 'Premium Wordpress Theme', 'besimple'); ?></span>

            </div>

       </div>

  </div>
  <!-- END .footer -->
      
    
    
      

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>


<?php
wp_footer();
function footer_code_add(){
    echo get_theme_mod( 'footer_code_add_setting' );
}
?>
</body>
</html>