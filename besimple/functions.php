<?php
/**
 * besimple functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package besimple
 */


if ( ! function_exists( 'besimple_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function besimple_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on besimple, use a find and replace
	 * to change 'besimple' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'besimple', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
    // add_image_size( 'slides', 1920, 485, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'besimple' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'besimple_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'besimple_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function besimple_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'besimple_content_width', 640 );
}
add_action( 'after_setup_theme', 'besimple_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
// remove_filter( 'the_content', 'wpautop' );
function besimple_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'besimple' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar-subscribe', 'besimple' ),
        'id'            => 'sidebar-subscribe',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar-google-ads', 'besimple' ),
        'id'            => 'sidebar-google-ads',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
   register_sidebar( array(
        'name'          => esc_html__( 'Footer', 'besimple' ),
        'id'            => 'footer-1',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          =>  esc_html__( 'Popular', 'besimple' ),
        'id'            => 'popular-007',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'besimple_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function modify_jquery() {
    if (!is_admin()) {

        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', false, '1.12.0');
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'modify_jquery');
function besimple_scripts() {
	wp_enqueue_style( 'besimple-style', get_stylesheet_uri() );
    wp_enqueue_style( 'besimple-slick', get_template_directory_uri() . '/css/slick.css' );
    wp_enqueue_style( 'besimple-fonts', get_template_directory_uri() . '/css/fonts.css' );

	wp_enqueue_script( 'besimple-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
    wp_register_script( 'besimple-slick', get_template_directory_uri() . '/js/slick.min.js', array(), '2016', true);
	wp_enqueue_script( 'besimple-infinitescroll', get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', array(), '20130115', true );
    wp_enqueue_script( 'besimple-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), '201301', true );


//	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
//		wp_enqueue_script( 'comment-reply' );
//	}

//    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.8.2.js');
    wp_register_script( 'control-page', get_template_directory_uri() . '/js/control-page.js', array('jquery'), '1.0');

    wp_localize_script('control-page', 'No_more_posts', __('No more posts to load', 'besimple'));
    wp_enqueue_script( 'control-page' );
//    wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.5.2' );
//    wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '3.3.2' );
//    wp_enqueue_script( 'infinitescroll', get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', array('jquery'), '2.1.0');

}
add_action( 'wp_enqueue_scripts', 'besimple_scripts' );


require get_template_directory() . '/widgets/popular_post_widget.php';
require get_template_directory() . '/widgets/recent_post_widget.php';
require get_template_directory() . '/widgets/recent_comments_besimple_widget.php';
require get_template_directory() . '/widgets/besimple_categories.php';
require get_template_directory() . '/widgets/subscribe_and_follow_widget.php';
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// include get_template_directory() . '/inc/wordpress-theme-customizer-custom-controls/theme-customizer-demo.php';


require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'beSimple_register_required_plugins' );

function beSimple_register_required_plugins() {
    $plugins = array(
        array(
            'name'         => 'Contact Form 7',
            'slug'         => 'contact-form-7',
            'required'     => false,
            'source' => 'https://wordpress.org/plugins/contact-form-7/',
        ),
        // array(
        //     'name'         => 'Menu Image',
        //     'slug'         => 'menu-image',
        //     'required'     => true,
        //     'source' => 'https://wordpress.org/plugins/menu-image/',
        // ),
        array(
            'name'         => 'Subscribe2',
            'slug'         => 'subscribe2',
            'required'     => true,
            'source' => 'https://wordpress.org/plugins/subscribe2/',
        ),
        // array(
        //     'name'         => 'Widget Importer & Exporter',
        //     'slug'         => 'widget-importer-exporter',
        //     'required'     => true,
        //     'source' => 'https://wordpress.org/plugins/widget-importer-exporter/',
        // ),
        array(
            'name'         => 'WordPress Importer',
            'slug'         => 'wordpress-importer',
            'required'     => true,
            'source' => 'https://wordpress.org/plugins/wordpress-importer/',
        ),
        array(
            'name'         => 'Widget Settings Importer/Exporter',
            'slug'         => 'widget-settings-importexport',
            'required'     => true,
            'source' => 'https://wordpress.org/plugins/widget-settings-importexport/',
        ),
        array(
            'name'                  => 'One Click',
            'slug'                  => 'one-click',
            'source'                => get_template_directory() . '/plugins/one-click.zip',
            'required'              => false,
            'is_automatic'          => true,
        ),
        array(
            'name'                  => 'WPBakery Visual Composer',
            'slug'                  => 'js-composer',
            'source'                => get_template_directory() . '/plugins/js_composer.zip',
            'required'              => false,
            'is_automatic'          => true,
        ),
        array(
            'name'                  => 'Widget Logic',
            'slug'                  => 'widget-logic',
            'source'                => 'https://wordpress.org/plugins/widget-logic/',
            'required'              => false,
            'is_automatic'          => true,
        ),

    );

    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.


    );

    tgmpa( $plugins, $config );
}
/**
 * Register menu for header.
 */
function register_my_menus() {
    register_nav_menu('header-menu',__( 'Header Menu', 'besimple'  ));
    register_nav_menu('left-footer-menu',__( 'Left Footer Menu', 'besimple'  ));
    register_nav_menu('middle-footer-menu',__( 'Middle Footer Menu', 'besimple'  ));
    register_nav_menu('social-footer-menu',__( 'Social Footer Menu', 'besimple'  ));
    register_nav_menu('bottom-footer-menu',__( 'Bottom Footer Menu', 'besimple'  ));
    register_nav_menu('social-menu',__( 'Social Menu', 'besimple'  ));
}
add_action( 'init', 'register_my_menus' );

/**
 * Creating taxonomy for news post type.
 */
function news_category_init() {
    // create a new taxonomy
    register_taxonomy(
        'news_category',
        'news',
        array(
            'hierarchical' => true,
            'label' => __( 'News Category', 'besimple'  ),
            'rewrite' => array( 'slug' => 'News Category' )
        )
    );
}
add_action( 'init', 'news_category_init' );

/**
 * Creating slides post type for front-page slider and news post type for header.
 */
function create_post_type() {
    register_post_type( 'slides',
        array(
            'labels' => array(
                'name' => __( 'Slides', 'besimple'  ),
                'singular_name' => __( 'Slide', 'besimple'  )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail')
        )
    );
    register_post_type( 'news',
        array(
            'labels' => array(
                'name' => __( 'News', 'besimple'  ),
                'singular_name' => __( 'News' , 'besimple' )
            ),
            'taxonomies' => array('news_category'),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title','editor','author','excerpt','comments','revisions')
        )
    );
}
add_action( 'init', 'create_post_type' );

/**
 * Removing brackets and dots from excerpt.
 */
function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

/**
 * Limits excerpt length (suddenly).
 */
function custom_limit_excerpt($excerpt, $word_limit)
{
    $words = explode(" ",$excerpt);
    return implode(" ",array_splice($words,0,$word_limit));
}

/**
 * Create custom metabox with different field types for post and news header
 */
//include the main class file
require_once("meta-box-class/my-meta-box-class.php");
/**
* configure your meta box
*/
$config = array(
    'id' => 'header_meta_box',             // meta box id, unique per meta box
    'title' => __('Header Meta Box', 'besimple' ),      // meta box title
    'pages' => array('post', 'news'),    // post types, accept custom post types as well, default is array('post'); optional
    'context' => 'normal',               // where the meta box appear: normal (default), advanced, side; optional
    'priority' => 'high',                // order of meta box: high (default), low; optional
    'fields' => array(),                 // list of meta fields (can be added by field arrays) or using the class's functions
    'local_images' => false,             // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true            //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
);
/*
* Initiate your meta box
*/
$my_meta = new AT_Meta_Box($config);
/*
* Add fields to your meta box
*/
//Image field
// $my_meta->addImage($prefix.'image_field_id',array('name'=> __('Post Header Image ', 'besimple' )));
$my_meta->addImage('image_field_id',array('name'=> __('Post Header Image ', 'besimple' )));
/*
* Don't Forget to Close up the meta box deceleration
*/
//Finish Meta Box Deceleration
$my_meta->Finish();

$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
$template_file = get_post_meta($post_id,'_wp_page_template', TRUE);

if ($template_file == 'aboutme.php' || $template_file == 'front-full.php' || $template_file == 'page.php' ) {
     /**
     * Create custom metabox with selects for pages with related post
     */
    $config = array(
        'id' => 'related_post_meta_box',             // meta box id, unique per meta box
        'title' => __('Related Post Meta Box', 'besimple' ),      // meta box title
        'pages' => array('page'),    // post types, accept custom post types as well, default is array('post'); optional
        'context' => 'normal',               // where the meta box appear: normal (default), advanced, side; optional
        'priority' => 'high',                // order of meta box: high (default), low; optional
        'fields' => array(),                 // list of meta fields (can be added by field arrays) or using the class's functions
        'local_images' => false,             // Use local or hosted images (meta box images for add/remove)
        'use_with_theme' => true            //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
    );
    /*
    * Initiate your meta box
    */
    $related_post_meta = new AT_Meta_Box($config);
    /*
    * Add fields to your meta box
    */

//select field
    if ($template_file == 'aboutme.php') {
        $related_post_meta->addPosts('related_post_field_1',array('post_type' => array( 'post', 'news' )), array('name'=> __('Post', 'besimple' )));
        $related_post_meta->addPosts('related_post_field_2',array('post_type' => array( 'post', 'news' )), array('name'=> __('Post', 'besimple' )));
        $related_post_meta->addPosts('related_post_field_3',array('post_type' => array( 'post', 'news' )), array('name'=> __('Post', 'besimple' )));
    }
    if ( $template_file == 'front-full.php' ||  $template_file == 'page.php' ) {
       $related_post_meta->addPosts('posts_field_id_1',array('post_type' => 'post'),array('name'=> __('Post', 'besimple' )));
    }

    /*
    * Don't Forget to Close up the meta box deceleration
    */
//Finish Meta Box Deceleration
    $related_post_meta->Finish();
}
if ( $template_file == 'contacts.php' ) {
    /**
     * Create custom metabox with selects for pages with related post
     */
    $config = array(
        'id' => 'contacts_meta_box',             // meta box id, unique per meta box
        'title' => __('Shortcode Meta Box', 'besimple' ),      // meta box title
        'pages' => array('page'),    // post types, accept custom post types as well, default is array('post'); optional
        'context' => 'normal',               // where the meta box appear: normal (default), advanced, side; optional
        'priority' => 'high',                // order of meta box: high (default), low; optional
        'fields' => array(),                 // list of meta fields (can be added by field arrays) or using the class's functions
        'local_images' => false,             // Use local or hosted images (meta box images for add/remove)
        'use_with_theme' => true            //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
    );
    /*
    * Initiate your meta box
    */
    $contacts_meta = new AT_Meta_Box($config);
    /*
    * Add fields to your meta box
    */
    $contacts_meta->addText('contacts_shortcode_field',array('name'=> __('Shortcode', 'besimple')));

    /*
    * Don't Forget to Close up the meta box deceleration
    */
//Finish Meta Box Deceleration
    $contacts_meta->Finish();
}

// ==========================================================================================
 $config = array(
    'id' => 'related_post_meta_box',             // meta box id, unique per meta box
    'title' => __('Related Post Meta Box', 'besimple' ),      // meta box title
    'pages' => array('slides'),    // post types, accept custom post types as well, default is array('post'); optional
    'context' => 'normal',               // where the meta box appear: normal (default), advanced, side; optional
    'priority' => 'high',                // order of meta box: high (default), low; optional
    'fields' => array(),                 // list of meta fields (can be added by field arrays) or using the class's functions
    'local_images' => false,             // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true            //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
);

 $related_post_slides = new AT_Meta_Box($config);

 $related_post_slides->addPosts('posts_field_id_slides_1',array('post_type' => array( 'post', 'news' )), array('name'=> __('Post', 'besimple' )));
 $related_post_slides->Finish();
/**
 * Register customization api for theme
 */
function besimple1_customize_register( $wp_customize ) {
    //Display today news
   $wp_customize->add_section( 'header' , array(
       'title'      => __( 'Header', 'besimple' )
   ));
   $wp_customize->add_setting( 'today_news_display_setting' , array(
       'default'     => '1',
       'transport'   => 'refresh'
   ));
   $wp_customize->add_control( 'today_news_display_control' , array(
       'label'        => __( 'Display News Header', 'besimple' ),
       'section'    => 'header',
       'settings'   => 'today_news_display_setting',
       'type'     => 'checkbox',
       'description' =>  __( 'Show and hide Today News section', 'besimple' )
   ));

    //Upload header logo
    $wp_customize->add_setting( 'header_logo_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo_setting', array(
        'label'    => __( 'Header Logo', 'besimple' ),
        'section'  => 'header',
        'settings' => 'header_logo_setting',
        'description' =>  __( 'Upload a logo to replace the default in the header', 'besimple' )
    ) ) );

    $wp_customize->add_setting( 'header_text_logo' , array(
       'default'     => '',
       'transport'   => 'refresh'
   ));
   $wp_customize->add_control( 'header_text_logo_control' , array(
       'label'        => __( 'Header logo text', 'besimple' ),
       'section'    => 'header',
       'settings'   => 'header_text_logo',
       'type'     => 'text'
   ));

   $wp_customize->add_setting( 'header_text_logo_bottom' , array(
       'default'     => '',
       'transport'   => 'refresh'
   ));
   $wp_customize->add_control( 'header_text_logo_bottom_control' , array(
       'label'        => __( 'Header bottom logo text', 'besimple' ),
       'section'    => 'header',
       'settings'   => 'header_text_logo_bottom',
       'type'     => 'text'
   ));


    //Add executable code in footer
    $wp_customize->add_section( 'footer' , array(
        'title'      => __( 'Footer', 'besimple' )
    ));
    $wp_customize->add_setting( 'footer_code_add_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    $wp_customize->add_control( 'footer_code_add_control' , array(
        'label'        => __( 'Code field', 'besimple' ),
        'section'    => 'footer',
        'settings'   => 'footer_code_add_setting',
        'type'     => 'textarea',
        'description' => __( 'Insert code with tags here', 'besimple' )
    ));

    //Change tagline in footer
    $wp_customize->add_setting( 'footer_tagline_change_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    $wp_customize->add_control( 'footer_tagline_change_control' , array(
        'label'        => __( 'Tagline field', 'besimple' ),
        'section'    => 'footer',
        'settings'   => 'footer_tagline_change_setting',
        'type'     => 'text'
    ));

    //Change label in footer
    $wp_customize->add_setting( 'footer_label_change_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    $wp_customize->add_control( 'footer_label_change_control' , array(
        'label'        => __( 'Label field', 'besimple' ),
        'section'    => 'footer',
        'settings'   => 'footer_label_change_setting',
        'type'     => 'text',
        'description' => __( 'Use "br" tag to change the line', 'besimple' )
    ));

    //Upload header logo
    $wp_customize->add_setting( 'footer_logo_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo_setting', array(
        'label'    => __( 'Logo', 'besimple' ),
        'section'  => 'footer',
        'settings' => 'footer_logo_setting',
        'description' =>  __( 'Upload a logo to replace the default in the footer', 'besimple' )
    ) ) );

    $wp_customize->add_setting( 'footer_logo_text' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( 'footer_logo_text_control', array(
        'label'    => __( 'Logo text', 'besimple' ),
        'section'  => 'footer',
        'settings' => 'footer_logo_text',
        'type'     => 'text',
        'description' =>  __( 'Write logo text', 'besimple' )
    ));


    //Change color scheme
    $wp_customize->add_setting( 'color_scheme_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_scheme_setting', array(
        'label'    => __( 'Color scheme', 'besimple' ),
        'section'  => 'colors',
        'settings' => 'color_scheme_setting',
        'description' =>  __( 'Change color scheme of the site', 'besimple' )
    ) ) );
    $wp_customize->add_setting( 'hover_color_scheme_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'hover_color_scheme_setting', array(
        'section'  => 'colors',
        'settings' => 'hover_color_scheme_setting',
        'description' =>  __( 'Change hover color', 'besimple' )
    ) ) );

    //Add executable code in footer
    $wp_customize->add_section( 'footer' , array(
        'title'      => __( 'Footer', 'besimple' )
    ));
    $wp_customize->add_setting( 'footer_code_add_setting' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    $wp_customize->add_control( 'footer_code_add_control' , array(
        'label'        => __( 'Code field', 'besimple' ),
        'section'    => 'footer',
        'settings'   => 'footer_code_add_setting',
        'type'     => 'textarea',
        'description' => __( 'Insert code with tags here', 'besimple' )
    ));

     //Add page customize
    $wp_customize->add_section( 'front_page' , array(
        'title'      => __( 'Position customize', 'besimple' )
    ));
    $wp_customize->add_setting( 'front_page_change_slider' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    $wp_customize->add_control( 'front_page_change_slider_control' , array(
        'label'        => __( 'Front page', 'besimple' ),
        'section'    => 'front_page',
        'settings'   => 'front_page_change_slider',
        'type'     => 'radio',
        'choices'    => array(
            'side' => 'side',
            'full' => 'full',   
        ),
        'description' => __( 'Slider', 'besimple' )
    ));

    $wp_customize->add_setting( 'front_page_add_block' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    
    $wp_customize->add_control( 'front_page_add_block_control' , array(
        'label'        => __( 'Show page block', 'besimple' ),
        'section'    => 'front_page',
        'settings'   => 'front_page_add_block',
        'type'     => 'checkbox',        
        // 'description' => __( 'Show block' , 'besimple' )
    ));
// ================================================================
    $wp_customize->add_setting( 'front_page_box_slider' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ));
    
    $wp_customize->add_control( 'front_page_box_slider_control' , array(
        'label'        => __( 'Box slider', 'besimple' ),
        'section'    => 'front_page',
        'settings'   => 'front_page_box_slider',
        'type'     => 'checkbox',
        // 'description' => __( 'Show block' , 'besimple' )
    ));
// =======================================================================
    $wp_customize->add_setting( 'sidebar_display_setting' , array(
       'default'     => '',
       'transport'   => 'refresh'
   ));

   $wp_customize->add_control( 'sidebar_display_control' , array(
       'label'        => __( 'Sidebar', 'besimple' ),
       'section'    => 'front_page',
       'settings'   => 'sidebar_display_setting',
       'type'     => 'radio',
       'choices'    => array(
            'left' => 'left',
            'right' => 'right',
            'full' => 'full',
        ),
      
   ));

    //Change sidebar
   //  $wp_customize->add_section( 'sidebar' , array(
   //     'title'      => __( 'Sidebar', 'besimple' )
   // ));

    //-----------------------------Change fonts-------------------------------------------
    $wp_customize->add_section( 'google_font_section', array(
        'title'          => __( 'Fonts' , 'besimple' ),
        'priority'       => 36,
    ) );

    require_once get_template_directory() . '/inc/wordpress-theme-customizer-custom-controls/select/google-font-dropdown-custom-control.php';
    $wp_customize->add_setting( 'google_font_setting', array(
        'default'        => '',
    ) );
    $wp_customize->add_control( new Google_Font_Dropdown_Custom_Control( $wp_customize, 'google_font_setting', array(
        'label'   => __( 'Google Font Setting' , 'besimple' ),
        'section' => 'google_font_section',
        'settings'   => 'google_font_setting',
        'priority' => 12
    ) ) );

    //-----------------------------Change fonts-------------------------------------------


    /*-----------------------------Add social menu in header-----------------------------*/
    $wp_customize->add_section( 'header_social_menu' , array(
        'title'      => __( 'Header social menu', 'besimple' )
    ));

    //Add first social network
    $wp_customize->add_setting( 'header_social_img_setting_1' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_img_setting_1', array(
        'label'    => __( 'First social network', 'besimple' ),
        'section'  => 'header_social_menu',
        'settings' => 'header_social_img_setting_1',
        'description' => __( 'Social network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_hov_img_setting_1' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_hov_img_setting_1', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_hov_img_setting_1',
        'description' => __( 'Social network hover image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_link_setting_1' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( 'header_social_link_control_1', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_link_setting_1',
        'type'     => 'text',
        'description' => __( 'Social network link', 'besimple')
    ) );

    //Add Second social network
    $wp_customize->add_setting( 'header_social_img_setting_2' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_img_setting_2', array(
        'label'    => __( 'Second social network', 'besimple' ),
        'section'  => 'header_social_menu',
        'settings' => 'header_social_img_setting_2',
        'description' => __( 'Social network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_hov_img_setting_2' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_hov_img_setting_2', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_hov_img_setting_2',
        'description' => __( 'Social network hover image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_link_setting_2' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( 'header_social_link_control_2', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_link_setting_2',
        'type'     => 'text',
        'description' => __( 'Social network link', 'besimple')
    ) );

    //Add Third social network
    $wp_customize->add_setting( 'header_social_img_setting_3' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_img_setting_3', array(
        'label'    => __( 'Third social network', 'besimple' ),
        'section'  => 'header_social_menu',
        'settings' => 'header_social_img_setting_3',
        'description' => __( 'Social network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_hov_img_setting_3' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_hov_img_setting_3', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_hov_img_setting_3',
        'description' => __( 'Social network hover image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_link_setting_3' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( 'header_social_link_control_3', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_link_setting_3',
        'type'     => 'text',
        'description' => __( 'Social network link', 'besimple')
    ) );

    //Add Fourth social network
    $wp_customize->add_setting( 'header_social_img_setting_4' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_img_setting_4', array(
        'label'    => __( 'Fourth social network', 'besimple' ),
        'section'  => 'header_social_menu',
        'settings' => 'header_social_img_setting_4',
        'description' => __( 'Social network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_hov_img_setting_4' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_hov_img_setting_4', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_hov_img_setting_4',
        'description' => __( 'Social network hover image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_link_setting_4' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( 'header_social_link_control_4', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_link_setting_4',
        'type'     => 'text',
        'description' => __( 'Social network link', 'besimple')
    ) );

    //Add Fifth social network
    $wp_customize->add_setting( 'header_social_img_setting_5' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_img_setting_5', array(
        'label'    => __( 'Fifth social network', 'besimple' ),
        'section'  => 'header_social_menu',
        'settings' => 'header_social_img_setting_5',
        'description' => __( 'Social network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_hov_img_setting_5' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_social_hov_img_setting_5', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_hov_img_setting_5',
        'description' => __( 'Social network hover image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'header_social_link_setting_5' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( 'header_social_link_control_5', array(
        'section'  => 'header_social_menu',
        'settings' => 'header_social_link_setting_5',
        'type'     => 'text',
        'description' => __( 'Social network link', 'besimple')
    ) );
    /*-----------------------------End of social menu in header-----------------------------*/

    /*-----------------------------Add social menu in post-----------------------------*/
    $wp_customize->add_section( 'post_social_menu' , array(
        'title'      => __( 'Post social menu', 'besimple' )
    ));

    //Add Facebook social network
    $wp_customize->add_setting( 'post_social_img_setting_1' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_img_setting_1', array(
        'label'    => __( 'Facebook social network', 'besimple' ),
        'section'  => 'post_social_menu',
        'settings' => 'post_social_img_setting_1',
        'description' => __( 'Facebook network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'post_social_hov_img_setting_1' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_hov_img_setting_1', array(
        'section'  => 'post_social_menu',
        'settings' => 'post_social_hov_img_setting_1',
        'description' => __( 'Facebook network hover image', 'besimple')
    ) ) );


    //Add Twitter social network
    $wp_customize->add_setting( 'post_social_img_setting_2' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_img_setting_2', array(
        'label'    => __( 'Twitter social network', 'besimple' ),
        'section'  => 'post_social_menu',
        'settings' => 'post_social_img_setting_2',
        'description' => __( 'Twitter network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'post_social_hov_img_setting_2' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_hov_img_setting_2', array(
        'section'  => 'post_social_menu',
        'settings' => 'post_social_hov_img_setting_2',
        'description' => __( 'Twitter network hover image', 'besimple')
    ) ) );

   
    //Add Google+ social network
    $wp_customize->add_setting( 'post_social_img_setting_3' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_img_setting_3', array(
        'label'    => __( 'Google+ social network', 'besimple' ),
        'section'  => 'post_social_menu',
        'settings' => 'post_social_img_setting_3',
        'description' => __( 'Google+ network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'post_social_hov_img_setting_3' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_hov_img_setting_3', array(
        'section'  => 'post_social_menu',
        'settings' => 'post_social_hov_img_setting_3',
        'description' => __( 'Google+ network hover image', 'besimple')
    ) ) );

  
    //Add Pinterest social network
    $wp_customize->add_setting( 'post_social_img_setting_4' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_img_setting_4', array(
        'label'    => __( 'Pinterest social network', 'besimple' ),
        'section'  => 'post_social_menu',
        'settings' => 'post_social_img_setting_4',
        'description' => __( 'Pinterest network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'post_social_hov_img_setting_4' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_hov_img_setting_4', array(
        'section'  => 'post_social_menu',
        'settings' => 'post_social_hov_img_setting_4',
        'description' => __( 'Pinterest network hover image', 'besimple')
    ) ) );

    //Add Fifth social network
    $wp_customize->add_setting( 'post_social_img_setting_5' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_img_setting_5', array(
        'label'    => __( 'Fifth social network', 'besimple' ),
        'section'  => 'post_social_menu',
        'settings' => 'post_social_img_setting_5',
        'description' => __( 'Social network image', 'besimple')
    ) ) );

    $wp_customize->add_setting( 'post_social_hov_img_setting_5' , array(
        'default'     => '',
        'transport'   => 'refresh'
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'post_social_hov_img_setting_5', array(
        'section'  => 'post_social_menu',
        'settings' => 'post_social_hov_img_setting_5',
        'description' => __( 'Social network hover image', 'besimple')
    ) ) );

    /*-----------------------------End of social menu in post-----------------------------*/

    
    
    
      
    
    
    

    $wp_customize->remove_control("header_image");
    $wp_customize->remove_control("header_textcolor");
    $wp_customize->remove_control("background_color");
    $wp_customize->remove_section("background_image");

}

add_action( 'customize_register', 'besimple1_customize_register' );

remove_action('template_redirect', 'redirect_canonical');


if(get_theme_mod( 'front_page_box_slider' ) != '' ) {    
    function beSimple_customize_css() {
        ?>
             <style type="text/css">
                 @media screen and (min-width: 1222px) { 
                    .slick-slider {
                        width: 1140px !important;
                        margin-left: 20px !important; 
                    }
                 }                
             </style>
        <?php
    }
    add_action( 'wp_head', 'beSimple_customize_css');    
}



function mytheme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <article id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <footer class="comment-meta">
    <div class="comment-author vcard">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    <?php printf( __( '<b class="fn">%s</b> <span class="says">'.__( 'says', 'besimple').':</span>' ), get_comment_author_link() ); ?>
    </div>
    <?php if ( $comment->comment_approved == '0' ) : ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'besimple'); ?></em>
        <br />
    <?php endif; ?>

    <div class="comment-metadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
            <?php
            /* translators: 1: date, 2: time */
//            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?><!--</a>--><?php //edit_comment_link( __( '(Edit)' ), '  ', '' );
            echo get_comment_date('F j, Y'); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
        ?>
    </div>
    </footer>

    <?php comment_text(); ?>

    <div class="reply">
        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
        </article>
    <?php endif; ?>
    <?php
}