jQuery(document).ready(function(){ /* ... */
    //jQuery('.comment-respond').css("display","none");

    //Add styles to comment form
    jQuery('.comment-respond').addClass("leaveReply");
    jQuery('.comment-reply-title').addClass("leaveReply__title");
    jQuery('.form-submit').addClass("leaveReply__button");
    jQuery('.leaveReply-input').insertBefore(".leaveReply-comment");

    //Add styles to footer subscribtion form
    jQuery("label[for='s2email']").hide();
    jQuery('.footerBox #s2email').attr({
        value: "EMAIL",
        onfocus: "if (this.value == 'EMAIL') {this.value = '';}",
        onblur: "if (this.value == '') {this.value = 'EMAIL';}"
    });
    jQuery('.footerBox #s2email').addClass('inputFooter-text');
    jQuery('.footerBox .inputFooter :submit').addClass("inputFooter-submit");

    //Add styles to sidebar subscribtion form
    jQuery('.content-left .s2_form_widget').addClass("newsletter");
    jQuery('.content-left .s2_form_widget .widget-title').addClass("newsletter__name");
    jQuery('.content-left #s2email').attr({
        value: "Your Email",
        onfocus: "if (this.value == 'Your Email') {this.value = '';}",
        onblur: "if (this.value == '') {this.value = 'Your Email';}"
    });
    jQuery('.content-left #s2email').addClass("inputNewsletter-text");
    jQuery('.content-left .newsletter__text :submit').addClass("inputNewsletter-submit");


    // social 
    $(".js-social-share").on("click", function(e) {
      e.preventDefault();

      windowPopup($(this).attr("href"), 500, 500);
    });

    // Subscribe errors handler
    if( jQuery('.s2_error').html() !== undefined){
        jQuery('.subscr_thanks').html(jQuery('.s2_error').html());
    }

});
jQuery(window).load(function(){
 masonry.initialization();

var $container = jQuery('.box');
$container.infinitescroll({
  navSelector  : '.navigation',    // selector for the paged navigation
  nextSelector : '.next',  // selector for the NEXT link (to page 2)
  itemSelector : '.box_item',     // selector for all items you'll retrieve
  debug: true,
  donetext     : "I think we've hit the end, Jim" ,
  loading: {
      //finishedMsg: "<script type='text/javascript'> jQuery('.morePosts-button').hide(); </script>",
      finishedMsg: "<script type='text/javascript'> jQuery('.morePosts-button').html(No_more_posts); </script>",
      // img: 'http://i.imgur.com/6RMhx.gif'
    }
  },
  // trigger Masonry as a callback
  function( newElements ) {
    var $newElems = jQuery( newElements );
    $container.append( $newElems ).masonry( 'appended', $newElems );
    jQuery('#infscr-loading').remove();
    $container.masonry('layout');
    $container.masonry('reloadItems');
    // $container.append( $newElems );
  }
);

    //Message for user on empty email field
    jQuery('.inputFooter-submit').click(function(e) {
//            alert(jQuery('.s2_error').html());
            if ( jQuery('.inputFooter form p #s2email').val() === undefined ) {
                e.preventDefault();
                alert('You are admin');
            }else if ( jQuery('.inputFooter form p #s2email').val() == 'EMAIL'){
            e.preventDefault();
            alert('Input your email');
        }else if(jQuery('.inputFooter form p #s2email').val().match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) === null){/*(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myForm.emailAddr.value)!=true){*/
            e.preventDefault();
            alert('Your email is incorrect');
        }
    });

    jQuery('.inputNewsletter-submit').click(function(e) {
        if ( jQuery('.newsletter__text form p #s2email').val() === undefined ) {
            e.preventDefault();
            alert('You are admin');
        }else if ( jQuery('.newsletter__text form p #s2email').val() == 'Your Email'){
            e.preventDefault();
            alert('Input your email');
        }else if(jQuery('.newsletter__text form p #s2email').val().match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) === null){/*(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myForm.emailAddr.value)!=true){*/
            e.preventDefault();
            alert('Your email is incorrect');
        }
    });
//
//jQuery('.inputNewsletter-submit').click(function(e) {
//    if ( !jQuery('.inputNewsletter-text').val() ) {
//        e.preventDefault();
//        alert('You are admin');
//    }
//});
//
//    jQuery('.inputFooter-text#s2email').click(function(e) {
//            alert('Input your email');
//    });
jQuery('#infscr-loading').remove();


jQuery(window).unbind('.infscr');

    // Resume Infinite Scroll
    jQuery('.morePosts-button').click(function(){
        $container.infinitescroll('retrieve');
         $container.masonry('reloadItems');
        // $container.masonry('layout');
        return false;
    });

    sliderHome.initialization();
    menu.initialization();
    boxEvents.initialization();
    content.initialization();
    todayNews.initialization();





    /* Comments --------------------------------- */

    jQuery('.show-hide-comments').click(function() {
        if ( jQuery('#comments').css('display') == 'none' ){
            jQuery('#comments').show();
            jQuery('.comment-respond').show();
        }else if( jQuery('#comments').css('display') != 'none' ){
            jQuery('#comments').hide();
            jQuery('.comment-respond').hide();
        }
    });
console.log(window.location);
    if(window.location.hash!=""){
        jQuery('#comments').show();
        jQuery('.comment-respond').show();
        jQuery("html, body").animate({ scrollTop: jQuery(window.location.hash).offset().top }, 500);
    }

    if(window.location.search.indexOf('scroll=comments')!=-1){
        jQuery('#comments').show();
        jQuery('.comment-respond').show();
        jQuery("html, body").animate({ scrollTop: jQuery("#comments").offset().top }, 500);
    }
});




/* todayNews ---------------------------------- */
var todayNews = {
    add: 'open',
    box: '.todayNews',
    butt: '.todayNews-title',
    cont: '.todayNews__container',
    tim: 500,
    x: 768
};

todayNews.initialization = function(){
     
    todayNews.events();
    todayNews.res();
    
    jQuery(window).resize(function(){
        todayNews.res();
    }); 
};

todayNews.events = function(){
    
    jQuery(todayNews.butt).on({
         click: function(){
             
             if(jQuery('body').width() < todayNews.x){
                 
                 if(jQuery(this).parents(todayNews.box).find(todayNews.cont).hasClass(todayNews.add)){
                     
                     jQuery(this).parents(todayNews.box).find(todayNews.cont).slideUp(todayNews.tim);
                     jQuery(this).parents(todayNews.box).find(todayNews.cont).removeClass(todayNews.add);
                 }else{
                     
                     jQuery(this).parents(todayNews.box).find(todayNews.cont).slideDown(todayNews.tim);
                     jQuery(this).parents(todayNews.box).find(todayNews.cont).addClass(todayNews.add);
                 }
             }
         }
     });
};

todayNews.res = function(){
    
    if(window.innerWidth > todayNews.x){
        
        jQuery(todayNews.cont).removeAttr('style');
        jQuery(todayNews.cont).removeClass(todayNews.add);

        jQuery('.todayNews .todayNews-box:first').prependTo(jQuery('.todayNews__container'));
    }else{
         
        jQuery('.todayNews-box').eq(0).prependTo(jQuery('.todayNews'));
    }
};





/* masonry ------------------------------------ */
var masonry = {
    less: {
        box: '.boxLess',
        cont: '.boxLess__cont'
    },
    big: {
        box: '.boxBig',
        cont: '.boxBig__cont'
    } 
};

masonry.initialization = function(){
     
     masonry.events();
};

masonry.events = function(){
    
    if(jQuery(masonry.less.box).length != 0) {
    
        jQuery(masonry.less.box).masonry({
              itemSelector: masonry.less.cont,
              singleMode: true,
              isResizable: true,
              isAnimated: true,
              animationOptions: { 
                   queue: false, 
                   duration: 500 
              }
        });
    }
    
    if(jQuery(masonry.big.box).length != 0){
    
        jQuery(masonry.big.box).masonry({
              itemSelector: masonry.big.cont,
              singleMode: true,
              isResizable: true,
              isAnimated: true,
              animationOptions: { 
                   queue: false, 
                   duration: 500 
              }
        });
    }
    
};





/* content ------------------------------------ */
var content = {
    x: 768
};

content.initialization = function(){
    
    content.res();
    
    jQuery(window).resize(function(){
        content.res();
    });
};

content.res = function(){
     
    //console.log(jQuery('body').width() + ' - ' + jQuery('html').width() + ' - ' + jQuery(document).width() + ' - ' + jQuery(window).width() + ' - ' + window.innerWidth);
     
    
    if(window.innerWidth > content.x){
        
        jQuery('.content-left').prependTo(jQuery('.content-cont'));
        jQuery('.popularPosts-text').insertAfter(jQuery('.content-cont'));
        
    }else{
        
        jQuery('.content-left').appendTo(jQuery('.content-cont'));
        jQuery('.popularPosts-text').insertAfter(jQuery('.left-content-less'));
    }
};





/* boxEvents ---------------------------------- */
var boxEvents = {
    x: 920
};

boxEvents.initialization = function(){
    
    boxEvents.res();
    
    jQuery(window).resize(function(){
        boxEvents.res();
    });
};

boxEvents.res = function(){
    
    if(window.innerWidth > boxEvents.x){
        
         jQuery('.eventsBox--info').eq(0).prependTo(jQuery('.events__cont').eq(0));
         jQuery('.eventsBox--info').eq(1).insertAfter(jQuery('.events__cont').eq(1).find('.eventsBox--block:first'));

    }else{
         
         jQuery('.eventsBox--info').eq(0).appendTo(jQuery('.events__cont').eq(0));
         jQuery('.eventsBox--info').eq(1).appendTo(jQuery('.events__cont').eq(1));
    }
    
};





/* sliderHome --------------------------------- */
var sliderHome = {
    cont: '.slideBox',
    contSide: '.slideSide'
};

sliderHome.initialization = function(){
    
    if(jQuery(sliderHome.cont).length != 0){
    
        jQuery(sliderHome.cont).slick({
            dots: true,
            infinite: true,
            speed: 700,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 5000
        });
        
    }
     
    if(jQuery(sliderHome.contSide).length != 0){
    
        jQuery(sliderHome.contSide).slick({
            dots: true,
            speed: 600,
            autoplay: true,
            autoplaySpeed: 5000
        });
        
    }
    
};
		   
						   
						   


/* menu --------------------------------------- */
var menu = {
    add: 'open',
    cont: '.menu-block',
    sub: '.submenu',
    butt: '.menu-block li a',
    menuCont: '.menuCont',     
    navBox: '.mobile-nav',
    navButt: '.mobile-nav__butt',
    tim: 500,
    x: 768
};

menu.initialization = function(){
    
    jQuery(window).resize(function(){
		menu.res();
    });
    
    menu.events();
};

menu.events = function(){
    
    jQuery(menu.navButt).on({
         click: function(){
             
             if(jQuery(this).parents(menu.cont).find(menu.menuCont).hasClass(menu.add)){
               
                 jQuery(this).parents(menu.cont).find(menu.menuCont).slideUp(menu.tim);
                 jQuery(this).parents(menu.cont).find(menu.menuCont).removeClass(menu.add);
             }else{
                 
                 jQuery(this).parents(menu.cont).find(menu.menuCont).slideDown(menu.tim);
                 jQuery(this).parents(menu.cont).find(menu.menuCont).addClass(menu.add);
             }
                 
         }
     });
    
     jQuery(menu.butt).on({
         click: function(){
             
             if(window.innerWidth <= menu.x){
                 
                 if(jQuery(this).parent().hasClass(menu.add)){

                     jQuery(this).parents('li').find(menu.sub).slideUp(menu.tim);
                     jQuery(this).parents('li').removeClass(menu.add);

                 }else{

                     jQuery(this).parents(menu.cont).find('li').removeClass(menu.add);
                     jQuery(this).parents(menu.cont).find(menu.sub).slideUp(menu.tim);

                     jQuery(this).parents('li').find(menu.sub).slideDown(menu.tim);
                     jQuery(this).parents('li').addClass(menu.add);
                 }
                 
             }
             
         }
     });
    
}

menu.res = function(){
    
    if(window.innerWidth > menu.x){
        
        jQuery(menu.sub).removeAttr('style');
        jQuery(menu.cont + ' li').removeClass(menu.add);
        
        jQuery(menu.menuCont).removeAttr('style');
        jQuery(menu.menuCont).removeClass(menu.add);
    }
};



function windowPopup(url, width, height) {
  // Calculate the position of the popup so
  // it’s centered on the screen.
  var left = (screen.width / 2) - (width / 2),
      top = (screen.height / 2) - (height / 2);

  window.open(
    url,
    "",
    "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
  );
}





						   
				   
						   