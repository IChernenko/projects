<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package besimple
 */

get_header(); ?>






	<!-- .content -->
	<div class="content">

	<div class="content__container">




	<div class="content-cont">


	<div class="content-centre">



	<!-- .boxLess -->
	<div class="boxLess box">

		<?php
		if ( have_posts() && get_search_query()!='') : ?>


			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				if(get_post_type()!='page') :
				?>
				<?php $post_categories = get_the_category();?>
	<div class="boxLess__cont box_item">

	<div class="boxLess-block">
	<div class="boxLess-img"><?php the_post_thumbnail();?></div>
	<div class="boxLess-cont">
	<span class="boxLess-category">
				<?php
				if(!empty($post_categories)) {
					foreach ($post_categories as $category) {
//                                                        var_dump($category);
						if ($category->term_id != 1)
							$category_line .= '<a href="' . get_term_link($category->term_id) . '">' . $category->cat_name . '</a>, ';
					}
				}
				echo rtrim($category_line,', ');
				unset($category_line);
				?></span>
		<span class="boxLess-title"><?php echo get_the_title();?></span>
		<span class="boxLess-date"><?php echo date('F j, Y',strtotime(get_the_date()));?></span>
		<div class="boxLess-text">
			<p><?php $excerpt = get_the_excerpt(); echo custom_limit_excerpt($excerpt, 100);?></p>
		</div>

	</div>

		<a class="boxLess-button" href="<?php echo get_post_permalink ();?>"><?php echo __('CONTINUE READING','besimple'); ?></a>

		<div class="infoMenu">
			<ul>
				<li><a href="javascript:void(0)"><?php echo ( get_comments_number() != 1 ) ? get_comments_number().__( ' Comments','besimple') : get_comments_number().__(' Comment','besimple'); ?></a></li>
				<li>
					<div class="socialNet-info">
						<?php for($i=1;$i<=5;$i++){ ?>
							<?php if( get_theme_mod('post_social_link_setting_'.$i) != '' && get_theme_mod('post_social_img_setting_'.$i) != '' && get_theme_mod('post_social_hov_img_setting_'.$i) != '' ){ ?>
								<a class="socialNetworks__fb" href="<?php echo get_theme_mod('post_social_link_setting_'.$i);?>">
									<img src="<?php echo get_theme_mod('post_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('post_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('post_social_img_setting_'.$i).'\''; ?>"/>
								</a>
							<?php } ?>
						<?php } ?>
					</div>
				</li>
			</ul>
		</div>

	</div>

	</div>
				<?php
				endif;
			endwhile;
			wp_reset_postdata();

		else:?>

		<span class="nothingFound-text"><?php printf( esc_html__( 'Nothing found for: %s', 'besimple' ), '<span>' . get_search_query() . '</span>' ); ?></span>

		<?php endif; ?>
	</div>

	<nav class="navigation">
	    <?php the_posts_pagination( array(
	        'mid_size' => 2,
	        'prev_text' => __( 'Back', 'textdomain' ),
	        'next_text' => __( 'Onward', 'textdomain' ),
	    )); ?>
	</nav>
                    
                    
<?php 
  if ( get_option('posts_per_page') < $wp_query->found_posts ) {
?>

<div class="morePosts-cont">
    <a class="morePosts-button" ><?php echo __('LOAD MORE POSTS','besimple'); ?></a>
</div> 

<?php 
    }
?>
<?php wp_reset_postdata(); ?>
	</div>

	</div>


		<!-- .events -->
		<?php if ( is_active_sidebar( 'popular-007' )) { ?>
			<?php dynamic_sidebar( 'popular-007' ); ?>
		<?php } ?>
		<!-- END .events -->



	</div>

	</div>
	<!-- END .content -->




<?php get_footer(); ?>