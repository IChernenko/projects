<?php
/*
Template Name: Typography
*/
get_header();?>
       <!-- .titleBlock -->
       <div class="titleBlock">
            
            <div class="titleBlock__img"><img src="<?php echo get_template_directory_uri(); ?>/images/content/big/img-4.jpg" alt="img"/></div>
            
            <div class="titleBlock__container">
                 
                 <div class="titleBlock-cont">
                 
                      <div class="titleBlock-box">

                           <?php if(have_posts()) :?>
                           <h1><?php the_title(); ?></h1>
                           <?php endif; ?>
                           <?php wp_reset_postdata(); ?>

                      </div>
                      
                 </div>
                 
            </div>
       
       </div>
       <!-- END .titleBlock -->
       
       

       
       
       <!-- .content -->
       <div class="content">
            
            <div class="content__container">
                 

                 
                 
                 <div class="content-cont">
                      

                      <div class="content-centre">
					  
					  <?php if(have_posts()) :?>
								<?php while ( have_posts() ) : the_post();?>
                           
                           <div class="contentBox">
                                
                                

                                     <?php the_content(); ?>
                                
                                
                                
                                
                                
                                <div class="infoMenu">
							<ul>
<!--								<li><a href="javascript:void(0)">by --><?php //the_author();?><!--</a></li>-->
								<li><?php the_author_posts_link(); ?></li>
								<li><a class="show-hide-comments" href="javascript:void(0)"><?php echo ( get_comments_number() != 1 ) ? get_comments_number().__( ' Comments','besimple') : get_comments_number().__(' Comment','besimple'); ?></a></li>
								<li>
									<div class="socialNet-info">
                                        <?php for($i=1;$i<=5;$i++){ ?>
                                            <?php if( get_theme_mod('post_social_link_setting_'.$i) != '' && get_theme_mod('post_social_img_setting_'.$i) != '' && get_theme_mod('post_social_hov_img_setting_'.$i) != '' ){ ?>
                                                <a class="socialNetworks__fb" href="<?php echo get_theme_mod('post_social_link_setting_'.$i);?>">
                                                    <img src="<?php echo get_theme_mod('post_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('post_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('post_social_img_setting_'.$i).'\''; ?>"/>
                                                </a>
                                            <?php } ?>
                                        <?php } ?>
									</div>
								</li>
							</ul>
						</div>
                            <?php comments_template(); ?>

                        
                                
          
                           </div>
						   
						   <?php // If comments are closed and there are comments, let's leave a little note, shall we?
if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

    <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'besimple' ); ?></p>
<?php endif;?>

<?php
$args = array(
    'comment_field' => '<div class="leaveReply-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="'._x( 'Comment', 'noun' ).'"></textarea></div>',
    'class_form' => 'leaveReply__cont',
    'fields' => apply_filters(
        'comment_form_default_fields', array(
            'author' =>'<div class="leaveReply-input"><div class="leaveReply-inputBox">' . '<input id="author" placeholder="'.__( 'Name *', 'besimple' ).'" name="author" type="text" value="' .
                esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />'.'</div>',

            'email'  => '<div class="leaveReply-inputBox">' . '<input id="email" placeholder="'.__( 'Email *', 'besimple' ).'" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                '" size="30"' . $aria_req . ' />'  .  '</div>',

            'url'    => '<div class="leaveReply-inputBox">' . '<input id="url" name="url" placeholder="'.__( 'Website', 'besimple' ).'" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /> ' . '</div></div>'

        )
    ),
);
comment_form($args); ?>

<?php endwhile; endif; // End of the loop.
                        ?>
                        <?php wp_reset_postdata(); ?>
                           
                      </div>
                 
                 </div>





                <!-- .events -->
                <?php if ( is_active_sidebar( 'popular-007' )) { ?>
                    <?php dynamic_sidebar( 'popular-007' ); ?>
                <?php } ?>
                <!-- END .events -->
                 
                 
                 
            </div>
       
       </div>
       <!-- END .content -->

       
      <?php //get_sidebar(); ?>
<?php get_footer(); ?>