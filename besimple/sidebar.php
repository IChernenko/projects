<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package besimple
 */?>

<div class="content-left">

    <!--<div class="banner"><a href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/images/content/banner-250x250.jpg" alt="img"/></a></div>-->
    
    <?php if ( is_active_sidebar( 'sidebar-google-ads' )) { ?>
        <?php dynamic_sidebar( 'sidebar-google-ads' ); ?>
    <?php } ?>


    <?php  if ( !is_user_logged_in() ) { ?>
	     <?php if ( is_active_sidebar( 'sidebar-subscribe' )) { ?>
	          <?php dynamic_sidebar( 'sidebar-subscribe' ); ?>
	     <?php } ?>
	<?php } else { ?>
			<div class="newsletter">
                <a class="newsletter__name" href="javascript:void(0)">SIGN UP OUR NEWSLETTER!</a>
                <div class="newsletter__text">
                     <p>Write down your email here to receive updates when we release new themes.</p>
                </div>
                <div class="newsletter__input">
                     <form>
                          <input class="inputNewsletter-text" type="text" placeholder="Your Email" />
                          <input class="inputNewsletter-submit" type="submit" value="SUBSCRIBE" />
                     </form>
                </div>
           </div>
	<?php } ?>

     <?php if ( is_active_sidebar( 'sidebar-1' )) { ?>
          <?php dynamic_sidebar( 'sidebar-1' ); ?>
     <?php } ?>

</div>


