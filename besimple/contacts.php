<?php
/*
Template Name: Contacts
*/
get_header();?>
<!-- .titleBlock -->
       <div class="titleBlock">

            <div class="titleBlock__img"><img src="<?php echo get_template_directory_uri(); ?>/images/content/big/img-1.jpg" alt="img"/></div>

            <div class="titleBlock__container">

                 <div class="titleBlock-cont">

                      <div class="titleBlock-box">
                           <?php if(have_posts()) :?>
                           <h1><?php the_title(); ?></h1>
                           <?php endif; ?>
                           <?php wp_reset_postdata(); ?>

                      </div>

                 </div>

            </div>

       </div>
       <!-- END .titleBlock -->
       
       

       
       
       <!-- .content -->
       <div class="content">
            
            <div class="content__container">
                 

                 
                 
                 <div class="content-cont">
                      

                      <div class="content-centre">
                           
                           <div class="contentBox">

                                <?php if(have_posts()) :?>

                                    <?php $post_meta = get_post_meta(get_the_ID());?>

                                    <h2><?php echo $post_meta['contacts_title_field'][0];?></h2>

                               <div class="contacts-about">
                                   <h2><?php __('About me','besimple');?></h2>
                                     <?php the_content(); ?>
                               </div>

                               <div class="contacts-form">
                                   <h2><?php __('Contact me','besimple');?></h2>
                                   <?php echo do_shortcode($post_meta['contacts_shortcode_field'][0]);?>
                               </div>
                                <div class="clear"></div>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>

                           </div>
                           
                      </div>
                 
                 </div>

                 
                 <!-- .events -->
                <?php if ( is_active_sidebar( 'popular-007' )) { ?>
                    <?php dynamic_sidebar( 'popular-007' ); ?>
                <?php } ?>
                 <!-- END .events -->


            </div>
       
       </div>
       <!-- END .content -->


<?php //get_sidebar(); ?>
<?php get_footer(); ?>