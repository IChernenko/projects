<?php
/**
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package besimple
 * @since besimple 1.0
 */
get_header(); ?>

   
<div class="content">
            
            <div class="content__container">
                 

                 
                 
                 <div class="content-cont">
                      

                      <div class="content-centre">
                        <div class="contentBox">
	                      	<?php the_content(); ?>
	                      </div>
                      </div>
                 
                 </div>


  
                 
                 
                 
            </div>
       
       </div>



<?php get_footer(); ?>