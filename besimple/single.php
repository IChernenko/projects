<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package besimple
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post();?>
	<!-- .titleBlock -->
	<div class="titleBlock">



    <?php $post_meta = get_post_meta(get_the_ID(),'image_field_id',true); ?>

    <div class="titleBlock__img"><img src="<?php echo $post_meta['url']==''? get_template_directory_uri().'/images/content/big/img-2.jpg' : $post_meta['url']; ?>" alt="img"/></div>

		<div class="titleBlock__container">

			<div class="titleBlock-cont">

				<div class="titleBlock-box">

                    <?php $post_categories = get_the_category();?>

                    <?php
						if(!empty($post_categories)) {
							$category_line = '';
							foreach ($post_categories as $category) {
								if ($category->term_id != 1)
									$category_line .= $category->cat_name . ', ';
							}
						}
                    ?>

                    <span class="titleBox-name"><?php echo rtrim($category_line,', ');?></span>

					<h1><?php echo get_the_title();?></h1>
					<span class="titleBox-date"><?php echo get_the_date(); ?></span>
                </div>

			</div>

		</div>

	</div>
	<!-- END .titleBlock -->

	<?php 
	
		// var_dump(get_theme_mods());
		if( get_theme_mod( 'sidebar_display_setting' ) == 'left' ) {
			get_template_part( 'template-parts/content', 'left_sidebar' );
		} else if ( get_theme_mod( 'sidebar_display_setting' ) == 'right' ) {
			get_template_part( 'template-parts/content', 'right_sidebar' );
		} else {
			get_template_part( 'template-parts/content', 'no_sidebar' );
		}

	?>




	<!-- .content -->
	

<?php endwhile; // End of the loop.?>
<?php wp_reset_postdata(); ?>

<?php
//get_sidebar();
get_footer();
