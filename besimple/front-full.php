<?php
/**
 * Template Name: Main Page
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package besimple
 * @since besimple 1.0
 */
get_header(); ?>

    <?php     	
    	if(get_theme_mod( 'front_page_box_slider' ) == '' ) {
			if(get_theme_mod( 'front_page_change_slider' ) == 'full' ) {
			get_template_part( 'template-parts/slider', 'full' );
			} else if ( get_theme_mod( 'front_page_change_slider' ) == 'side' ) {
				get_template_part( 'template-parts/slider', 'side' );
			} else {
				get_template_part( 'template-parts/slider', 'full' );
			}
		} 


    ?>
    <!-- .content -->
    <div class="content">
    <div class="hidden-subscribe-form" style="display: none">
        <?php echo do_shortcode('[subscribe2]');?>
    </div>


	
        <div class="content__container">
<?php 
		if(get_theme_mod( 'front_page_box_slider' ) != '' ) {		

			if(get_theme_mod( 'front_page_change_slider' ) == 'full' ) {
				get_template_part( 'template-parts/slider', 'full' );
			} else if ( get_theme_mod( 'front_page_change_slider' ) == 'side' ) {
				get_template_part( 'template-parts/slider', 'side' );
			} else {
				get_template_part( 'template-parts/slider', 'full' );
			}
		}
	?>
            <div class="content-cont">
                <?php 
                	if(get_theme_mod( 'sidebar_display_setting' ) == 'left' ) {
						get_template_part( 'template-parts/page', 'front_left' );
					} else if ( get_theme_mod( 'sidebar_display_setting' ) == 'right' ) {
						get_template_part( 'template-parts/page', 'front_right' );
					} else if ( get_theme_mod( 'sidebar_display_setting' ) == 'full' ) {
						get_template_part( 'template-parts/page', 'front_full' );
					} else {
						get_template_part( 'template-parts/page', 'front_full' );
					}
                ?>
                
                  
                <?php 

                	if ( get_theme_mod( 'sidebar_display_setting' ) == 'left' ) {
						get_sidebar();
					}
					if ( get_theme_mod( 'sidebar_display_setting' ) == 'right' ) {
						get_sidebar(); 

					?>
						
				<?php 
					}
                ?>    
                      

            </div>



            <?php if ( is_active_sidebar( 'popular-007' )) { ?>
                <?php dynamic_sidebar( 'popular-007' ); ?>
            <?php } ?>





        </div>

    </div>
    <!-- END .content -->

<script>
	<?php 
		if ( get_theme_mod( 'sidebar_display_setting' ) == 'right' ) {
	?>
    $('.content-left').css('float', 'right');
    $('.content-left').css('padding-right', '0');
    $('.content-left').css('padding-left', '50px');
    <?php } ?>
</script>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>