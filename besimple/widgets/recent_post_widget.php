<?php 
// Creating the widget 
class recent_widget extends WP_Widget {

function __construct() {
	parent::__construct(
	'recent_widget', 

	// Widget name will appear in UI
	__('Recent posts Besimple', 'besimple'),

	// Widget description
	array( 'description' => __( 'Recent posts widget', 'besimple' ), ) 
	);
}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	// if ( ! empty( $title ) )
	// echo $args['before_title'] . $title . $args['after_title'];

	// This is where you run the code and display the output
?>

 <div class="recentPost">
                                <span class="blockInfo__title"><?php echo $title; ?></span>
                                <div class="recentPost__cont">
								
								<?php

				$recentpost = new WP_Query( array(
					'posts_per_page' => 5,  
					'orderby' => 'post_date', 
					'order' => 'DESC',
					'post_type' => 'post') );
				while ( $recentpost->have_posts() ) : $recentpost->the_post();
				?>
                                     
                                     <div class="recentPost-box">
                                          <div class="recentPost-img"><a href="<?php the_permalink() ?>"><img src="<?php the_post_thumbnail_url(  ); ?>" alt="img"/></a></div>
                                          <div class="recentPost-text">
                                               <div class="recentPost-link"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
                                               <span class="recentPost-date"><?php the_time( 'F j, Y' ); ?></span>
                                          </div>
                                     </div>
									 
									 <?php endwhile;
								wp_reset_postdata();?>
                                     
                                </div>
                           </div>

<?php

	echo $args['after_widget'];
}
		
// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	}
	else {
		$title = __( 'New title', 'besimple' );
	}
// Widget admin form
?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class recent_widget ends here

// Register and load the widget
function recent_load_widget() {
	register_widget( 'recent_widget' );
}
add_action( 'widgets_init', 'recent_load_widget' );