<?php 
// Creating the widget 
class besimple_categories extends WP_Widget {

function __construct() {
	parent::__construct(
	'besimple_categories',

	// Widget name will appear in UI
	__('Besimple categories', 'besimple'),

	// Widget description
	array( 'description' => __( 'Besimple categories', 'besimple' ), )
	);
}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	// if ( ! empty( $title ) )
	// echo $args['before_title'] . $title . $args['after_title'];

	// This is where you run the code and display the output
?>

		<?php

		$args_category = array(
			'type'                     => 'post',
			'child_of'                 => 0,
			'parent'                   => '',
			'orderby'                  => 'name',
			'order'                    => 'ASC',
			'hide_empty'               => 1,
			'hierarchical'             => 1,
			'exclude'                  => '',
			'include'                  => '',
			'number'                   => '',
			'taxonomy'                 => 'category',
			'pad_counts'               => false

		);

		$categories = get_categories( $args_category );

		?>

		<div class="categories">
			<span class="blockInfo__title"><?php echo $title; ?></span>
			<div class="categories__cont">
				<?php if(!empty($categories)) {
					foreach ($categories as $category) {
//                        var_dump($category);?>


						<div class="categories-box">
							<div class="categories-link" style="<?php if( $category->parent>0 ) { echo 'margin-left: 15px;';} ?>"><a href="<?php echo get_term_link($category->cat_ID); ?>"><?php echo $category->name ?><span>(<?php echo $category->count ?>)</span></a></div>
						</div>
					<?php }
				}?>

			</div>
		</div>

<?php

	echo $args['after_widget'];
}
		
// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	}
	else {
		$title = __( 'New title', 'besimple' );
	}
// Widget admin form
?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class besimple_categories ends here

// Register and load the widget
function besimple_categories_load_widget() {
	register_widget( 'besimple_categories' );
}
add_action( 'widgets_init', 'besimple_categories_load_widget' );