<?php 
// Creating the widget 
class subscribe_and_follow_widget extends WP_Widget {

function __construct() {
	parent::__construct(
	'subscribe_and_follow_widget',

	// Widget name will appear in UI
	__('Subscribe and Follow Besimple', 'besimple'),

	// Widget description
	array( 'description' => __( 'Subscribe and Follow widget', 'besimple' ), )
	);
}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	// if ( ! empty( $title ) )
	// echo $args['before_title'] . $title . $args['after_title'];

	// This is where you run the code and display the output
?>

		<div class="subscribeFollow">
            <span class="blockInfo__title"><?php echo $title; ?></span>
            <div class="subscribeFollow__cont">

                <div class="socialNet-subscribe">
                    <?php for($i=1;$i<=5;$i++){ ?>
                        <?php if( get_theme_mod('widget_social_link_setting_'.$i) != '' && get_theme_mod('widget_social_img_setting_'.$i) != '' && get_theme_mod('widget_social_hov_img_setting_'.$i) != '' ){ ?>
                            <a class="socialNetworks__fb" href="<?php echo get_theme_mod('widget_social_link_setting_'.$i);?>">
                                <img src="<?php echo get_theme_mod('widget_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('widget_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('widget_social_img_setting_'.$i).'\''; ?>"/>
                            </a>
                        <?php } ?>
                    <?php } ?>
                </div>

            </div>
        </div>

<?php

	echo $args['after_widget'];
}
		
// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	}
	else {
		$title = __( 'New title', 'besimple' );
	}
// Widget admin form
?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class recent_widget ends here

// Register and load the widget
function subscribe_and_follow_load_widget() {
	register_widget( 'subscribe_and_follow_widget' );
}
add_action( 'widgets_init', 'subscribe_and_follow_load_widget' );