<?php
// Creating the widget 
class recent_comments_besimple_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'recent_comments_besimple_widget',

			// Widget name will appear in UI
			__('Recent comments Besimple', 'besimple'),

			// Widget description
			array( 'description' => __( 'Recent comments widget', 'besimple' ), )
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		// if ( ! empty( $title ) )
		// echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output
		?>

		<?php

		$comments = get_comments( apply_filters( 'widget_comments_args', array(
		'number'      => 5,
		'status'      => 'approve',
		'post_status' => 'publish'
		) ) );
		?>


		<div class="recentComments">
			<span class="blockInfo__title"><?php echo $title; ?></span>
			<div class="recentComments__cont">
<?php
			if ( is_array( $comments ) && $comments ) {
			// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
			$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
//				var_dump($post_ids);
			_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

			foreach ( (array) $comments as $comment ) {
//				var_dump($comment);
				?>

				<div class="recentComments-box">
					<div class="recentComments-link"><a href="<?php echo get_comment_link( $comment );?>"><?php echo get_the_title( $comment->comment_post_ID );?></a></div>
					<span class="recentComments-date"><?php echo get_comment_date( 'F j, Y', $comment );?></span>
				</div>

			<?php } ?>

			<?php } ?>

			</div>
		</div>

		<?php

		echo $args['after_widget'];
	}

// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'besimple' );
		}
// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class recent_widget ends here

// Register and load the widget
function recent_comments_besimple_load_widget() {
	register_widget( 'recent_comments_besimple_widget' );
}
add_action( 'widgets_init', 'recent_comments_besimple_load_widget' );