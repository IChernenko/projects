<?php 
// Creating the widget 
class popular_widget extends WP_Widget {

function __construct() {
	parent::__construct(
	'popular_widget', 

	// Widget name will appear in UI
	__('Popular width Widget', 'besimple'), 

	// Widget description
	array( 'description' => __( 'Popular width widget', 'besimple' ), ) 
	);
}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	// if ( ! empty( $title ) )
	// echo $args['before_title'] . $title . $args['after_title'];

	// This is where you run the code and display the output
?>
<span class="popularPosts-text"><?php echo $title; ?></span>
<div class="events">
	<div class="events__separator"></div>
		<div class="events__cont">
			<?php

				$popularpost = new WP_Query( array( 
					'posts_per_page' => 6, 
					'meta_key' => 'wpb_post_views_count', 
					'orderby' => 'meta_value_num', 
					'order' => 'DESC'  ) );	
					$i = 0;			
				while ( $popularpost->have_posts() ) : $popularpost->the_post();
					$i++;
                     $post_categories = get_the_category();
                     if ( $i == 1 || $i == 5 ) {

                     
				?>
					
					<div class="eventsBox eventsBox--info">
                        <div class="eventsInfo">
                            <span class="eventsInfo-category">
                            	<?php
                         if(!empty($post_categories)) {
                            $category_line = '';
                             foreach ($post_categories as $category) {
//                                          
                                 if ($category->term_id != 1) {
                                     $category_line .= '<a href="' . get_term_link($category->term_id) . '">' . $category->cat_name . '</a>, ';
                                 }
                             }
                         }
                                    echo rtrim($category_line,', ');

                                ?>
                            </span>
                            <div class="eventsInfo-name"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
                            <span class="eventsInfo-date"><?php echo date('F j, Y',strtotime(get_the_date()));?></span>
                            <div class="eventsInfo-text">
                            <!-- <?php the_excerpt(); ?> -->
                            <p><?php echo custom_limit_excerpt(get_the_excerpt(), 40); ?>  </p>
                            	                            
                            </div>
                        </div>
                    </div>


			<?php } else { ?>
				
					<div class="eventsBox eventsBox--block">
                        <div class="eventsBlock">
                            <div class="eventsBlock__img"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail();?></a></div>
                            <!-- <?php the_post_thumbnail();?> -->
                            <div class="eventsBlock__cont">
                                <span class="eventsBlock-category">
                                	<?php
                         if(!empty($post_categories)) {
                            $category_line = '';
                             foreach ($post_categories as $category) {
//                                          
                                 if ($category->term_id != 1) {
                                     $category_line .= '<a href="' . get_term_link($category->term_id) . '">' . $category->cat_name . '</a>, ';
                                 }
                             }
                         }
                                    echo rtrim($category_line,', ');

                                ?>
                                </span>
                                <div class="eventsBlock-name"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
                                <span class="eventsBlock-date"><?php echo date('F j, Y',strtotime(get_the_date()));?></span>
                            </div>
                        </div>
                    </div>
			<?php }
				endwhile;
            wp_reset_postdata();
			?>
		</div>

	</div>


<?php

	echo $args['after_widget'];
}
		
// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	}
	else {
		$title = __( 'New title', 'besimple' );
	}
// Widget admin form
?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class popular_widget ends here

// Register and load the widget
function popular_load_widget() {
	register_widget( 'popular_widget' );
}
add_action( 'widgets_init', 'popular_load_widget' );



// vidget function
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
     wpb_set_post_views($post_id); 
}
add_action( 'wp_head', 'wpb_track_post_views');