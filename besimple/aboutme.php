<?php
/*
Template Name: AboutMe
*/
get_header();?>
<!-- .titleBlock -->
       <div class="titleBlock">
            
            <div class="titleBlock__img"><img src="<?php echo get_template_directory_uri(); ?>/images/content/big/img-1.jpg" alt="img"/></div>
            
            <div class="titleBlock__container">
                 
                 <div class="titleBlock-cont">
                 
                      <div class="titleBlock-box">
                           <?php if(have_posts()) :?>
                           <h1><?php the_title(); ?></h1>
                           <?php endif; ?>
                           <?php wp_reset_postdata(); ?>

                      </div>
                      
                 </div>
                 
            </div>
       
       </div>
       <!-- END .titleBlock -->
       
       

       
       
       <!-- .content -->
       <div class="content">
            
            <div class="content__container">
                 

                 
                 
                 <div class="content-cont">
                      

                      <div class="content-centre">
                           
                           <div class="contentBox">

                                <?php if(have_posts()) :?>

                                     <?php the_content(); ?>

                                    <?php $post_meta = get_post_meta(get_the_ID()); ?>

                                    <?php for($i=1;$i<=3;$i++){
                                        if($post_meta['related_post_field_'.$i][0] != '-1' && isset($post_meta['related_post_field_'.$i][0])){
                                            $is_related_posts = true;
                                        }
                                    }
                                    if($is_related_posts){?>
                                        <h2><?php echo __('Related post','besimple'); ?></h2>
                                    <?php }?>

                                    <div class="relatedPost">

                                <?php for($i=1;$i<=3;$i++){?>
                                   <?php if($post_meta['related_post_field_'.$i][0] != '-1' && isset($post_meta['related_post_field_'.$i][0])){?>

                                     <div class="relatedPost-cont">
                                          <div class="relatedPost-box">
                                               <a class="relatedPost-img" href="<?php the_permalink($post_meta['related_post_field_'.$i][0]); ?>"><?php echo get_the_post_thumbnail ($post_meta['related_post_field_'.$i][0]); ?></a>
                                               <a class="relatedPost-link" href="<?php the_permalink($post_meta['related_post_field_'.$i][0]); ?>"><?php echo get_the_title ($post_meta['related_post_field_'.$i][0]); ?></a>
                                               <p><?php echo get_post_field('post_excerpt', $post_meta['related_post_field_'.$i][0]) != '' ? custom_limit_excerpt(get_post_field('post_excerpt', $post_meta['related_post_field_'.$i][0]),10) : custom_limit_excerpt(get_post_field('post_content', $post_meta['related_post_field_'.$i][0]),10); ?></p>
                                          </div>
                                     </div>

                                   <?php }?> 
                                <?php }?>
                                    </div>

                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>

                           </div>
                           
                      </div>
                 
                 </div>


                <!-- .events -->
                <?php if ( is_active_sidebar( 'popular-007' )) { ?>
                    <?php dynamic_sidebar( 'popular-007' ); ?>
                <?php } ?>
                <!-- END .events -->
                 
                 
                 
            </div>
       
       </div>
       <!-- END .content -->


<?php //get_sidebar(); ?>
<?php get_footer(); ?>