<?php
/*
Template Name: Slidess
*/
get_header(); ?>
 <?php wp_reset_postdata(); ?>
    <!-- .titleBlock -->
    <div class="titleBlock">

        <?php
        $args = array('post_type' => 'slides',
            'orderby' => 'menu_order',
            'posts_per_page' => -1);

        $slides = new WP_Query($args);

        if ( $slides->have_posts() ) : ?>
            <div class="titleBlock__img">
                <div class="slideBox">
                    <?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
                        <div class="slideBox__cont"> <?php the_post_thumbnail('slides');?> </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>

                </div>
            </div>
        <?php endif;    ?>


        <div class="titleBlock__container">
            <div class="titleBlock-cont">
                <div class="titleBlock-box">
                <?php
                    $args = array('post_type' => 'news',
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'post_status' => 'publish',
                        'posts_per_page' => 1);

                        $news = new WP_Query($args);

                        if ( $news->have_posts() ) : $news->the_post();?>

                            <span class="titleBox-name"><?php echo __('NEWS','besimple'); ?></span>
                            <h2><?php the_title(); ?></h2>
                            <span class="titleBox-text"><?php $excerpt = get_the_excerpt(); echo custom_limit_excerpt($excerpt, 20);?></span>

                            <a class="readMore-button" href="<?php echo get_post_permalink ();?>"><?php echo __('READ MORE','besimple'); ?></a>
                            <?php wp_reset_postdata(); ?>
                        <?php endif;    ?>

                </div>
            </div>
        </div>
    </div>
    <!-- END .titleBlock -->





    <!-- .content -->
    <div class="content">
            
            <div class="content__container">  
             <div class="content-cont">
                      

                      <div class="left-content-less">
                           
                           <div class="contentBox">
                                
                                <img class="picture-entireWidth removeIndent-6" src="images/content/middle/img-13.jpg" alt="img"/>
                                
                                
                                <span class="contentBox-name">CITIES</span>
                                <h1 class="text-centre-2">About the capital of the Czech Republic.</h1>
                                <span class="contentBox-date">OCTOBER 15, 2016</span>
                                
                                
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
                                
                                
                                <a class="contentBox-button" href="javascript:void(0)">CONTINUE READING</a>
                              
                                
                                <div class="infoMenu infoMenu-indent">
                                     <ul>
                                          <li><a href="javascript:void(0)">by Maneken</a></li>
                                          <li><a href="javascript:void(0)">20 Comments</a></li>
                                          <li>                                     
                                               <div class="socialNet-info">
                                                   <?php for($i=1;$i<=5;$i++){ ?>
                                                       <?php if( get_theme_mod('post_social_link_setting_'.$i) != '' && get_theme_mod('post_social_img_setting_'.$i) != '' && get_theme_mod('post_social_hov_img_setting_'.$i) != '' ){ ?>
                                                           <a class="socialNetworks__fb" href="<?php echo get_theme_mod('post_social_link_setting_'.$i);?>">
                                                               <img src="<?php echo get_theme_mod('post_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('post_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('post_social_img_setting_'.$i).'\''; ?>"/>
                                                           </a>
                                                       <?php } ?>
                                                   <?php } ?>
                                               </div>
                                          </li>
                                     </ul>
                                </div>
                                
          
                           </div>
                           
                           
                           
                           <!-- .boxBig -->
                           <div class="boxBig box"> 
                                <?php 
                                    wp_reset_query();
                                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                    $args = array(
                                    'posts_per_page' => 3,
                                    'orderby' => 'post_date',
                                    'order' => 'DESC',
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'paged' => $paged,
                                );
                                 $wp_query = new WP_Query( $args );

                                if ( $wp_query->have_posts() ) {

                                while ( $wp_query->have_posts() ) {
                                    $wp_query->the_post();
                                    $post_categories = get_the_category();?>
                                <div class="boxBig__cont box_item">                                     
                                     <div class="boxBig-block">
                                          <div class="boxBig-img"><?php the_post_thumbnail();?></div>
                                          <div class="boxBig-cont">
                                               <span class="boxBig-category"><?php
                                    if(!empty($post_categories)) {
                                        foreach ($post_categories as $category) {
//                                                        var_dump($category);
                                            if ($category->term_id != 1)
                                                $category_line .= '<a href="' . get_term_link($category->term_id) . '">' . $category->cat_name . '</a>, ';
                                        }
                                    }
                                                    echo rtrim($category_line,', ');
                                                    unset($category_line);
                                                ?></span>
                                               <span class="boxBig-title"><a href="<?php echo get_post_permalink ();?>"><?php echo get_the_title();?></a></span>
                                               <span class="boxBig-date"><?php echo date('F j, Y',strtotime(get_the_date()));?></span>
                                               <div class="boxBig-text">
                                                    <p><?php $excerpt = get_the_excerpt(); echo custom_limit_excerpt($excerpt, 80);?></p>
                                               </div>
    
                                          </div>
                                          
                                          <a class="boxBig-button" href="<?php echo get_post_permalink ();?>"><?php echo __('CONTINUE READING','besimple'); ?></a>
                                          
                                          <div class="infoMenu">
                                            <ul>
                                                <li><a href="<?php echo get_post_permalink ().'&scroll=comments';?>"><?php echo ( get_comments_number() != 1 ) ? get_comments_number().__( ' Comments','besimple') : get_comments_number().__(' Comment','besimple'); ?></a></li>
                                                <li>
                                                    <div class="socialNet-info">
                                                        <?php for($i=1;$i<=5;$i++){ ?>
                                                            <?php if( get_theme_mod('post_social_link_setting_'.$i) != '' && get_theme_mod('post_social_img_setting_'.$i) != '' && get_theme_mod('post_social_hov_img_setting_'.$i) != '' ){ ?>
                                                                <a class="socialNetworks__fb" href="<?php echo get_theme_mod('post_social_link_setting_'.$i);?>">
                                                                    <img src="<?php echo get_theme_mod('post_social_img_setting_'.$i); ?>" onmouseover="<?php echo 'this.src=\''.get_theme_mod('post_social_hov_img_setting_'.$i).'\''; ?>" onmouseout="<?php echo 'this.src=\''.get_theme_mod('post_social_img_setting_'.$i).'\''; ?>"/>
                                                                </a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                          
                                     </div>
                                     
                                </div>
                                 <?php } ?>
                                <?php wp_reset_postdata(); ?>

                                    
                                <?php } ?>
                           
                           </div>
                           <!-- END .boxBig -->
                            <nav class="navigation">
                                <?php the_posts_pagination( array(
                                    'mid_size' => 2,
                                    'prev_text' => __( 'Back', 'textdomain' ),
                                    'next_text' => __( 'Onward', 'textdomain' ),
                                )); ?>
                            </nav>
                           
                           <div class="morePosts-cont">
                                <a class="morePosts-button"><?php echo __('LOAD MORE POSTS','besimple'); ?></a>
                           </div>
                           
                           
                      </div>
                      
                      
                      
                      <?php get_sidebar(); ?>
                 
                 </div>
                 
                 
                 
               
                 
                 
                 
                 <!-- .events -->
                   <?php if ( ! dynamic_sidebar() ) : ?>
                      <?php dynamic_sidebar( 'popular-007' ); ?>
                    <?php endif; ?> 
                 <!-- END .events -->
                 
                 
                 
            </div>
       
       </div>
    <!-- END .content -->



<?php //get_sidebar(); ?>
<?php get_footer(); ?>